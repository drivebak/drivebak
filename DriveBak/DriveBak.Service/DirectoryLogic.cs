using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DriveBak.Drive;
using DriveBak.Logging;

namespace DriveBak.Service
{
    public interface IDirectoryLogic
    {
        Task ExecuteAsync(BackupDirectory backupDirectory);
    }

    public class DirectoryLogic : IDirectoryLogic
    {
        private readonly IFilesystem filesystem;
        private readonly IDriveManager driveManager;
        private readonly ILogger<DirectoryLogic> logger;

        public DirectoryLogic(IFilesystem filesystem, IDriveManager driveManager, ILogger<DirectoryLogic> logger)
        {
            this.filesystem = filesystem;
            this.driveManager = driveManager;
            this.logger = logger;
        }

        public async Task ExecuteAsync(BackupDirectory backupDirectory)
        {
            logger.AddContext(LogContext.DirPath, backupDirectory.LocalPath)
                .AddContext(LogContext.RemotePath, backupDirectory.RemotePath)
                .Information("Local path '{@LocalPath}': Starting backup", backupDirectory.LocalPath);

            List<string> newFiles;

            var localFiles = filesystem.List(backupDirectory.LocalPath);
            if (localFiles == null)
            {
                logger.AddContext(LogContext.DirPath, backupDirectory.LocalPath)
                    .AddContext(LogContext.RemotePath, backupDirectory.RemotePath)
                    .Warning("Local path '{@LocalPath}': Directory was not found", backupDirectory.LocalPath);
                return;
            }

            var remoteFiles = await driveManager.GetFileNamesAsync(backupDirectory.RemotePath);
            if (remoteFiles == null)
            {
                await driveManager.CreateFolderAsync(backupDirectory.RemotePath);
                newFiles = localFiles.ToList();
            }
            else
            {
                newFiles = localFiles.Except(remoteFiles).ToList();
            }

            logger.AddContext(LogContext.DirPath, backupDirectory.LocalPath)
                .AddContext(LogContext.RemotePath, backupDirectory.RemotePath)
                .Information("Local path '{@LocalPath}': Backing up {@Count} new files", backupDirectory.LocalPath, newFiles.Count());

            foreach (var newFile in newFiles)
                await driveManager.UploadFileAsync(Path.Join(backupDirectory.LocalPath, newFile), backupDirectory.RemotePath, newFile, "application/gzip");
        }
    }
}