using System;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Logging;
using Microsoft.Extensions.Hosting;

namespace DriveBak.Service
{
    public class DaemonService : BackgroundService
    {
        private readonly IBackupManager backupManager;
        private readonly IServiceSettings serviceSettings;
        private readonly ILogger<DaemonService> logger;

        public DaemonService(IBackupManager backupManager, IServiceSettings serviceSettings, ILogger<DaemonService> logger)
        {
            this.backupManager = backupManager;
            this.serviceSettings = serviceSettings;
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.Information("Starting service with {@ExecutionPeriodSeconds} second execution period",
                serviceSettings.ExecutionPeriodSeconds);
            while (!stoppingToken.IsCancellationRequested)
            {
                backupManager.Execute(stoppingToken);
                await Task.Delay(TimeSpan.FromSeconds(serviceSettings.ExecutionPeriodSeconds), stoppingToken);
            }
        }
    }
}