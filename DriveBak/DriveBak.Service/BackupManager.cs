using System.Linq;
using System.Threading;
using DriveBak.DB;
using DriveBak.Logging;

namespace DriveBak.Service
{
    public interface IBackupManager
    {
        void Execute(CancellationToken stoppingToken);
    }

    public class BackupManager : IBackupManager
    {
        private readonly IApplicationsManager applicationsManager;
        private readonly IDirectoryLogic directoryLogic;
        private readonly ILogger<BackupManager> logger;

        public BackupManager(IApplicationsManager applicationsManager, IDirectoryLogic directoryLogic, ILogger<BackupManager> logger)
        {
            this.applicationsManager = applicationsManager;
            this.directoryLogic = directoryLogic;
            this.logger = logger;
        }

        public void Execute(CancellationToken stoppingToken)
        {
            logger.Information("Starting Execution");
            var applications = applicationsManager.GetAllApplications().Select(a => a.Name);
            foreach (var application in applications)
            {
                var directories = applicationsManager.GetApplicationDirectories(application);
                foreach (var directory in directories)
                {
                    if (!stoppingToken.IsCancellationRequested)
                        directoryLogic.ExecuteAsync(directory);
                }
            }
            logger.Information("Completed Execution");
        }
    }
}