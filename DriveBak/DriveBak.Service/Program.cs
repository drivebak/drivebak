using System;
using System.Threading.Tasks;
using DriveBak.Cache;
using DriveBak.DB;
using DriveBak.Drive.Auth;
using DriveBak.Logging;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Util.Store;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace DriveBak.Service
{
    public class Program
    {
        public static async Task Main(String[] args)
        {
            var builder = new HostBuilder().ConfigureServices(ConfigureIOC);
            await builder.RunConsoleAsync();
        }

        public static void ConfigureIOC(HostBuilderContext ctx, IServiceCollection services)
        {
            services.Scan(ts =>
            {
                ts.FromAssemblyOf<BackupDirectory>().AddClasses().AsSelf().AsMatchingInterface().WithTransientLifetime();
                ts.FromCallingAssembly().AddClasses().AsSelf().AsMatchingInterface().WithTransientLifetime();
            });
            var settings = new ServiceSettings();
            Log.Logger = Logging.Configuration
                .BuildLoggerConfiguration(settings.LogSettings)
                .ConfigureAppLogging(settings.LogSettings)
                .CreateLogger();
            services.AddSingleton(Log.Logger);
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            services.AddSingleton<IServiceSettings>(settings);
            services.AddSingleton(settings.PostgresSettings);
            services.AddSingleton(settings.GoogleOAuthSettings);
            services.AddSingleton(settings.RedisSettings);
            services.AddSingleton<IDataStore, RedisDataStore>();
            services.AddSingleton<ICacheClient, RedisCacheClient>();
            services.AddSingleton<IBackupManager, BackupManager>();
            services.AddTransient<IDBClient, PostgresClient>();
            services.AddSingleton<IAuthorizationCodeFlow, DriveBakAuthorizationCodeFlow>();
            services.AddHostedService<DaemonService>();
        }
    }
}