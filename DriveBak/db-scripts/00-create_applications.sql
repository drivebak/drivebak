begin;

CREATE TABLE IF NOT EXISTS applications
(
    id          serial PRIMARY KEY,
    name        varchar UNIQUE NOT NULL,
    description varchar,
    created     timestamp default now(),
    updated     timestamp default now()
);

end;