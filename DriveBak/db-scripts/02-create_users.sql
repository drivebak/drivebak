begin;

CREATE USER :writeuser WITH PASSWORD :'writepassword';
CREATE USER :readuser WITH PASSWORD :'readpassword';

GRANT CONNECT ON DATABASE drivebak TO :writeuser, :readuser;
GRANT USAGE ON SCHEMA public TO :writeuser, :readuser;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO :readuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO :writeuser;

GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO :readuser;
GRANT SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA public TO :readuser, :writeuser;

end;