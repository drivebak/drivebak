begin;

CREATE TABLE IF NOT EXISTS directories
(
    id             serial PRIMARY KEY,
    application_id int            NOT NULL,
    description    varchar,
    local_path     varchar UNIQUE NOT NULL,
    remote_path    varchar UNIQUE NOT NULL,
    created        timestamp default now(),
    updated        timestamp default now(),
    FOREIGN KEY (application_id)
        REFERENCES applications (id)
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

end;