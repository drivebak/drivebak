#!/usr/bin/env sh
PUBLISH_COMMAND="dotnet publish -c Release"

[[ -z "${APP_VERSION}" ]] || PUBLISH_COMMAND="${PUBLISH_COMMAND} /p:Version=${APP_VERSION} /p:AssemblyVersion=${APP_VERSION}"
[[ -z "${PUBLISH_DIR}" ]] || PUBLISH_COMMAND="${PUBLISH_COMMAND} -o ${PUBLISH_DIR}"
[[ -z "${PROJECT}" ]] || PUBLISH_COMMAND="${PUBLISH_COMMAND} ${PROJECT}/${PROJECT}.csproj"

exec ${PUBLISH_COMMAND}

