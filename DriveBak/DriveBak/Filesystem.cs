using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DriveBak
{
    public interface IFilesystem
    {
        IEnumerable<string> List(string path);
        bool IsValidPath(string path);
    }

    public class Filesystem : IFilesystem
    {
        public IEnumerable<string> List(string path)
        {
            try
            {
                return Directory.EnumerateFiles(path, "*.tar.gz", SearchOption.TopDirectoryOnly)
                    .Select(Path.GetFileName);
            }
            catch
            {
                return null;
            }
        }

        public bool IsValidPath(string path)
        {
            try
            {
                new DirectoryInfo(path);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}