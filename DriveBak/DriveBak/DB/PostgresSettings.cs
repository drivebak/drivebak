namespace DriveBak.DB
{
    public interface IPostgresSettings
    {
        string Host { get; }
        string Database { get; }
        string Username { get; }
        string Password { get; }
        int CommandTimeoutSeconds  { get; }
        int MaxPoolSize  { get; }
        int TimeoutSeconds  { get; }
    }

    public class PostgresSettings : IPostgresSettings
    {
        public string Host { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int CommandTimeoutSeconds => 30;
        public int MaxPoolSize => 50;
        public int TimeoutSeconds => 15;

        public override string ToString()
        {
            var passwordString = string.IsNullOrEmpty(Password) ? "NULL OR EMPTY" : "MASKED";
            return $"Host: {Host}\n" +
                   $"Database: {Database}\n" +
                   $"Username: {Username}\n" +
                   $"Password: {passwordString}\n" +
                   $"CommandTimeoutSeconds: {CommandTimeoutSeconds}\n" +
                   $"MaxPoolSize: {MaxPoolSize}\n" +
                   $"TimeoutSeconds: {TimeoutSeconds}\n";
        }
    }
}