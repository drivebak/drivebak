using System.Collections.Generic;

namespace DriveBak.DB
{
    public interface IDBClient
    {
        string Name { get; }
        string Version { get; }
        T Write<T>(string command, object parameters = null);
        void Write(string command, object parameters = null);
        IEnumerable<T> Read<T>(string command, object parameters = null);
    }
}