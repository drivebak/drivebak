using System.Collections.Generic;
using System.Linq;
using DriveBak.Logging;
using Npgsql;

namespace DriveBak.DB
{

    public interface IApplicationsManager
    {
        IEnumerable<Application> GetAllApplications();
        int AddApplication(Application application);
        int GetApplicationId(string applicationName);
        Application GetApplication(string applicationName);
        void RemoveApplication(int applicationId);
        int AddDirectory(int applicationId, BackupDirectory backupDirectory);
        int GetDirectoryId(string localPath);
        IEnumerable<BackupDirectory> GetDirectory(string localPath);
        void RemoveDirectory(int directoryId);
        IEnumerable<BackupDirectory> GetApplicationDirectories(string applicationName);
    }

    public class ApplicationsManager : IApplicationsManager
    {
        private readonly IDBClient dbClient;
        private readonly ILogger<ApplicationsManager> logger;

        private const string AppIdContext = "AppId";
        private const string AppNameContext = "AppName";
        private const string DirIdContext = "DirId";
        private const string DirPathContext = "DirPath";
        private const string RemotePathContext = "RemotePath";

        public ApplicationsManager(IDBClient dbClient, ILogger<ApplicationsManager> logger)
        {
            this.dbClient = dbClient;
            this.logger = logger;
        }

        public IEnumerable<Application> GetAllApplications()
        {
            logger.Information("Getting all applications");
            var getApplicationsCommand = @"SELECT name, description FROM applications";
            return dbClient.Read<Application>(getApplicationsCommand);
        }

        public int AddApplication(Application application)
        {
            logger.AddContext(LogContext.AppName, application.Name)
                .Information("Creating new application with name '{@Name}'", application.Name);

            var addApplicationCommand = @"INSERT INTO applications (name, description) values (@Name, @Description) RETURNING id";
            return dbClient.Write<int>(addApplicationCommand, application);
        }

        public int AddDirectory(int applicationId, BackupDirectory backupDirectory)
        {
            logger.AddContext(LogContext.AppName, backupDirectory.Application)
                .AddContext(LogContext.AppId, applicationId.ToString())
                .AddContext(LogContext.DirPath, backupDirectory.LocalPath)
                .AddContext(LogContext.RemotePath, backupDirectory.RemotePath)
                .Information("Adding new directory with local path '{@LocalPath}' to application '{@Application}'",
                backupDirectory.LocalPath,
                backupDirectory.Application);

            var command = @"INSERT INTO directories (application_id, description, local_path, remote_path)
                            VALUES (@appId, @description, @localPath, @remotePath)
                            RETURNING id";
            try
            {
                return dbClient.Write<int>(command, new
                {
                    appId = applicationId,
                    description = backupDirectory.Description,
                    localPath = backupDirectory.LocalPath,
                    remotePath = backupDirectory.RemotePath
                });
            }
            catch (PostgresException)
            {
                return 0;
            }
        }

        public int GetApplicationId(string applicationName)
        {
            logger.AddContext(LogContext.AppName, applicationName)
                .Debug("Getting ID for application '{@applicationName}'", applicationName);

            var command = @"SELECT id from applications WHERE name=@applicationName";
            return dbClient.Read<int>(command, new {applicationName}).SingleOrDefault();
        }

        public Application GetApplication(string applicationName)
        {
            logger.AddContext(LogContext.AppName, applicationName)
                .Information("Getting application by name '{@applicationName}'", applicationName);

            var command = @"SELECT name, description from applications WHERE name=@applicationName";
            return dbClient.Read<Application>(command, new {applicationName}).SingleOrDefault();
        }

        public int GetDirectoryId(string localPath)
        {
            logger.AddContext(LogContext.DirPath, localPath)
                .Debug("Getting directory ID  by local path '{@localPath}'", localPath);

            var command = @"SELECT id FROM directories WHERE local_path=@localPath";
            return dbClient.Read<int>(command, new {localPath}).SingleOrDefault();
        }

        public IEnumerable<BackupDirectory> GetDirectory(string localPath)
        {
            logger.AddContext(LogContext.DirPath, localPath)
                .Information("Getting directory by local path '{@localPath}'", localPath);

            var command = @"SELECT name as application, directories.description as description, local_path, remote_path
                            FROM directories 
                            LEFT JOIN applications a on directories.application_id = a.id
                            WHERE local_path=@localPath";
            return dbClient.Read<BackupDirectory>(command, new {localPath});
        }

        public void RemoveDirectory(int directoryId)
        {
            logger.AddContext(LogContext.DirId, directoryId.ToString())
                .Information("Removing directory by id '{@directoryId}'", directoryId);

            var command = @"DELETE from directories WHERE id = @directoryId";
            dbClient.Write(command, new {directoryId});
        }

        public void RemoveApplication(int applicationId)
        {
            logger.AddContext(LogContext.AppId, applicationId.ToString())
                .Information("Removing all directories for application by id '{@applicationId}'", applicationId);

            var dirDeleteCommand = @"DELETE FROM directories WHERE application_id=@applicationId";
            dbClient.Write(dirDeleteCommand, new {applicationId});

            logger.AddContext(LogContext.AppId, applicationId.ToString())
                .Information("Removing application by id '{@applicationId}'", applicationId);

            var appDeleteCommand = @"DELETE FROM applications WHERE id=@applicationId";
            dbClient.Write(appDeleteCommand, new {applicationId});
        }

        public IEnumerable<BackupDirectory> GetApplicationDirectories(string applicationName)
        {
            logger.AddContext(LogContext.AppName, applicationName)
                .Information("Getting all directories for application by name '{@applicationName}'", applicationName);

            var command = @"SELECT name as application, directories.description as description, local_path, remote_path
                            FROM directories 
                            LEFT JOIN applications a on directories.application_id = a.id
                            WHERE name=@application";
            return dbClient.Read<BackupDirectory>(command, new {application = applicationName});
        }
    }
}