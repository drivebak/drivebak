using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using DriveBak.Logging;
using Npgsql;

namespace DriveBak.DB
{
    public class PostgresClient : IDBClient
    {
        private readonly IPostgresSettings settings;
        private readonly ILogger<PostgresClient> logger;

        public PostgresClient(IPostgresSettings settings, ILogger<PostgresClient> logger)
        {
            DefaultTypeMap.MatchNamesWithUnderscores = true;
            this.settings = settings ?? throw new ArgumentException("Postgres settings cannot be null");
            this.logger = logger;
        }

        public string Name => "Postgres";
        public string Version => Read<string>("SHOW server_version;").SingleOrDefault();

        public T Write<T>(string command, object parameters = null)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                logger.Debug("{@Name}: Executing write query '{@command}'", Name, command);
                return connection.ExecuteScalar<T>(command, parameters);
            }
        }

        public void Write(string command, object parameters = null)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                logger.Debug("{@Name} Executing write query '{@command}'", Name, command);
                connection.Execute(command, parameters);
            }
        }

        public IEnumerable<T> Read<T>(string command, object parameters = null)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                logger.Debug("{@Name} Executing read query '{@command}'", Name, command);
                var result = connection.Query<T>(command, parameters);
                return result;
            }
        }

        private IDbConnection GetConnection()
        {
            logger.Verbose("{@Name} Creating connection", Name);
            var connectionStringBuilder = new NpgsqlConnectionStringBuilder()
            {
                Host = settings.Host,
                Database = settings.Database,
                Username = settings.Username,
                Password = settings.Password,
                CommandTimeout = settings.CommandTimeoutSeconds,
                MaxPoolSize = settings.MaxPoolSize,
                Timeout = settings.TimeoutSeconds,
            };
            return new NpgsqlConnection(connectionStringBuilder.ToString());
        }
    }
}