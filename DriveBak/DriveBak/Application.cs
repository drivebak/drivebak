namespace DriveBak
{
    public class Application
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Application;
            if (other == null)
                return false;

            return this.Name == other.Name &&
                   this.Description == other.Description;
        }

        public override int GetHashCode() =>
            (Name, Description).GetHashCode();

        public override string ToString() => Name;
    }
}