using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Cache;
using DriveBak.Drive.Auth;
using DriveBak.Logging;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Upload;
using File = Google.Apis.Drive.v3.Data.File;

namespace DriveBak.Drive
{
    public interface IDriveClient
    {
        Task<string> GetFolderIdAsync(string path);
        Task<IEnumerable<File>> GetChildrenAsync(string folderId);
        Task<string> CreateFolderAsync(string path);

        Task UploadFileAsync(string backupDirLocalPath, string backupDirRemotePath, string fileName, string contentType,
            Action<IUploadProgress> driveUploadProgressChanged, Action<File> driveUploadResponseReceived);
    }

    public class DriveClient : IDriveClient
    {
        private readonly IDriveAuth driveAuth;
        private readonly ICacheClient cacheClient;
        private readonly ILogger<DriveClient> logger;
        private Task<DriveService> driveService;

        public Task<DriveService> DriveService
        {
            get
            {
                if (driveService == null)
                    driveService = BuildDriveService();

                return driveService;
            }
        }

        public DriveClient(IDriveAuth driveAuth, ICacheClient cacheClient, ILogger<DriveClient> logger)
        {
            this.driveAuth = driveAuth;
            this.cacheClient = cacheClient;
            this.logger = logger;
        }

        private string GetCacheKey(List<string> path) =>
            $"drive:folder:{string.Join('/', path)}:id";

        public async Task<string> GetFolderIdAsync(string path) => await GetFolderIdAsync(path.Split("/").ToList());

        private async Task<string> GetFolderIdAsync(List<string> path)
        {
            string id;
            var final = path.Last();
            var cacheKey = GetCacheKey(path);
            if (cacheClient.TryGet(cacheKey, out id))
            {
                var folder = await GetFolderQueryAsync(id);
                if (folder == null || (folder.Trashed.HasValue && folder.Trashed.Value))
                    return null;

                return id;
            }

            var subId = "root";
            if (path.Count() > 1)
            {
                subId = await GetFolderIdAsync(path.SkipLast(1).ToList());
                if (subId == null)
                    return null;
            }

            id = await QueryFolderIdAsync(final, subId);
            if (!string.IsNullOrEmpty(id))
                _ = cacheClient.SetAsync(cacheKey, id); // No need to await

            return id;
        }

        private async Task<File> GetFolderQueryAsync(string folderId)
        {
            logger.Verbose("Google Drive: Querying for folder by id '{@folderId}'", folderId);

            var getRequest = (await DriveService).Files.Get(folderId);
            getRequest.Fields = "kind, id, name, trashed, mimeType";
            try
            {
                return await getRequest.ExecuteAsync();
            }
            catch
            {
                return null;
            }
        }

        private async Task<string> QueryFolderIdAsync(string name, string parent = "root")
        {
            logger.Verbose("Google Drive: Querying for id of folder named '{@name}' with parent id '{@parent}'",
                name, parent);

            var query = $"('{parent}' in parents) and " +
                        $"(trashed = false) and " +
                        $"(mimeType = 'application/vnd.google-apps.folder') and " +
                        $"(name = '{name}')";
            var listRequest = (await DriveService).Files.List();
            listRequest.Fields = "nextPageToken, files(id, name)";
            listRequest.Q = query;
            var fileResponse = await listRequest.ExecuteAsync();
            return fileResponse?.Files.SingleOrDefault()?.Id;
        }

        public async Task<IEnumerable<File>> GetChildrenAsync(string folderId)
        {
            var files = new List<File>();
            var query = $"('{folderId}' in parents) and (trashed = false) and (mimeType != 'application/vnd.google-apps.folder')";
            var listRequest = (await DriveService).Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name)";
            listRequest.Q = query;

            FileList fileResponse;
            string pageToken = null;

            do
            {
                listRequest.PageToken = pageToken;
                fileResponse = await listRequest.ExecuteAsync();
                files.AddRange(fileResponse.Files);
                pageToken = fileResponse.NextPageToken;
            } while (!string.IsNullOrEmpty(pageToken));

            return files;
        }

        public async Task<string> CreateFolderAsync(string path) => await CreateFolderAsync(path.Split("/").ToList());

        private async Task<string> CreateFolderAsync(List<string> path)
        {
            var dirId = await GetFolderIdAsync(path);
            if (!string.IsNullOrEmpty(dirId))
                return dirId;

            var parent = "root";
            if (path.Count > 1)
            {
                parent = await CreateFolderAsync(path.SkipLast(1).ToList());
            }

            var id = await QueryCreateFolderAsync(path.Last(), parent);
            _ = cacheClient.SetAsync(GetCacheKey(path), id); // No need to await
            return id;
        }

        private async Task<string> QueryCreateFolderAsync(string name, string parentId = "root")
        {
            logger.Verbose("Google Drive: Creating folder named '{@name}' with parent id '{@parent}'",
                name, parentId);

            var file = new File
            {
                MimeType = "application/vnd.google-apps.folder",
                Name = name,
                Parents = new List<string>() {parentId},
            };
            var createRequest = (await DriveService).Files.Create(file);
            var createResponse = await createRequest.ExecuteAsync();
            return createResponse.Id;
        }

        public async Task UploadFileAsync(string backupDirLocalPath,
            string backupDirRemotePath,
            string fileName,
            string contentType,
            Action<IUploadProgress> driveUploadProgressChanged,
            Action<File> driveUploadResponseReceived)
        {
            logger.Verbose("Google Drive: {@fileName}: Creating remote folder '{@backupDirRemotePath}'",
                fileName,
                backupDirRemotePath);

            var parent = CreateFolderAsync(backupDirRemotePath);

            logger.Verbose("Google Drive: {@fileName}: Initializing stream for local file '{@backupDirLocalPath}'",
                fileName,
                backupDirLocalPath);

            var uploadStream = new System.IO.FileStream(backupDirLocalPath,
                System.IO.FileMode.Open,
                System.IO.FileAccess.Read);

            logger.Verbose("Google Drive: {@fileName}: Initializing insert request", fileName);
            var insertRequest = (await DriveService).Files.Create(
                new File
                {
                    Name = fileName,
                    Parents = new List<string>() { await parent}
                },
                uploadStream,
                contentType);

            insertRequest.ProgressChanged += driveUploadProgressChanged;
            insertRequest.ResponseReceived += driveUploadResponseReceived;

            logger.Verbose("Google Drive: {@fileName}: Starting upload.", fileName);
            var task = insertRequest.UploadAsync();
            _= task.ContinueWith(t =>
            {
                uploadStream.Dispose();
            });
        }

        private async Task<DriveService> BuildDriveService()
        {
            var credential = driveAuth.GetUserCredentialsAsync(CancellationToken.None);
            try
            {
                return new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = await credential,
                    ApplicationName = "DriveBak",
                });
            }
            catch (Exception e)
            {
                logger.Error($"Error while authenticating to Google: {e.Message}\nTrace: {e.StackTrace}");
                throw;
            }
        }
    }
}