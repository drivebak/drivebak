using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DriveBak.Cache;
using DriveBak.Logging;
using Google.Apis.Upload;
using Serilog;
using DriveFile = Google.Apis.Drive.v3.Data.File;

namespace DriveBak.Drive
{
    public interface IDriveManager
    {
        Task UploadFileAsync(string localPath, string drivePath, string driveFileName, string contentType);
        Task<IEnumerable<string>> GetFileNamesAsync(string drivePath);
        bool IsValidPath(string path);
        Task CreateFolderAsync(string remotePath);
    }

    public class DriveManager : IDriveManager
    {
        private readonly IDriveClient driveClient;
        private readonly ICacheClient cacheClient;
        private readonly ILogger<DriveManager> logger;

        public DriveManager(IDriveClient driveClient, ICacheClient cacheClient, ILogger<DriveManager> logger)
        {
            this.driveClient = driveClient;
            this.cacheClient = cacheClient;
            this.logger = logger;
        }

        public async Task UploadFileAsync(string localPath, string drivePath, string driveFileName, string contentType)
        {
            logger.AddContext(LogContext.DirPath, localPath)
                .AddContext(LogContext.RemotePath, drivePath)
                .AddContext(LogContext.FileName, driveFileName)
                .Information("Google Drive: File '{@driveFileName}': initializing upload", driveFileName);
            await driveClient.UploadFileAsync(
                localPath,
                drivePath,
                driveFileName,
                contentType,
                GetUploadProgressChangedFunction(drivePath, driveFileName),
                GetResponseReceivedFunction(drivePath, driveFileName)
            );
        }

        private Action<IUploadProgress> GetUploadProgressChangedFunction(string drivePath, string driveFileName)
        {
            return progress =>
            {
                logger.AddContext(LogContext.RemotePath, drivePath)
                    .AddContext(LogContext.FileName, driveFileName)
                    .Verbose("Google Drive: File '{@driveFileName}': Status '{@Status}': {@} bytes sent", driveFileName,
                        progress.Status, progress.BytesSent);

                cacheClient.SetAsync(GetCacheKeyStatus(drivePath, driveFileName), progress.Status.ToString());
                cacheClient.SetAsync(GetCacheKeyBytes(drivePath, driveFileName), progress.BytesSent.ToString());
            };
        }


        private Action<DriveFile> GetResponseReceivedFunction(string drivePath, string driveFileName)
        {
            logger.AddContext(LogContext.RemotePath, drivePath)
                .AddContext(LogContext.FileName, driveFileName)
                .Information("Google Drive: File '{@driveFileName}': completed upload", driveFileName);

            return file =>
            {
                cacheClient.SetAsync(GetCacheKeyStatus(drivePath, driveFileName), UploadStatus.Completed.ToString());
            };
        }

        private string GetCacheKeyStatus(string drivePath, string driveFileName) =>
            $"drive:file:{drivePath}/{driveFileName}:progress";

        private string GetCacheKeyBytes(string drivePath, string driveFileName) =>
            $"drive:file:{drivePath}/{driveFileName}:bytes";

        public async Task<IEnumerable<string>> GetFileNamesAsync(string drivePath)
        {
            logger.AddContext(LogContext.RemotePath, drivePath)
                .Debug("Google Drive: listing files at '{@drivePath}'", drivePath);

            var folderId = await driveClient.GetFolderIdAsync(drivePath);
            if (!string.IsNullOrEmpty(folderId))
            {
                var files = await driveClient.GetChildrenAsync(folderId);
                return files.Select(f => f.Name).ToList();
            }

            return null;
        }

        public bool IsValidPath(string path)
        {
            var directories = path.Split("/");
            var regex = new Regex(@"^[a-zA-Z0-9_()-]+$");
            foreach (var dir in directories)
            {
                if (!regex.IsMatch(dir))
                    return false;
            }

            return true;
        }

        public async Task CreateFolderAsync(string remotePath)
        {
            logger.AddContext(LogContext.RemotePath, remotePath)
                .Debug("Google Drive: creating folder '{@remotePath}'", remotePath);

            if (IsValidPath(remotePath))
                await driveClient.CreateFolderAsync(remotePath);
            else
                logger.Error("Google Drive: Attempted to create invalid remote directory '{@remotePath}", remotePath);
        }
    }
}