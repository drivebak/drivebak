using System;
using Newtonsoft.Json;

namespace DriveBak.Drive.Auth
{
    public class AccessErrorConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) =>
            throw new NotImplementedException();

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var enumString = (string)reader.Value;

            if (string.IsNullOrEmpty(enumString))
                return null;

            enumString = enumString.Replace("_", "");

            return Enum.Parse(typeof(AccessError), enumString, true);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
    }
}