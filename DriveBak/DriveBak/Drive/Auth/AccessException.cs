using System;

namespace DriveBak.Drive.Auth
{
    public class AccessException : Exception
    {
        public AccessException(string message) : base(message) {}
    }
}