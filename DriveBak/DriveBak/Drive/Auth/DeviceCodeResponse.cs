using Google.Apis.Auth.OAuth2.Responses;

namespace DriveBak.Drive.Auth
{
    public class DeviceCodeResponse
    {
        public string DeviceCode { get; set; }
        public string UserCode { get; set; }
        public int ExpiresIn { get; set; }
        public int Interval { get; set; }
        public string VerificationUrl { get; set; }
        public string ErrorCode { get; set; }
    }
}