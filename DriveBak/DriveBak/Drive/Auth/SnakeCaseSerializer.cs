using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace DriveBak.Drive.Auth
{
    public class SnakeCaseSerializer : IDeserializer, ISerializer
    {
        private readonly JsonSerializerSettings settings;

        public SnakeCaseSerializer()
        {
            settings = new JsonSerializerSettings()
            {
                ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new SnakeCaseNamingStrategy(),
                }
            };
        }

        public T Deserialize<T>(IRestResponse response) =>
            JsonConvert.DeserializeObject<T>(response.Content, settings);

        public string Serialize(object obj) =>
            JsonConvert.SerializeObject(obj, settings);

        public string ContentType
        {
            get => "application/json";
            set { }
        }
    }
}