using System;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Logging;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util;
using Google.Apis.Util.Store;
using RestSharp;

namespace DriveBak.Drive.Auth
{
    public class DriveBakAuthorizationCodeFlow : IAuthorizationCodeFlow
    {
        private readonly ILogger<DriveAuth> logger;
        private readonly IOAuthRestHelper client;
        private readonly IGoogleOAuthSettings googleOAuthSettings;

        public IAccessMethod AccessMethod { get; }
        public IClock Clock { get; }
        public IDataStore DataStore { get; }

        public DriveBakAuthorizationCodeFlow(
            ILogger<DriveAuth> logger,
            IOAuthRestHelper client,
            IDataStore dataStore,
            IGoogleOAuthSettings googleOAuthSettings)
        {
            this.logger = logger;
            this.client = client;
            this.googleOAuthSettings = googleOAuthSettings;
            this.DataStore = dataStore;

            this.Clock = SystemClock.Default;
            this.AccessMethod = new BearerToken.AuthorizationHeaderAccessMethod();
        }

        public void Dispose() { }

        public async Task<TokenResponse> LoadTokenAsync(string userId, CancellationToken taskCancellationToken)
        {
            taskCancellationToken.ThrowIfCancellationRequested();
            if (DataStore == null)
            {
                return null;
            }

            return await DataStore.GetAsync<TokenResponse>(userId).ConfigureAwait(false);
        }

        public async Task DeleteTokenAsync(string userId, CancellationToken taskCancellationToken)
        {
            taskCancellationToken.ThrowIfCancellationRequested();
            if (DataStore != null)
            {
                await DataStore.DeleteAsync<TokenResponse>(userId).ConfigureAwait(false);
            }
        }

        public AuthorizationCodeRequestUrl CreateAuthorizationCodeRequest(string redirectUri)
        {
            return new AuthorizationCodeRequestUrl(new Uri(redirectUri))
            {
                ClientId = googleOAuthSettings.Secrets.ClientId,
                Scope = googleOAuthSettings.Scope,
            };
        }

        private async Task StoreTokenAsync(string userId, TokenResponse token, CancellationToken taskCancellationToken)
        {
            taskCancellationToken.ThrowIfCancellationRequested();
            if (DataStore != null)
            {
                await DataStore.StoreAsync<TokenResponse>(userId, token).ConfigureAwait(false);
            }
        }

        public async Task<TokenResponse> ExchangeCodeForTokenAsync(string userId, string code, string redirectUri,
            CancellationToken taskCancellationToken)
        {
            logger.Information("Auth: Token Request: Polling for access token");

            var waitSeconds = 30;

            while (!taskCancellationToken.IsCancellationRequested)
            {
                var access = await QueryAccess(userId, code, taskCancellationToken);
                if (access == null)
                {
                    var unexpectedErrorMessage = "Auth: Unexpected error occurred while requesting access";
                    logger.Error(unexpectedErrorMessage);
                    throw new AccessException(unexpectedErrorMessage);
                }

                switch (access.Error)
                {
                    case AccessError.SlowDown:
                        logger.Warning("Auth: Token request: Responded with slow_down");
                        waitSeconds += 10;
                        break;
                    case AccessError.AuthorizationPending:
                        logger.Debug("Auth: Token request: Responded with authorization_pending");
                        break;
                    case AccessError.AccessDenied:
                        logger.Error("Auth: Token request: Responded with access_denied");
                        throw new AccessException("User denied access");
                    case null:
                        logger.Debug("Auth: Token request: Responded with token");
                        var token = access.GetTokenResponse(DateTime.UtcNow, googleOAuthSettings.Scope);
                        await StoreTokenAsync(userId, token, taskCancellationToken).ConfigureAwait(false);
                        return token;
                    default:
                        var errorMessage =
                            $"Auth: Token request: Unexpected error response: {access.Error}, Error description: {access.ErrorDescription}";
                        logger.Error(errorMessage);
                        throw new ArgumentOutOfRangeException(errorMessage);
                }

                await Task.Delay(TimeSpan.FromSeconds(waitSeconds), taskCancellationToken);
            }

            var warningMessage = "Auth: Cancellation requested while attempting to authenticate.";
            logger.Warning(warningMessage);
            throw new AccessException(warningMessage);
        }

        private async Task<AccessResponse> QueryAccess(string clientId, string deviceCode, CancellationToken cancellationToken)
        {
            logger.Debug("Auth: Querying for token");
            var response = client.ExecuteRequestAsync<AccessResponse>(Method.POST, "token", new
            {
                client_id = clientId,
                client_secret = googleOAuthSettings.Secrets.ClientSecret,
                device_code = deviceCode,
                grant_type = "urn:ietf:params:oauth:grant-type:device_code",
            }, cancellationToken);

            return (await response).Data;
        }

        public async Task<TokenResponse> RefreshTokenAsync(string userId, string refreshToken, CancellationToken taskCancellationToken)
        {
            logger.Debug("Auth: Refreshing access token");
            IRestResponse<AccessResponse> response = null;

            try
            {
                response = await client.ExecuteRequestAsync<AccessResponse>(Method.POST, "token", new
                {
                    client_id = googleOAuthSettings.Secrets.ClientId,
                    client_secret = googleOAuthSettings.Secrets.ClientSecret,
                    refresh_token = refreshToken,
                    grant_type = "refresh_token",
                }, taskCancellationToken);

                if (response.Data == null)
                {
                    var unexpectedErrorMessage = "Auth: Unexpected error occurred while refreshing token";
                    logger.Error(unexpectedErrorMessage);
                    throw new AccessException(unexpectedErrorMessage);
                }

                var token = response.Data.GetTokenResponse(DateTime.UtcNow, googleOAuthSettings.Scope);

                if (string.IsNullOrEmpty(token.RefreshToken))
                    token.RefreshToken = refreshToken;

                await StoreTokenAsync(userId, token, taskCancellationToken).ConfigureAwait(false);

                return token;
            }
            catch (Exception)
            {
                var statusCode = (int) (response?.StatusCode ?? 0);
                if (!(statusCode >= 500 && statusCode < 600)) // Server error
                    await DeleteTokenAsync(userId, taskCancellationToken).ConfigureAwait(false);
                throw;
            }
        }

        public async Task RevokeTokenAsync(string userId, string token, CancellationToken taskCancellationToken)
        {
            await DeleteTokenAsync(userId, taskCancellationToken).ConfigureAwait(false);
        }

        public bool ShouldForceTokenRetrieval() => false;
    }
}