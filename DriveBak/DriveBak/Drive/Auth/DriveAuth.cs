using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Logging;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util.Store;
using RestSharp;

namespace DriveBak.Drive.Auth
{
    public interface IDriveAuth
    {
        Task<DeviceCodeResponse> GetDeviceCodes(CancellationToken cancellationToken);
        Task<UserCredential> GetUserCredentialsAsync(CancellationToken cancellationToken);
    }

    public class DriveAuth : IDriveAuth
    {
        private readonly IGoogleOAuthSettings googleOAuthSettings;
        private readonly ILogger<DriveAuth> logger;
        private readonly IOAuthRestHelper client;
        private readonly IDataStore dataStore;
        private readonly IAuthorizationCodeFlow codeFlow;

        public DriveAuth(IGoogleOAuthSettings googleOAuthSettings,
            ILogger<DriveAuth> logger,
            IOAuthRestHelper client,
            IDataStore dataStore,
            IAuthorizationCodeFlow codeFlow)
        {
            this.googleOAuthSettings = googleOAuthSettings;
            this.logger = logger;
            this.client = client;
            this.dataStore = dataStore;
            this.codeFlow = codeFlow;
        }

        public async Task<DeviceCodeResponse> GetDeviceCodes(CancellationToken cancellationToken)
        {
            logger.Debug("Auth: Requesting device code");
            var response = client.ExecuteRequestAsync<DeviceCodeResponse>(Method.POST, "device/code", new
            {
                client_id = googleOAuthSettings.Secrets.ClientId,
                scope = googleOAuthSettings.Scope,
            }, cancellationToken);

            return (await response)?.Data;
        }

        private Task WriteDeviceCodesToDataStore(DeviceCodeResponse deviceCodeResponse)
        {
            var deviceTask = dataStore.StoreAsync($"user_code:{googleOAuthSettings.User}", deviceCodeResponse.UserCode);
            var urlTask = dataStore.StoreAsync($"verification_url:{googleOAuthSettings.User}", deviceCodeResponse.VerificationUrl);
            return Task.WhenAll(deviceTask, urlTask);
        }

        public async Task<UserCredential> GetUserCredentialsAsync(CancellationToken cancellationToken)
        {
            logger.Information("Auth: Starting Google OAuth2 process");

            var token = await codeFlow.LoadTokenAsync(googleOAuthSettings.Secrets.ClientId, cancellationToken);

            if (ShouldRequestAuthorizationCode(codeFlow, token))
            {
                var deviceCodes = GetDeviceCodes(cancellationToken);
                await WriteDeviceCodesToDataStore(await deviceCodes);
                token = await codeFlow.ExchangeCodeForTokenAsync(googleOAuthSettings.Secrets.ClientId,
                    (await deviceCodes).DeviceCode,
                    "",
                    cancellationToken);
            }

            logger.Information("Auth: Completed Google OAuth2 process");

            return new UserCredential(codeFlow, googleOAuthSettings.User, token);
        }

        private bool ShouldRequestAuthorizationCode(IAuthorizationCodeFlow flow, TokenResponse token)
        {
            return flow.ShouldForceTokenRetrieval()
                   || token == null
                   || (string.IsNullOrEmpty(token.RefreshToken) && token.IsExpired(flow.Clock));
        }
    }
}