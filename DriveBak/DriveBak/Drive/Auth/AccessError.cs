namespace DriveBak.Drive.Auth
{
    public enum AccessError
    {
        AccessDenied = 0,
        AuthorizationPending = 1,
        SlowDown = 2,
    }
}