using System;
using Google.Apis.Auth.OAuth2.Responses;
using Newtonsoft.Json;

namespace DriveBak.Drive.Auth
{
    public class AccessResponse
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
        public string RefreshToken { get; set; }
        public string TokenType { get; set; }

        [JsonConverter(typeof(AccessErrorConverter))]
        public AccessError? Error { get; set; }

        public string ErrorDescription { get; set; }

        public TokenResponse GetTokenResponse(DateTime issuedUtc, string scope) => new TokenResponse
            {
                AccessToken = AccessToken,
                ExpiresInSeconds = ExpiresIn,
                IssuedUtc = issuedUtc,
                RefreshToken = RefreshToken,
                Scope = scope,
                TokenType = TokenType,
            };
    }
}