using System;
using System.Threading.Tasks;
using DriveBak.Cache;
using Google.Apis.Util.Store;
using Newtonsoft.Json;

namespace DriveBak.Drive.Auth
{
    public class RedisDataStore : IDataStore
    {
        private readonly IRedisSettings redisSettings;
        private readonly IRedisConnection redisConnection;

        public RedisDataStore(IRedisSettings redisSettings, IRedisConnection redisConnection)
        {
            this.redisSettings = redisSettings;
            this.redisConnection = redisConnection;
        }

        private string GetRedisKey(string key) => $"drive:auth:{key}";

        public Task StoreAsync<T>(string key, T value)
        {
            key = GetRedisKey(key);
            var data = JsonConvert.SerializeObject(value);
            return redisConnection.Database.StringSetAsync(key, data);
        }

        public Task DeleteAsync<T>(string key)
        {
            key = GetRedisKey(key);
            return redisConnection.Database.KeyDeleteAsync(key);
        }

        public async Task<T> GetAsync<T>(string key)
        {
            key = GetRedisKey(key);
            var value = redisConnection.Database.StringGetAsync(key);

            try
            {
                return JsonConvert.DeserializeObject<T>(await value);
            }
            catch (Exception)
            {
                return default;
            }
        }

        public Task ClearAsync()
        {
            var key = GetRedisKey("*");
            return redisConnection.Database.KeyDeleteAsync(key);
        }
    }
}