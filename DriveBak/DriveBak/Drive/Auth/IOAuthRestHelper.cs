using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace DriveBak.Drive.Auth
{
    public interface IOAuthRestHelper
    {
        Task<IRestResponse<T>> ExecuteRequestAsync<T>(Method method, string path, object body, CancellationToken cancellationToken);
    }

    public class OAuthRestHelper : IOAuthRestHelper
    {
        private readonly RestClient client;

        public OAuthRestHelper()
        {
            client = new RestClient("https://oauth2.googleapis.com");
        }

        public Task<IRestResponse<T>> ExecuteRequestAsync<T>(Method method, string path, object body, CancellationToken cancellationToken)
        {
            var request = new RestRequest(path, method);
            if (body != null)
                request.AddJsonBody(body);

            var serializer = new SnakeCaseSerializer();
            request.JsonSerializer = serializer;
            client.ClearHandlers();
            client.AddHandler(() => serializer, "application/json");

            return client.ExecuteAsync<T>(request, cancellationToken);
        }

    }
}