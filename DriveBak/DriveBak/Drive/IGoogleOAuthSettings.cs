using System.IO;
using Google.Apis.Auth.OAuth2;

namespace DriveBak.Drive
{
    public interface IGoogleOAuthSettings
    {
        string CredentialsFile { get; set; }
        ClientSecrets Secrets { get; }
        string[] Scopes { get; }
        string Scope { get; }
        string User { get; set; }
    }

    public class GoogleOAuthSettings : IGoogleOAuthSettings
    {
        public string CredentialsFile { get; set; }
        public string User { get; set; }
        public string[] Scopes => new [] {"openid", "profile", "email", "https://www.googleapis.com/auth/drive.file"};
        public string Scope => string.Join(" ", Scopes);

        private ClientSecrets secrets;
        public ClientSecrets Secrets
        {
            get
            {
                if (secrets == null)
                {
                    using (var stream = new FileStream(CredentialsFile, FileMode.Open, FileAccess.Read))
                    {
                        secrets = GoogleClientSecrets.Load(stream).Secrets;
                    }
                }

                return secrets;
            }
        }
    }
}
