namespace DriveBak
{
    public class BackupDirectory
    {
        public string Application { get; set; }
        public string LocalPath { get; set; }
        public string RemotePath { get; set; }
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as BackupDirectory;
            if (other == null)
                return false;

            return this.Application == other.Application &&
                   this.LocalPath == other.LocalPath &&
                    this.RemotePath == other.RemotePath &&
                    this.Description == other.Description;
        }

        public override int GetHashCode() =>
            (Application, LocalPath, RemotePath).GetHashCode();

        public override string ToString() =>
            $"App: {Application}, Local: {LocalPath}, Remote: {RemotePath}, Description: {Description}";
    }
}