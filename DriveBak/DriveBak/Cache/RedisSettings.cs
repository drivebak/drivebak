namespace DriveBak.Cache
{
    public interface IRedisSettings
    {
        string Host { get; set; }
        int Port { get; set; }
        string Password { get; set; }
        int CacheLengthSeconds { get; set; }
    }

    public class RedisSettings : IRedisSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public int CacheLengthSeconds { get; set; }
    }
}