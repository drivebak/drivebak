using System.Collections.Generic;
using System.Linq;
using DriveBak.Logging;
using StackExchange.Redis;

namespace DriveBak.Cache
{
    public interface IRedisConnection
    {
        IDatabase Database { get; }
        IEnumerable<IServer> Servers { get; }
    }

    public class RedisConnection : IRedisConnection
    {
        private readonly IRedisSettings redisSettings;
        private readonly ILogger<RedisConnection> logger;

        private IConnectionMultiplexer connection;
        private IConnectionMultiplexer Connection
        {
            get
            {
                if (connection == null)
                {
                    logger.Debug("Redis: Creating connection");
                    connection = ConnectionMultiplexer.Connect(new ConfigurationOptions
                        {
                            AllowAdmin = false,
                            Password = redisSettings.Password,
                            EndPoints =
                            {
                                {redisSettings.Host, redisSettings.Port},
                            },
                        }
                    );
                }

                return connection;

            }
        }

        public IDatabase Database => Connection.GetDatabase();

        public IEnumerable<IServer> Servers =>
            Connection.GetEndPoints().Select(e => Connection.GetServer(e));

        public RedisConnection(IRedisSettings redisSettings, ILogger<RedisConnection> logger)
        {
            this.redisSettings = redisSettings;
            this.logger = logger;
        }
    }
}