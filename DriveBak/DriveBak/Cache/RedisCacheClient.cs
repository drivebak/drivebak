using System;
using System.Linq;
using System.Threading.Tasks;
using DriveBak.Logging;
using Serilog;

namespace DriveBak.Cache
{
    public class RedisCacheClient : ICacheClient
    {
        private readonly IRedisConnection redisConnection;
        private readonly IRedisSettings redisSettings;
        private readonly ILogger<RedisCacheClient> logger;

        public RedisCacheClient(IRedisConnection redisConnection,
            IRedisSettings redisSettings,
            ILogger<RedisCacheClient> logger)
        {
            this.redisConnection = redisConnection;
            this.redisSettings = redisSettings;
            this.logger = logger;
        }

        public string Name => "Redis";
        public string Version =>
            redisConnection.Servers
                .Where(s => s.IsConnected)
                .Select(s => s.Version.ToString())
                .FirstOrDefault();

        public bool TryGet(string key, out string value)
        {
            logger.Debug("{@NAME}: Querying for key '{@key}", Name, key);
            value =  redisConnection.Database.StringGet(key);
            if (!string.IsNullOrEmpty(value))
                return true;

            return false;
        }

        public async Task SetAsync(string key, string value)
        {
            logger.Debug("{@Name}: setting key '{@key} to value '{@value}", Name, key, value);
            await redisConnection.Database.StringSetAsync(key, value,
                TimeSpan.FromSeconds(redisSettings.CacheLengthSeconds));
        }
    }
}