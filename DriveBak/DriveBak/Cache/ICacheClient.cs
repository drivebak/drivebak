using System.Threading.Tasks;

namespace DriveBak.Cache
{
    public interface ICacheClient
    {
        string Name { get; }
        string Version { get; }
        bool TryGet(string key, out string value);
        Task SetAsync(string key, string value);
    }
}