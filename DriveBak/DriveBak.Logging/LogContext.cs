namespace DriveBak.Logging
{
    public enum LogContext
    {
        AppId =  1,
        AppName = 2,
        DirId = 3,
        DirPath = 4,
        RemotePath = 5,
        FileName = 6,
    }
}