using Serilog;

namespace DriveBak.Logging
{
    public interface ILogger<T>
    {
        ILogger<T> AddContext<T1>();
        ILogger<T> AddContext(LogContext key, string value);
        void Verbose(string message, params object[] values);
        void Debug(string message, params object[] values);
        void Information(string message, params object[] values);
        void Warning(string message, params object[] values);
        void Error(string message, params object[] values);
        void Fatal(string message, params object[] values);
    }

    public class Logger<T> : ILogger<T>
    {
        private readonly ILogger logger;

        public Logger(ILogger logger)
        {
            this.logger = logger.ForContext<T>();
        }

        public ILogger<T> AddContext(LogContext key, string value)
        {
            logger.ForContext(key.ToString(), value);
            return this;
        }

        public ILogger<T> AddContext<T1>()
        {
            logger.ForContext<T1>();
            return this;
        }

        public void Verbose(string message, params object[] values) =>
            logger.Verbose(message, values);

        public void Debug(string message, params object[] values) =>
            logger.Debug(message, values);

        public void Information(string message, params object[] values) =>
            logger.Information(message, values);

        public void Warning(string message, params object[] values) =>
            logger.Warning(message, values);

        public void Error(string message, params object[] values) =>
            logger.Error(message, values);

        public void Fatal(string message, params object[] values) =>
            logger.Fatal(message, values);
    }
}