using Serilog.Events;

namespace DriveBak.Logging
{
    public enum Level
    {
        Verbose = 1,
        Debug = 2,
        Information = 3,
        Warning = 4,
        Error = 5,
        Fatal = 6,
    }

    public static class LevelMethods
    {
        public static LogEventLevel ToSerilog(this Level level)
        {
            switch (level)
            {
                case Level.Verbose:
                    return LogEventLevel.Verbose;
                case Level.Debug:
                    return LogEventLevel.Debug;
                case Level.Information:
                    return LogEventLevel.Information;
                case Level.Warning:
                    return LogEventLevel.Warning;
                case Level.Error:
                    return LogEventLevel.Error;
                case Level.Fatal:
                    return LogEventLevel.Fatal;
                default:
                    return LogEventLevel.Information;
            }
        }
    }
}