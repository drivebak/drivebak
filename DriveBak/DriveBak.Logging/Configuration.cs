using Serilog;
using Serilog.Events;
using Serilog.Filters;
using Serilog.Formatting.Compact;

namespace DriveBak.Logging
{
    public class Configuration
    {
        public static LoggerConfiguration BuildLoggerConfiguration(ILogSettings settings)
        {
            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Is(settings.Level.ToSerilog())
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                // Filter out ASP.NET Core Infrastructure Information and below Logs
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .Enrich.FromLogContext();
            if (settings.ConsoleEnabled)
                loggerConfiguration.WriteTo.Console();

            return loggerConfiguration;
        }
    }

    public static class LoggerConfigurationExtensions
    {
        public static LoggerConfiguration ConfigureAppLogging(this LoggerConfiguration loggerConfiguration,
            ILogSettings settings,
            params string[] exclusions)
        {
            if (!string.IsNullOrEmpty(settings.AppLogPath))
            {
                loggerConfiguration
                    .WriteTo.Logger(lc =>
                        {
                            foreach (var exclusion in exclusions)
                                lc.Filter.ByExcluding(Matching.FromSource(exclusion));

                            lc.WriteTo.File(new RenderedCompactJsonFormatter(), settings.AppLogPath);
                        }
                    );
            }

            return loggerConfiguration;
        }

        public static LoggerConfiguration ConfigureWebLogging(this LoggerConfiguration loggerConfiguration,
            ILogSettings settings,
            params string[] inclusions)
        {
            if (!string.IsNullOrEmpty(settings.WebLogPath))
            {
                loggerConfiguration
                    .WriteTo.Logger(lc => lc
                        .Filter.ByIncludingOnly(e =>
                        {
                            var include = false;
                            foreach (var inclusion in inclusions)
                                include = include || Matching.FromSource(inclusion)(e);

                            return include;
                        })
                        .WriteTo.File(new RenderedCompactJsonFormatter(), settings.WebLogPath)
                    );
            }

            return loggerConfiguration;
        }
    }
}