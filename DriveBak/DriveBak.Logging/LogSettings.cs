namespace DriveBak.Logging
{
    public interface ILogSettings
    {
        Level Level { get; set; }
        string WebLogPath { get; set; }
        string AppLogPath { get; set; }
        bool ConsoleEnabled { get; set; }
    }

    public class LogSettings : ILogSettings
    {
        public Level Level { get; set; }
        public string WebLogPath { get; set; }
        public string AppLogPath { get; set; }
        public bool ConsoleEnabled { get; set; }
    }
}