using System;
using System.Collections.Generic;
using System.Linq;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using NUnit.Framework;

namespace DriveBak.Test.Integration.DB
{
    public class WhenConstructing
    {
        [Test]
        public void ShouldRaiseExceptionIfSettingsIsNull()
        {
            Assert.Throws(typeof(ArgumentException), () => new PostgresClient(null, new TestLogger<PostgresClient>()));
        }
    }

    public class BasePostgresClientTests
    {
        protected PostgresClient postgresClient;
        protected string tableName;

        [SetUp]
        public void SetUp()
        {
            var postgresSettings = new IntegrationTestSettings().PostgresSettings;
            postgresClient = new PostgresClient(postgresSettings, new TestLogger<PostgresClient>());
            tableName = "t" + Guid.NewGuid().ToString().Replace("-", "");

            var createTableCommand = $@"CREATE TABLE IF NOT EXISTS {tableName}
(
    id      serial PRIMARY KEY,
    name    varchar UNIQUE NOT NULL,
    created timestamp default now(),
    updated timestamp default now()
)";
            postgresClient.Write(createTableCommand);
        }

        [TearDown]
        public void TearDown()
        {
            var dropTableCommand = $@"DROP TABLE {tableName} CASCADE";
            postgresClient.Write(dropTableCommand);
        }
    }

    public class WhenTableIsEmpty : BasePostgresClientTests
    {
        [Test]
        public void ReadShouldReturnEmpty() =>
            Assert.IsEmpty(postgresClient.Read<int>($"SELECT id from {tableName}"));
    }

    public class WhenWritingAndReading : BasePostgresClientTests
    {
        private IEnumerable<(int, string)> results;
        private string name;

        [SetUp]
        public void Setup()
        {
            name = Guid.NewGuid().ToString();
            var insertCommand = $@"INSERT INTO {tableName} (name) values (@name)";
            postgresClient.Write(insertCommand, new {name});
            var selectCommand = $@"SELECT id as item1, name as item2 FROM {tableName} where name=@name";
            results = postgresClient.Read<(int, string)>(selectCommand, new {name});
        }

        [Test]
        public void ShouldReturnSingle() =>
            Assert.NotNull(results.SingleOrDefault());

        [Test]
        public void IdShouldBeGreaterThan0() =>
            Assert.Greater(results.Single().Item1, 0);

        [Test]
        public void NameShouldMatch() =>
            Assert.AreEqual(name, results.Single().Item2);
    }

    public class WhenWritingWithReturnValue : BasePostgresClientTests
    {
        private int selectedId;
        private string name;
        private int id;

        [SetUp]
        public void Setup()
        {
            name = Guid.NewGuid().ToString();
            var insertCommand = $@"INSERT INTO {tableName} (name) values (@name) RETURNING id";
            id = postgresClient.Write<int>(insertCommand, new {name});
            var selectCommand = $@"SELECT id FROM {tableName} where name=@name";
            selectedId = postgresClient.Read<int>(selectCommand, new {name}).Single();
        }

        [Test]
        public void ReturnShouldMatchSelected() =>
            Assert.AreEqual(selectedId, id);
    }

    public class WhenWritingAndReadingMultiple : BasePostgresClientTests
    {
        private IEnumerable<(int, string)> results;
        private string[] names;

        [SetUp]
        public void Setup()
        {
            names = new string[]
            {
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
            };
            foreach (var name in names)
            {
                var insertCommand = $@"INSERT INTO {tableName} (name) values (@name)";
                postgresClient.Write(insertCommand, new {name});
            }

            var selectCommand = $@"SELECT id as item1, name as item2 FROM {tableName}";
            results = postgresClient.Read<(int, string)>(selectCommand);
        }

        [Test]
        public void ShouldReturnThreeValues() =>
            Assert.AreEqual(3, results.Count());

        [Test]
        public void IdsShouldBeGreaterThan0() =>
            Assert.That(results.All(r => r.Item1 > 0));

        [Test]
        public void NamesShouldMatch() =>
            CollectionAssert.AreEquivalent(names, results.Select(r => r.Item2));
    }

    public class WhenDeleting : BasePostgresClientTests
    {
        [SetUp]
        public void Setup()
        {
            var name = Guid.NewGuid().ToString();
            var insertCommand = $@"INSERT INTO {tableName} (name) values (@name)";
            postgresClient.Write(insertCommand, new {name});
            var deleteCommand = $@"DELETE FROM {tableName} WHERE name=@name";
            postgresClient.Write(deleteCommand, new {name});
        }

        [Test]
        public void ReadShouldReturnEmpty() =>
            Assert.IsEmpty(postgresClient.Read<int>($"SELECT id from {tableName}"));
    }

    public class WhenGettingVersion : BasePostgresClientTests
    {
        [Test]
        public void ShouldNotBeEmpty() => Assert.IsNotEmpty(postgresClient.Version);
    }
}