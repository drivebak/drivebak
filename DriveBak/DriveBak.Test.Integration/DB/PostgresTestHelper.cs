using System.Linq;
using DriveBak.DB;

namespace DriveBak.Test.Integration.DB
{
    class PostgresTestHelper
    {
        private readonly PostgresClient postgresClient;

        public PostgresTestHelper(PostgresClient postgresClient)
        {
            this.postgresClient = postgresClient;
        }

        public int WriteApplication(string name) => WriteApplication(new Application() {Name = name});
        public int WriteApplication(Application application)
        {
            var writeQuery = @"INSERT INTO applications (name, description) values (@Name, @Description) RETURNING id";
            return postgresClient.Write<int>(writeQuery, application);
        }

        public int? GetApplicationId(Application application) => GetApplicationId(application.Name);
        public int? GetApplicationId(string application)
        {
            var readQuery = @"SELECT id from applications where name = @name";
            return postgresClient.Read<int>(readQuery, new {name=application}).SingleOrDefault();
        }

        public Application GetApplication(int appId)
        {
            var readQuery = @"SELECT name, description from applications where id = @id";
            return postgresClient.Read<Application>(readQuery, new {id=appId}).SingleOrDefault();
        }

        public int WriteDirectory(int appId, BackupDirectory directory)
        {
            var writeQuery = @"INSERT INTO directories (application_id, description, local_path, remote_path)
                               values (@appId, @description, @localPath, @remotePath)
                               RETURNING id";
            return postgresClient.Write<int>(writeQuery, new
            {
                appId,
                description=directory.Description,
                localPath=directory.LocalPath,
                remotePath=directory.RemotePath,
            });
        }

        public BackupDirectory GetDirectory(string localPath)
        {
            var readQuery = @"SELECT name as application, directories.description as description, local_path, remote_path
                            FROM directories 
                            LEFT JOIN applications a on directories.application_id = a.id
                            WHERE local_path=@localPath";
            return postgresClient.Read<BackupDirectory>(readQuery, new {localPath}).SingleOrDefault();
        }

        public int GetDirectoryId(string localPath)
        {
            var readQuery = @"SELECT id from directories where local_path=@localPath";
            return postgresClient.Read<int>(readQuery, new {localPath}).SingleOrDefault();
        }

        public void DeleteDirectory(string localPath)
        {
            var deleteDirQuery = @"DELETE FROM directories where local_path=@localPath";
            postgresClient.Write(deleteDirQuery, new {localPath});
        }


        public void DeleteApp(Application application) => DeleteApp(application.Name);
        public void DeleteApp(string name)
        {
            var deleteAppQuery = @"DELETE FROM applications where name=@name";
            postgresClient.Write(deleteAppQuery, new {name});
        }
    }
}