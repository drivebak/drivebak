using System;
using System.Collections.Generic;
using System.Linq;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using NUnit.Framework;

namespace DriveBak.Test.Integration.DB.ApplicationManagerTests
{
    public class WhenAddingBackupDirectoryAndApplicationDoesExist
    {
        private BackupDirectory expectedDirectory;
        private PostgresTestHelper postgresTestHelper;
        private int actualId;

        [SetUp]
        public void Setup()
        {
            expectedDirectory = TestHelpers.GetBackupDirectory();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);
            var appId = postgresTestHelper.WriteApplication(expectedDirectory.Application);
            actualId = applicationsManager.AddDirectory(appId, expectedDirectory);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(expectedDirectory.LocalPath);
            postgresTestHelper.DeleteApp(expectedDirectory.Application);
        }

        [Test]
        public void ShouldReturnValidId() =>
            Assert.Greater(actualId, 0);

        [Test]
        public void ShouldReturnCorrectId() =>
            Assert.AreEqual(postgresTestHelper.GetDirectoryId(expectedDirectory.LocalPath), actualId);

        [Test]
        public void ShouldInsertDirectory() =>
            Assert.AreEqual(expectedDirectory, postgresTestHelper.GetDirectory(expectedDirectory.LocalPath));
    }

    public class WhenAddingBackupDirectoryWithNoDescription
    {
        private BackupDirectory expectedDirectory;
        private PostgresTestHelper postgresTestHelper;
        private int actualId;

        [SetUp]
        public void Setup()
        {
            expectedDirectory = TestHelpers.GetBackupDirectory();
            expectedDirectory.Description = null;
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);
            var appId = postgresTestHelper.WriteApplication(expectedDirectory.Application);
            actualId = applicationsManager.AddDirectory(appId, expectedDirectory);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(expectedDirectory.LocalPath);
            postgresTestHelper.DeleteApp(expectedDirectory.Application);
        }

        [Test]
        public void ShouldReturnValidId() =>
            Assert.Greater(actualId, 0);

        [Test]
        public void ShouldReturnCorrectId() =>
            Assert.AreEqual(postgresTestHelper.GetDirectoryId(expectedDirectory.LocalPath), actualId);

        [Test]
        public void ShouldInsertDirectory() =>
            Assert.AreEqual(expectedDirectory, postgresTestHelper.GetDirectory(expectedDirectory.LocalPath));
    }


    public class WhenAddingBackupDirectoryAndApplicationDoesNotExist
    {
        private BackupDirectory backupDirectory;
        private PostgresTestHelper postgresTestHelper;
        private int actualId;

        [SetUp]
        public void Setup()
        {
            backupDirectory = TestHelpers.GetBackupDirectory();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);
            actualId = applicationsManager.AddDirectory(0, backupDirectory);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);
            postgresTestHelper.DeleteApp(backupDirectory.Application);
        }

        [Test]
        public void ShouldNotInsertApplication() =>
            Assert.AreEqual(0, postgresTestHelper.GetApplicationId(backupDirectory.Application));

        [Test]
        public void ShouldNotInsertDirectory() =>
            Assert.AreEqual(0, postgresTestHelper.GetDirectoryId(backupDirectory.LocalPath));

        [Test]
        public void ShouldReturnId0() =>
            Assert.AreEqual(0, actualId);
    }

    public class WhenGettingDirectory
    {
        private BackupDirectory extraDirectory;
        private BackupDirectory expectedDirectory;
        private IEnumerable<BackupDirectory> dirs;
        private PostgresTestHelper postgresTestHelper;

        [SetUp]
        public void Setup()
        {
            var app = TestHelpers.GetApplication();
            expectedDirectory = TestHelpers.GetBackupDirectory(app);
            extraDirectory = TestHelpers.GetBackupDirectory(app);
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            var appId = postgresTestHelper.WriteApplication(expectedDirectory.Application);
            postgresTestHelper.WriteDirectory(appId, expectedDirectory);
            postgresTestHelper.WriteDirectory(appId, extraDirectory);
            dirs = applicationsManager.GetDirectory(expectedDirectory.LocalPath);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(expectedDirectory.LocalPath);
            postgresTestHelper.DeleteDirectory(extraDirectory.LocalPath);
            postgresTestHelper.DeleteApp(expectedDirectory.Application);
        }

        [Test]
        public void ShouldReturnTheSingleDirectory() =>
            Assert.AreEqual(expectedDirectory, dirs.Single());
    }

    public class WhenGettingNonExistentDirectory
    {
        private BackupDirectory backupDirectory;
        private BackupDirectory backupDirectory2;
        private IEnumerable<BackupDirectory> dirs;
        private PostgresTestHelper postgresTestHelper;

        [SetUp]
        public void Setup()
        {
            backupDirectory = TestHelpers.GetBackupDirectory();
            backupDirectory2 = TestHelpers.GetBackupDirectory();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            var appId = postgresTestHelper.WriteApplication(backupDirectory.Application);
            postgresTestHelper.WriteDirectory(appId, backupDirectory);
            postgresTestHelper.WriteDirectory(appId, backupDirectory2);
            dirs = applicationsManager.GetDirectory("path");
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);
            postgresTestHelper.DeleteDirectory(backupDirectory2.LocalPath);
            postgresTestHelper.DeleteApp(backupDirectory.Application);
        }

        [Test]
        public void ShouldReturnEmpty() =>
            Assert.IsEmpty(dirs);
    }

    public class WhenGettingDirectoryAndThereAreNoRecords
    {
        private IEnumerable<BackupDirectory> dirs;

        [SetUp]
        public void Setup()
        {
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            dirs = applicationsManager.GetDirectory("path");
        }

        [Test]
        public void ShouldReturnEmpty() =>
            Assert.IsEmpty(dirs);
    }

    public class WhenGettingDirectoryId
    {
        private BackupDirectory backupDirectory2;
        private BackupDirectory backupDirectory;
        private PostgresTestHelper postgresTestHelper;
        private int expectedId;
        private int actualId;

        [SetUp]
        public void Setup()
        {
            var app = TestHelpers.GetApplication();
            backupDirectory = TestHelpers.GetBackupDirectory(app);
            backupDirectory2 = TestHelpers.GetBackupDirectory(app);
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            var appId = postgresTestHelper.WriteApplication(backupDirectory.Application);
            expectedId = postgresTestHelper.WriteDirectory(appId, backupDirectory);
            postgresTestHelper.WriteDirectory(appId, backupDirectory2);
            actualId = applicationsManager.GetDirectoryId(backupDirectory.LocalPath);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);
            postgresTestHelper.DeleteDirectory(backupDirectory2.LocalPath);
            postgresTestHelper.DeleteApp(backupDirectory.Application);
        }

        [Test]
        public void ShouldReturnTheId() =>
            Assert.AreEqual(expectedId, actualId);
    }

    public class WhenGettingNonexistentDirectoryId
    {
        private BackupDirectory backupDirectory;
        private PostgresTestHelper postgresTestHelper;
        private double actual;

        [SetUp]
        public void Setup()
        {
            backupDirectory = TestHelpers.GetBackupDirectory();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            var appId = postgresTestHelper.WriteApplication(backupDirectory.Application);
            postgresTestHelper.WriteDirectory(appId, backupDirectory);
            actual = applicationsManager.GetDirectoryId(Guid.NewGuid().ToString());
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);
            postgresTestHelper.DeleteApp(backupDirectory.Application);
        }

        [Test]
        public void ShouldReturnZero() =>
            Assert.AreEqual(0, actual);
    }

    public class WhenRemovingDirectory
    {
        private BackupDirectory backupDirectory;
        private PostgresTestHelper postgresTestHelper;
        private BackupDirectory backupDirectory2;

        [SetUp]
        public void Setup()
        {
            backupDirectory = TestHelpers.GetBackupDirectory();
            backupDirectory2 = TestHelpers.GetBackupDirectory();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            var appId = postgresTestHelper.WriteApplication(backupDirectory.Application);
            var directoryId = postgresTestHelper.WriteDirectory(appId, backupDirectory);
            postgresTestHelper.WriteDirectory(appId, backupDirectory2);

            applicationsManager.RemoveDirectory(directoryId);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);
            postgresTestHelper.DeleteDirectory(backupDirectory2.LocalPath);
            postgresTestHelper.DeleteApp(backupDirectory.Application);
        }

        [Test]
        public void ShouldRemoveDirectory() =>
            Assert.AreEqual(0, postgresTestHelper.GetDirectoryId(backupDirectory.LocalPath));

        [Test]
        public void ShouldNotRemoveOtherDirectories() =>
            Assert.Greater(postgresTestHelper.GetDirectoryId(backupDirectory2.LocalPath), 0);
    }

    public class WhenGettingAllDirectoriesFromApplication
    {
        private List<BackupDirectory> backupDirectories;
        private List<BackupDirectory> expectedDirectories;
        private PostgresTestHelper postgresTestHelper;
        private IEnumerable<BackupDirectory> actualDirectories;
        private Application[] applications;

        [SetUp]
        public void Setup()
        {
            backupDirectories = new List<BackupDirectory>();
            expectedDirectories = new List<BackupDirectory>();

            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            applications = TestHelpers.GetApplications(2).ToArray();
            expectedDirectories = TestHelpers.GetBackupDirectories(10, applications.First()).ToList();
            backupDirectories = expectedDirectories
                .Concat(TestHelpers.GetBackupDirectories(10, applications.Last())).ToList();
            foreach (var application in applications)
                postgresTestHelper.WriteApplication(application);
            foreach (var directory in backupDirectories)
            {
                var appId = postgresTestHelper.GetApplicationId(directory.Application);
                postgresTestHelper.WriteDirectory(appId.Value, directory);
            }
            actualDirectories = applicationsManager.GetApplicationDirectories(applications[0].Name);
        }

        [TearDown]
        public void Teardown()
        {
            foreach (var backupDirectory in backupDirectories)
                postgresTestHelper.DeleteDirectory(backupDirectory.LocalPath);

            foreach (var application in applications)
                postgresTestHelper.DeleteApp(application);
        }

        [Test]
        public void ShouldReturnSingleApplicationsDirectories() =>
            CollectionAssert.AreEquivalent(expectedDirectories, actualDirectories);
    }

    public class WhenGettingAllDirectoriesFromApplicationThatDoesNotExist
    {
        private IEnumerable<BackupDirectory> result;

        [SetUp]
        public void Setup()
        {
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());

            result = applicationsManager.GetApplicationDirectories(Guid.NewGuid().ToString());
        }

        [Test]
        public void ShouldReturnEmpty() =>
            Assert.IsEmpty(result);
    }
}