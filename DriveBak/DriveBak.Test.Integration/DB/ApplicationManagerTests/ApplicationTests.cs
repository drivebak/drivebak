using System;
using System.Collections.Generic;
using System.Linq;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using NUnit.Framework;

namespace DriveBak.Test.Integration.DB.ApplicationManagerTests
{
    public class WhenAddingAnApplication
    {
        private PostgresTestHelper postgresTestHelper;
        private Application expected;
        private Application actual;
        private int appId;

        [SetUp]
        public void Setup()
        {
            expected = TestHelpers.GetApplication();
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);
            appId = applicationsManager.AddApplication(expected);
            actual = postgresTestHelper.GetApplication(appId);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteApp(expected);
        }

        [Test]
        public void ShouldInsertApplication() =>
            Assert.AreEqual(expected, actual);

        [Test]
        public void ShouldReturnNewAppId() =>
            Assert.AreEqual(postgresTestHelper.GetApplicationId(expected), appId);
    }

    public class WhenGettingAnApplication
    {
        private PostgresTestHelper postgresTestHelper;
        private Application expectedApplication;
        private Application actualApplication;

        [SetUp]
        public void Setup()
        {
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            expectedApplication = TestHelpers.GetApplication();
            postgresTestHelper.WriteApplication(expectedApplication);

            actualApplication = applicationsManager.GetApplication(expectedApplication.Name);
        }

        [TearDown]
        public void Teardown()
        {
            postgresTestHelper.DeleteApp(expectedApplication);
        }

        [Test]
        public void ShouldReturnExpectedApplication() =>
            Assert.AreEqual(expectedApplication, actualApplication);
    }

    public class WhenGettingAnApplicationThatDoesNotExist
    {
        private Application actualApplication;

        [SetUp]
        public void Setup()
        {
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());

            actualApplication = applicationsManager.GetApplication(Guid.NewGuid().ToString());
        }

        [Test]
        public void ShouldReturnNull() =>
            Assert.IsNull(actualApplication);
    }

    public class WhenGettingAllApplications
    {
        private PostgresTestHelper postgresTestHelper;
        private List<Application> expectedApplications;
        private IEnumerable<Application> actualApplications;

        [SetUp]
        public void Setup()
        {
            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            expectedApplications = TestHelpers.GetApplications(5).ToList();
            foreach (var application in expectedApplications)
                postgresTestHelper.WriteApplication(application);

            actualApplications = applicationsManager.GetAllApplications();
        }

        [TearDown]
        public void Teardown()
        {
            foreach (var application in expectedApplications)
                postgresTestHelper.DeleteApp(application);
        }

        [Test]
        public void ShouldReturnAllApplications()
        {
            Assert.Multiple(() =>
                {
                    foreach (var expectedApp in expectedApplications)
                        Assert.Contains(expectedApp, actualApplications.ToList());
                }
            );
        }
    }

    public class WhenDeletingApplication
    {
        private List<BackupDirectory> directoriesToKeep;
        private List<BackupDirectory> directoriesToDelete;
        private PostgresTestHelper postgresTestHelper;
        private Application[] applications;

        [SetUp]
        public void Setup()
        {
            directoriesToKeep = new List<BackupDirectory>();
            directoriesToDelete = new List<BackupDirectory>();

            var postgresClient = new PostgresClient(new IntegrationTestSettings().PostgresSettings, new TestLogger<PostgresClient>());
            var applicationsManager = new ApplicationsManager(postgresClient, new TestLogger<ApplicationsManager>());
            postgresTestHelper = new PostgresTestHelper(postgresClient);

            applications = TestHelpers.GetApplications(2).ToArray();
            foreach (var application in applications)
            {
                var appId = postgresTestHelper.WriteApplication(application);
                for (var i = 0; i < 10; i++)
                {
                    var dir = TestHelpers.GetBackupDirectory(application);
                    if (application.Equals(applications[0]))
                        directoriesToDelete.Add(dir);
                    else
                        directoriesToKeep.Add(dir);

                    postgresTestHelper.WriteDirectory(appId, dir);
                }
            }

            var appId0 = postgresTestHelper.GetApplicationId(applications[0]);
            applicationsManager.RemoveApplication(appId0.Value);
        }

        [TearDown]
        public void Teardown()
        {
            foreach (var dir in directoriesToKeep)
                postgresTestHelper.DeleteDirectory(dir.LocalPath);

            foreach (var dir in directoriesToDelete) // Delete in case of test failure
            {
                try
                {
                    postgresTestHelper.DeleteDirectory(dir.LocalPath);
                }
                catch
                {
                    // ignored
                }
            }

            foreach (var application in applications)
                postgresTestHelper.DeleteApp(application);

        }

        [Test]
        public void ShouldDeleteAllApplicationDirectories() =>
            Assert.That(directoriesToDelete.All(d => postgresTestHelper.GetDirectoryId(d.LocalPath) == 0));

        [Test]
        public void ShouldDeleteApplication() =>
            Assert.AreEqual(0, postgresTestHelper.GetApplicationId(applications[0]));

        [Test]
        public void ShouldLeaveOtherApplicationDirectoriesIntact() =>
            Assert.AreEqual(10, directoriesToKeep.Count(d => postgresTestHelper.GetDirectoryId(d.LocalPath) > 0));

        [Test]
        public void ShouldLeaveOtherApplication() =>
            Assert.Greater(postgresTestHelper.GetApplicationId(applications[1]), 0);
    }
}