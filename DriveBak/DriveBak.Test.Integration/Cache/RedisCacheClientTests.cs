using System;
using System.Threading.Tasks;
using DriveBak.Cache;
using DriveBak.Test.Helpers;
using NUnit.Framework;

namespace DriveBak.Test.Integration.Cache
{
    public class WhenSettingAndGettingAValue
    {
        private string actualOut;
        private bool cacheHit;
        private string expected;

        [SetUp]
        public async Task Setup()
        {
            var settings = new IntegrationTestSettings().RedisSettings;
            var connection = new RedisConnection(settings, new TestLogger<RedisConnection>());
            var sut = new RedisCacheClient(connection, settings, new TestLogger<RedisCacheClient>());

            expected = Guid.NewGuid().ToString();
            var key = $"test:drive:folder:{expected}:id";

            await sut.SetAsync(key, expected);
            cacheHit = sut.TryGet(key, out actualOut);
        }

        [Test]
        public void ShouldReturnTrue() =>
            Assert.That(cacheHit);

        [Test]
        public void ShouldReturnShouldSetOutParamToId() =>
            Assert.AreEqual(expected, actualOut);
    }

    public class WhenGettingANonexistentKey
    {
        private bool cacheHit;

        [SetUp]
        public void Setup()
        {
            var settings = new IntegrationTestSettings().RedisSettings;
            var connection = new RedisConnection(settings, new TestLogger<RedisConnection>());
            var sut = new RedisCacheClient(connection, settings, new TestLogger<RedisCacheClient>());
            var key = $"test:drive:folder:{Guid.NewGuid()}:id";

            cacheHit = sut.TryGet(key, out string _);
        }

        [Test]
        public void ShouldReturnFalse() =>
            Assert.False(cacheHit);
    }

    public class WhenGettingVersion
    {
        private string version;

        [SetUp]
        public void Setup()
        {
            var settings = new IntegrationTestSettings().RedisSettings;
            var connection = new RedisConnection(settings, new TestLogger<RedisConnection>());
            var sut = new RedisCacheClient(connection, settings, new TestLogger<RedisCacheClient>());

            version = sut.Version;
        }

        [Test]
        public void ShouldReturnVersion() =>
            Assert.IsNotEmpty(version);
    }
}