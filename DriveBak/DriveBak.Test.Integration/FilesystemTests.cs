using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace DriveBak.Test.Integration
{
    public class WhenListingDirectory
    {
        public class WhenDirectoryIsEmpty
        {
            private Filesystem filesystem;
            private string path;

            [SetUp]
            public void Setup()
            {
                path = TemporaryDirectoryHelper.CreateDirectory();
                filesystem = new Filesystem();
            }

            [TearDown]
            public void TearDown()
            {
                Directory.Delete(path, recursive: true);
            }

            [Test]
            public void ListShouldReturnEmpty() =>
                Assert.IsEmpty(filesystem.List(path));
        }

        public class WhenDirectoryContainsOnlyTarballs
        {
            private Filesystem filesystem;
            private string path;
            private readonly List<string> fileList = new List<string>() {"file1.tar.gz", "file2.tar.gz"};

            [SetUp]
            public void Setup()
            {
                path = TemporaryDirectoryHelper.CreateDirectory(fileList.ToArray());
                filesystem = new Filesystem();
            }

            [TearDown]
            public void TearDown()
            {
                Directory.Delete(path, recursive: true);
            }

            [Test]
            public void ListShouldReturnListOfFiles()
            {
                CollectionAssert.AreEquivalent(fileList, filesystem.List(path));
            }
        }

        public class WhenDirectoryContainsMoreThanTarballs
        {
            private Filesystem filesystem;
            private string path;
            private readonly List<string> expectedFileList = new List<string>() {"file1.tar.gz", "file2.tar.gz"};

            private readonly List<string> fileList = new List<string>()
                {"file.exe", "file1.tar.gz", "script.sh", "file2.tar.gz"};

            [SetUp]
            public void Setup()
            {
                path = TemporaryDirectoryHelper.CreateDirectory(fileList.ToArray(), "dir1", "fdir2", "folder");
                filesystem = new Filesystem();
            }

            [TearDown]
            public void TearDown()
            {
                Directory.Delete(path, recursive: true);
            }

            [Test]
            public void ListShouldReturnListOfOnlyTarballs()
            {
                CollectionAssert.AreEquivalent(expectedFileList, filesystem.List(path));
            }
        }

        public class WhenDirectoryContainsSubdirectoriesWithTarballs
        {
            private Filesystem filesystem;
            private string path;
            private readonly List<string> expectedFileList = new List<string>() {"file1.tar.gz", "file2.tar.gz"};

            private readonly List<string> fileList = new List<string>()
                {"file.exe", "file1.tar.gz", "script.sh", "file2.tar.gz"};

            [SetUp]
            public void Setup()
            {
                path = TemporaryDirectoryHelper.CreateDirectory(fileList.ToArray(), "dir1", "fdir2", "folder");
                File.WriteAllText(Path.Join(path, "folder", "ignoreme.tar.gz"), "test");
                filesystem = new Filesystem();
            }

            [TearDown]
            public void TearDown()
            {
                Directory.Delete(path, recursive: true);
            }

            [Test]
            public void ListShouldReturnListOfOnlyTarballs()
            {
                CollectionAssert.AreEquivalent(expectedFileList, filesystem.List(path));
            }
        }
    }

    public class WhenDirectoryDoesNotExist
    {
        [Test]
        public void ListShouldReturnEmptyList()
        {
            var path = Path.Join(Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
            Assert.IsNull(new Filesystem().List(path));
        }
    }

    public class WhenValidatingPath
    {
        public class WhenPathIsValidAndExists
        {
            private string path;

            [SetUp] public void Setup() => path = TemporaryDirectoryHelper.CreateDirectory();

            [TearDown] public void Teardown() => Directory.Delete(path);

            [Test] public void ShouldReturnTrue() => Assert.That(new Filesystem().IsValidPath(path));
        }

        public class WhenPathIsValidButDoesNotExist
        {
            private string path;

            [SetUp]
            public void Setup()
            {
                path = TemporaryDirectoryHelper.CreateDirectory();
                Directory.Delete(path);
            }

            [Test]
            public void ShouldReturnTrue() => Assert.That(new Filesystem().IsValidPath(path));
        }

        public class WhenPathIsInvalid
        {
            static IEnumerable<TestCaseData> InvalidPermutations() =>
                Path.GetInvalidPathChars().Select(c => new TestCaseData(Path.Join(Path.GetTempPath(), $"a{c.ToString()}b")));

            [Test]
            [TestCaseSource(nameof(InvalidPermutations))]
            public void ShouldReturnFalse(string path) => Assert.False(new Filesystem().IsValidPath(path));
        }
    }
}