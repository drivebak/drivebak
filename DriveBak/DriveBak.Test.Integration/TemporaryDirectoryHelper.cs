using System.Collections.Generic;
using System.IO;

namespace DriveBak.Test.Integration
{
    public static class TemporaryDirectoryHelper
    {
        public static string CreateDirectory() => CreateDirectory(new List<string>());
        public static string CreateDirectory(IEnumerable<string> filenames, params string[] directories)
        {
            var path = Path.Join(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(path);
            foreach (var dir in directories)
            {
                var dirPath = Path.Join(path, dir);
                Directory.CreateDirectory(dirPath);
            }
            foreach (var filename in filenames)
            {
                var filePath = Path.Join(path, filename);
                File.WriteAllText(filePath, filePath);
            }

            return path;
        }
    }
}