using System.IO;
using DriveBak.Cache;
using DriveBak.DB;
using Microsoft.Extensions.Configuration;

namespace DriveBak.Test.Integration
{
    public class IntegrationTestSettings
    {
        private readonly IConfigurationRoot config;

        public IntegrationTestSettings()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
        }

        public IRedisSettings RedisSettings => new RedisSettings
        {
            Host = config.GetValue<string>("REDIS_HOST"),
            Port = config.GetValue<int>("REDIS_PORT"),
            Password = config.GetValue<string>("REDIS_PASSWORD"),
            CacheLengthSeconds = config.GetValue<int>("REDIS_CACHE_LENGTH_SECONDS"),
        };

        public IPostgresSettings PostgresSettings => new PostgresSettings()
        {
            Host = config.GetValue<string>("POSTGRES_HOST"),
            Database = config.GetValue<string>("POSTGRES_DATABASE"),
            Username = config.GetValue<string>("POSTGRES_USERNAME"),
            Password = config.GetValue<string>("POSTGRES_PASSWORD"),
        };

    }
}