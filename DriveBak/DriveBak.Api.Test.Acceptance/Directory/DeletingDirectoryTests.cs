using System;
using System.Net;
using System.Web;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Directory
{
    public class WhenDeletingADirectory
    {
        private BackupDirectory directory;
        private IRestResponse deleteResponse;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            var application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            var appId = AcceptanceTestHelper.AddApplication(application);
            AcceptanceTestHelper.AddDirectory(appId, directory);
            var encodedLocalPath = HttpUtility.UrlEncode(directory.LocalPath);
            deleteResponse = HttpTestHelper.ExecuteRequest(
                Method.DELETE,
                $"application/{directory.Application}/directory/{encodedLocalPath}"
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithStatusNoContent() =>
            Assert.AreEqual(HttpStatusCode.NoContent, deleteResponse.StatusCode);

        [Test]
        public void ShouldRespondWithEmptyBody() =>
            Assert.IsEmpty(deleteResponse.Content);
    }

    public class WhenDeletingADirectoryThatDoesNotExist
    {
        private IRestResponse<ErrorResponse> response;

        [SetUp]
        public void Setup()
        {
            response = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.DELETE,
                $"application/{Guid.NewGuid()}/directory/{Guid.NewGuid()}"
            );
        }

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(response.Data.Message, response.Content);

        [Test]
        public void ShouldReturnStatusNotFound() =>
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
    }
}