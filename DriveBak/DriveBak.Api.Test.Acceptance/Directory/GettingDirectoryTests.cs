using System;
using System.Net;
using System.Web;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Directory
{
    public class WhenGettingADirectory
    {
        private IRestResponse<BackupDirectory> getResponse;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            var application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            var appId = AcceptanceTestHelper.AddApplication(application);
            AcceptanceTestHelper.AddDirectory(appId, directory);
            var encodedLocalPath = HttpUtility.UrlEncode(directory.LocalPath);
            getResponse = HttpTestHelper.ExecuteRequest<BackupDirectory>(
                Method.GET,
                $"application/{directory.Application}/directory/{encodedLocalPath}"
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveDirectory(directory);
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, getResponse.StatusCode);

        [Test]
        public void ShouldReturnCreatedBackupDirectory() =>
            Assert.AreEqual(directory, getResponse.Data);
    }

    public class WhenGettingADirectoryThatDoesNotExist
    {
        private IRestResponse<ErrorResponse> getResponse;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            var application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            AcceptanceTestHelper.AddApplication(application);
            var encodedLocalPath = HttpUtility.UrlEncode(directory.LocalPath);
            getResponse = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.GET,
                $"application/{directory.Application}/directory/{encodedLocalPath}"
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithStatusNotFound() =>
            Assert.AreEqual(HttpStatusCode.NotFound, getResponse.StatusCode);

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(getResponse.Data.Message);
    }
}