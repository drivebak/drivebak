using System;
using System.IO;
using System.Linq;
using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Directory
{
    public class WhenAddingADirectoryWithoutADescription
    {
        private IRestResponse<BackupDirectory> createResponse;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            var application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            directory.Description = null;
            AcceptanceTestHelper.AddApplication(application);
            createResponse = HttpTestHelper.ExecuteRequest<BackupDirectory>(
                Method.POST,
                $"application/{directory.Application}/directory",
                new
                {
                    directory.LocalPath,
                    directory.RemotePath
                }
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveDirectory(directory);
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, createResponse.StatusCode);

        [Test]
        public void ShouldReturnCreatedBackupDirectory() =>
            Assert.AreEqual(directory, createResponse.Data);
    }

    public class WhenAddingADirectoryWithADescription
    {
        private IRestResponse<BackupDirectory> createResponse;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            var application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            AcceptanceTestHelper.AddApplication(application);
            createResponse = HttpTestHelper.ExecuteRequest<BackupDirectory>(
                Method.POST,
                $"application/{directory.Application}/directory",
                new
                {
                    directory.Description,
                    directory.LocalPath,
                    directory.RemotePath
                }
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveDirectory(directory);
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, createResponse.StatusCode);

        [Test]
        public void ShouldReturnCreatedBackupDirectory() =>
            Assert.AreEqual(directory, createResponse.Data);
    }

    public class WhenAddingADirectoryAndApplicationDoesNotExist
    {
        private IRestResponse<ErrorResponse> response;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            directory = AcceptanceTestHelper.GenerateBackupDirectory();
            response = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.POST,
                $"application/{directory.Application}/directory",
                new
                {
                    directory.Description,
                    directory.LocalPath,
                    directory.RemotePath
                }
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveDirectory(directory);
            AcceptanceTestHelper.RemoveApplication(directory.Application);
        }

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(response.Data.Message);

        [Test]
        public void ShouldReturnStatusBadRequest() =>
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, response.Content);
    }

    public class WhenAddingInvalidLocalDirectory
    {
        private IRestResponse<ErrorResponse> response;

        [SetUp]
        public void Setup()
        {
            response = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.POST,
                $"application/{Guid.NewGuid()}/directory",
                new
                {
                    LocalPath = $"/tmp/{Path.GetInvalidPathChars().First()}",
                    RemotePath = $"drivebak/test/acceptance/{Guid.NewGuid()}",
                });
        }

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(response.Data.Message);

        [Test]
        public void ShouldReturnStatusBadRequest() =>
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, response.Content);
    }
}