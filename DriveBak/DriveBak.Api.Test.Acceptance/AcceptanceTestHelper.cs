using System;
using DriveBak.DB;
using DriveBak.Test.Helpers;

namespace DriveBak.Api.Test.Acceptance
{
    public static class AcceptanceTestHelper
    {
        private static ApplicationsManager applicationsManager;

        private static ApplicationsManager ApplicationsManager
        {
            get
            {
                if (applicationsManager == null)
                    applicationsManager =
                        new ApplicationsManager(
                            new PostgresClient(new AcceptanceTestSettings().PostgresSettings, new TestLogger<PostgresClient>()),
                            new TestLogger<ApplicationsManager>()
                        );
                return applicationsManager;
            }
        }

        public static int AddApplication(DriveBak.Application application)
        {
            return ApplicationsManager.AddApplication(application);
        }

        public static void RemoveApplication(string appName)
        {
            var appId = ApplicationsManager.GetApplicationId(appName);
            ApplicationsManager.RemoveApplication(appId);
        }

        public static BackupDirectory AddDirectory(int appId, BackupDirectory backupDirectory)
        {
            backupDirectory ??= GenerateBackupDirectory();
            ApplicationsManager.AddDirectory(appId, backupDirectory);
            return backupDirectory;
        }

        public static void RemoveDirectory(BackupDirectory backupDirectory)
        {
            var directoryId = applicationsManager.GetDirectoryId(backupDirectory.LocalPath);
            ApplicationsManager.RemoveDirectory(directoryId);
        }

        public static BackupDirectory GenerateBackupDirectory(DriveBak.Application application = null, string guid = null)
        {
            guid ??= Guid.NewGuid().ToString();
            application ??= GenerateApplication(guid);
            return new BackupDirectory()
            {
                Application = application.Name,
                Description = $"acceptanceTests-description-{guid}",
                LocalPath = $"/tmp/drivebak/test/acceptance/{guid}",
                RemotePath = $"drivebak/test/acceptance/{guid}",
            };
        }

        public static DriveBak.Application GenerateApplication(string guid = null)
        {
            guid ??= Guid.NewGuid().ToString();
            return new DriveBak.Application()
            {
                Name = $"acceptancetests-{guid}",
                Description = $"acceptancetests-{guid}",
            };
        }
    }
}
