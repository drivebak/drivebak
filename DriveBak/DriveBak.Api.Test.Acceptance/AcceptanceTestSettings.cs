using DriveBak.DB;
using Microsoft.Extensions.Configuration;

namespace DriveBak.Api.Test.Acceptance
{
    public class AcceptanceTestSettings
    {
        private readonly IConfigurationRoot config;

        public AcceptanceTestSettings()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        public string BaseUrl => config.GetValue<string>("DRIVEBAK_BASE_URL");

        public IPostgresSettings PostgresSettings => new AcceptanceTestPostgresSettings()
        {
            Host = config.GetValue<string>("POSTGRES_HOST"),
            Database = config.GetValue<string>("POSTGRES_DATABASE"),
            Username = config.GetValue<string>("POSTGRES_USERNAME"),
            Password = config.GetValue<string>("POSTGRES_PASSWORD"),
        };
    }

    public class AcceptanceTestPostgresSettings : IPostgresSettings
    {
        public string Host { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int CommandTimeoutSeconds => 30;
        public int MaxPoolSize => 50;
        public int TimeoutSeconds => 15;
    }
}