using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Serialization;

namespace DriveBak.Api.Test.Acceptance
{
    public class JsonNetSerializer : IRestSerializer
    {
        private JsonSerializerSettings serializerSettings;

        public string ContentType { get ; set; } = "application/json";

        public string[] SupportedContentTypes => new[] { "application/json", "text/json", "text/x-json", "*+json" };
        public DataFormat DataFormat => DataFormat.Json;

        public JsonNetSerializer()
        {
            serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };
        }

        public string Serialize(object obj) => JsonConvert.SerializeObject(obj, serializerSettings);

        public T Deserialize<T>(IRestResponse response) => JsonConvert.DeserializeObject<T>(response.Content, serializerSettings);

        public string Serialize(Parameter parameter) => JsonConvert.SerializeObject(parameter.Value, serializerSettings);
    }
}