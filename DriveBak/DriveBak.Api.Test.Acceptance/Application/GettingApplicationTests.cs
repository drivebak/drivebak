using System;
using System.Collections.Generic;
using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Application
{
    public class WhenGettingAnApplicationThatDoesNotExist
    {
        private IRestResponse<ErrorResponse> response;

        [SetUp]
        public void Setup()
        {
            response = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.GET,
                $"application/{Guid.NewGuid()}"
            );
        }

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(response.Data.Message, response.Content);

        [Test]
        public void ShouldReturnStatusNotFound() =>
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode, response.Content);
    }

    public class WhenGettingAnApplicationThatDoesExistAndHasDirectories
    {
        private IRestResponse<GetApplicationResponse> response;
        private DriveBak.Application application;
        private BackupDirectory directory;

        [SetUp]
        public void Setup()
        {
            var guid = Guid.NewGuid().ToString();
            application = AcceptanceTestHelper.GenerateApplication(guid);
            directory = AcceptanceTestHelper.GenerateBackupDirectory(application, guid);
            var appId = AcceptanceTestHelper.AddApplication(application);
            AcceptanceTestHelper.AddDirectory(appId, directory);
            response = HttpTestHelper.ExecuteRequest<GetApplicationResponse>(
                Method.GET,
                $"application/{application.Name}"
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveDirectory(directory);
            AcceptanceTestHelper.RemoveApplication(application.Name);
        }

        [Test]
        public void ShouldRespondWithName() =>
            Assert.AreEqual(application.Name, response.Data.Name, response.Content);

        [Test]
        public void ShouldRespondWithDescription() =>
            Assert.AreEqual(application.Description, response.Data.Description, response.Content);

        [Test]
        public void ShouldRespondWithDirectoryList() =>
            CollectionAssert.AreEquivalent(new List<BackupDirectory>() {directory}, response.Data.Directories,
                response.Content);

        [Test]
        public void ShouldReturnStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, response.Content);


        class GetApplicationResponse
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public IEnumerable<BackupDirectory> Directories { get; set; }
        }
    }

    public class WhenGettingAnApplicationThatDoesExistButHasNoDirectories
    {
        private IRestResponse<GetApplicationResponse> response;
        private DriveBak.Application application;

        [SetUp]
        public void Setup()
        {
            application = AcceptanceTestHelper.GenerateApplication();
            AcceptanceTestHelper.AddApplication(application);
            response = HttpTestHelper.ExecuteRequest<GetApplicationResponse>(
                Method.GET,
                $"application/{application.Name}"
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveApplication(application.Name);
        }

        [Test]
        public void ShouldRespondWithName() =>
            Assert.AreEqual(application.Name, response.Data.Name, response.Content);

        [Test]
        public void ShouldRespondWithDescription() =>
            Assert.AreEqual(application.Description, response.Data.Description, response.Content);

        [Test]
        public void ShouldRespondWithEmptyDirectoryList() =>
            Assert.IsEmpty(response.Data.Directories, response.Content);

        [Test]
        public void ShouldReturnStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, response.Content);


        class GetApplicationResponse
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public IEnumerable<BackupDirectory> Directories { get; set; }
        }
    }
}