using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Application
{
    public class WhenGettingApplicationAllThatDoesExistAndHasDirectories
    {
        private IRestResponse<List<DriveBak.Application>> response;
        private List<DriveBak.Application> apps;

        [SetUp]
        public void Setup()
        {
            apps = new List<DriveBak.Application>();
            for (var i = 0; i < 5; i++)
            {
                var app = AcceptanceTestHelper.GenerateApplication();
                AcceptanceTestHelper.AddApplication(app);
                apps.Add(app);
            }

            response = HttpTestHelper.ExecuteRequest<List<DriveBak.Application>>(
                Method.GET,
                "application"
            );
        }

        [TearDown]
        public void Teardown()
        {
            foreach (var app in apps)
                AcceptanceTestHelper.RemoveApplication(app.Name);
        }

        [Test]
        public void ShouldResponseWithAllApps() =>
            Assert.IsEmpty(apps.Except(response.Data));

        [Test]
        public void ShouldReturnStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, response.Content);
    }
}