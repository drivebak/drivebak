using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Application
{
    public class WhenCreatingAnApplication
    {
        private IRestResponse response;
        private DriveBak.Application application;

        [SetUp]
        public void Setup()
        {
            application = AcceptanceTestHelper.GenerateApplication();
            response = HttpTestHelper.ExecuteRequest(
                Method.POST,
                $"application",
                application
            );
        }

        [TearDown]
        public void Teardown()
        {
            AcceptanceTestHelper.RemoveApplication(application.Name);
        }

        [Test]
        public void ShouldReturnStatusCreated() =>
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode, response.Content);
    }
}