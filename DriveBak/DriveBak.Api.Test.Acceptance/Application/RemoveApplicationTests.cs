using System;
using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Application
{
    public class WhenRemovingAnApplicationThatDoesNotExist
    {
        private IRestResponse<ErrorResponse> response;

        [SetUp]
        public void Setup()
        {
            response = HttpTestHelper.ExecuteRequest<ErrorResponse>(
                Method.DELETE,
                $"application/{Guid.NewGuid()}"
            );
        }

        [Test]
        public void ShouldRespondWithMessage() =>
            Assert.IsNotEmpty(response.Data.Message, response.Content);

        [Test]
        public void ShouldReturnStatusNotFound() =>
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode, response.Content);
    }

    public class WhenRemovingAnApplicationThatDoesExist
    {
        private DriveBak.Application application;
        private IRestResponse response;

        [SetUp]
        public void Setup()
        {
            application = AcceptanceTestHelper.GenerateApplication();
            var appId = AcceptanceTestHelper.AddApplication(application);
            response = HttpTestHelper.ExecuteRequest(
                Method.DELETE,
                $"application/{application.Name}"
            );
        }

        [Test]
        public void ShouldReturnStatusNoContent() =>
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode, response.Content);
    }
}