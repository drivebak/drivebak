using System.Net;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Api.Test.Acceptance.Health
{
    public class WhenGettingHealth
    {
        private IRestResponse<HealthCheckResponse> response;

        [SetUp]
        public void Setup()
        {
            response = HttpTestHelper.ExecuteRequest<HealthCheckResponse>(Method.GET, "health");
        }

        [Test]
        public void ShouldReturnStatusHealthy() =>
            Assert.AreEqual("healthy", response.Data.Status.ToLower());

        [Test]
        public void ShouldReturnApiAppName() =>
            Assert.AreEqual("Drive Bak API", response.Data.Api.Name);

        [Test]
        public void ShouldReturnApiVersion() =>
            Assert.IsNotEmpty(response.Data.Api.Version);

        [Test]
        public void ShouldReturnApiStatusHealthy() =>
            Assert.AreEqual("healthy", response.Data.Api.Status.ToLower());

        [Test]
        public void ShouldReturnDatabaseAppName() =>
            Assert.AreEqual("Postgres", response.Data.Database.Name);

        [Test]
        public void ShouldReturnDatabaseVersion() =>
            Assert.IsNotEmpty(response.Data.Database.Version);

        [Test]
        public void ShouldReturnDatabaseStatusHealthy() =>
            Assert.AreEqual("healthy", response.Data.Database.Status.ToLower());

        [Test]
        public void ShouldReturnCacheAppName() =>
            Assert.AreEqual("Redis", response.Data.Cache.Name);

        [Test]
        public void ShouldReturnCacheVersion() =>
            Assert.IsNotEmpty(response.Data.Cache.Version);

        [Test]
        public void ShouldReturnCacheStatusHealthy() =>
            Assert.AreEqual("healthy", response.Data.Cache.Status.ToLower());

        [Test]
        public void ShouldReturnStatusOk() =>
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
    }

    public class HealthCheckResponse
    {
        public string Status { get; set; }
        public HealthInfo Api { get; set; }
        public HealthInfo Database { get; set; }
        public HealthInfo Cache { get; set; }
    }

    public class HealthInfo
    {
        public string Status { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
    }
}