using RestSharp;
using RestSharp.Serialization.Json;

namespace DriveBak.Api.Test.Acceptance
{
    public static class HttpTestHelper
    {
        public static IRestResponse ExecuteRequest(Method method, string path, object body=null)
        {
            var client = BuildClient();
            var request = BuildRequest(method, path, body);
            var response = client.Execute(request);
            return response;
        }

        public static IRestResponse<T> ExecuteRequest<T>(Method method, string path, object body=null) where T : new()
        {
            var client = BuildClient();
            var request = BuildRequest(method, path, body);
            var response = client.Execute<T>(request);
            return response;
        }

        private static RestClient BuildClient()
        {
            var settings = new AcceptanceTestSettings();
            var client = new RestClient(settings.BaseUrl);
            client.ClearHandlers();
            client.UseSerializer(() => new JsonNetSerializer());
            return client;
        }

        private static RestRequest BuildRequest(Method method, string path, object body)
        {
            var request = new RestRequest($"api/{path}", method);
            request.XmlSerializer = null;
            request.JsonSerializer = new JsonDeserializer();
            if (body != null)
                request.AddJsonBody(body);
            return request;
        }
    }
}