using Autofac.Extras.Moq;
using DriveBak.Logging;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Service.Test.Unit
{
    public abstract class WithAutoMocked<T> where T : class
    {
        private static AutoMock autoMock;
        protected static T Sut => autoMock.Create<T>();
        protected static Mock<TM> GetTestDouble<TM>() where TM : class => autoMock.Mock<TM>();

        [SetUp]
        public void SetUp()
        {
            autoMock = AutoMock.GetLoose();
            autoMock.Provide<ILogger<T>>(new TestLogger<T>());
        }


        [TearDown]
        public void TearDown() => autoMock?.Dispose();
    }
}