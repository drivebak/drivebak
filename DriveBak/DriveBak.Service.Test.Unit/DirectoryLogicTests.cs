using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DriveBak.Drive;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Service.Test.Unit
{
    public class WhenRunningLogic : WithAutoMocked<DirectoryLogic>
    {
        private readonly string newFileName = "newfile.tar.gz";

        private readonly BackupDirectory backupDirectory = new BackupDirectory()
        {
            LocalPath = "/tmp/testdir/",
            RemotePath = "files/testdir_backup/",
        };

        [SetUp]
        public async Task Setup()
        {
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((IEnumerable<string>)new List<string>()));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(new List<string>() {newFileName});
            await Sut.ExecuteAsync(backupDirectory);
        }

        [Test]
        public void ShouldListCorrectRemoteDirectory() =>
            GetTestDouble<IFilesystem>().Verify(x => x.List(backupDirectory.LocalPath));

        [Test]
        public void ShouldScanCorrectLocalDirectory() =>
            GetTestDouble<IDriveManager>().Verify(x => x.GetFileNamesAsync(backupDirectory.RemotePath));
    }

    public class WhenDirHasNewTarball : WithAutoMocked<DirectoryLogic>
    {
        private readonly string newFileName = "newfile.tar.gz";

        private readonly BackupDirectory backupDirectory = new BackupDirectory()
        {
            LocalPath = "/tmp/testdir/",
            RemotePath = "files/testdir_backup/",
        };

        [SetUp]
        public async Task Setup()
        {
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((IEnumerable<string>)new List<string>()));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(new List<string>() {newFileName});
            await Sut.ExecuteAsync(backupDirectory);
        }

        [Test]
        public void ShouldUploadNewTarball() =>
            GetTestDouble<IDriveManager>()
                .Verify(x => x.UploadFileAsync(Path.Join(backupDirectory.LocalPath, newFileName),
                    backupDirectory.RemotePath, newFileName, It.IsAny<string>()));
    }

    public class WhenDirHasMultipleNewTarballs : WithAutoMocked<DirectoryLogic>
    {
        private readonly List<string> originalFileList =
            new List<string>() {"file1.tar.gz"};

        private readonly List<string> newFileList =
            new List<string>() {"file1.tar.gz", "newfile1.tar.gz", "newfile2.tar.gz", "newfile3.tar.gz"};

        private readonly BackupDirectory backupDirectory = new BackupDirectory()
        {
            LocalPath = "/tmp/testdir/",
            RemotePath = "files/testdir_backup/",
        };

        [SetUp]
        public async Task Setup()
        {
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((IEnumerable<string>)originalFileList));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(newFileList);

            await Sut.ExecuteAsync(backupDirectory);
        }

        [Test]
        public void ShouldUploadNewTarball() =>
            GetTestDouble<IDriveManager>()
                .Verify(
                    x => x.UploadFileAsync(
                        It.Is<string>(s => s.StartsWith(Path.Join(backupDirectory.LocalPath, "new"))),
                        backupDirectory.RemotePath, It.Is<string>(s => s.StartsWith("new")), It.IsAny<string>()),
                    Times.Exactly(3));
    }

    public class WhenDirHasNoFiles : WithAutoMocked<DirectoryLogic>
    {
        private readonly BackupDirectory backupDirectory = new BackupDirectory()
        {
            LocalPath = "/tmp/testdir/",
            RemotePath = "files/testdir_backup/",
        };

        [SetUp]
        public async Task Setup()
        {
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((IEnumerable<string>)new List<string>()));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(new List<string>());
            await Sut.ExecuteAsync(backupDirectory);
        }

        [Test]
        public void ShouldUploadNewTarball() =>
            GetTestDouble<IDriveManager>()
                .Verify(
                    x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()), Times.Never);
    }

    public class WhenDirHasNoNewFiles : WithAutoMocked<DirectoryLogic>
    {
        private readonly string fileName = "file.tar.gz";

        private readonly BackupDirectory backupDirectory = new BackupDirectory()
        {
            LocalPath = "/tmp/testdir/",
            RemotePath = "files/testdir_backup/",
        };

        [SetUp]
        public async Task Setup()
        {
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(It.IsAny<string>()))
                .Returns(Task.FromResult((IEnumerable<string>)new List<string>(){fileName}));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(new List<string>() {fileName});
            await Sut.ExecuteAsync(backupDirectory);
        }

        [Test]
        public void ShouldNotUploadNewTarball() =>
            GetTestDouble<IDriveManager>()
                .Verify(
                    x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()), Times.Never);
    }

    public class WhenRemoteDirectoryDoesNotExist : WithAutoMocked<DirectoryLogic>
    {
        private string remotePath = "files/testdir_backup/";
        private IEnumerable<string> localFileList;

        [SetUp]
        public async Task Setup()
        {
            localFileList = new List<string>
            {
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
            };
            GetTestDouble<IDriveManager>()
                .Setup(x => x.GetFileNamesAsync(remotePath))
                .Returns(Task.FromResult((IEnumerable<string>) null));
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns(localFileList);
            await Sut.ExecuteAsync(new BackupDirectory()
            {
                RemotePath = remotePath,
                LocalPath = Guid.NewGuid().ToString(),
                Application = TestHelpers.GetApplication().Name,
            });
        }

        [Test]
        public void ShouldCreateRemoteDirectory() =>
            GetTestDouble<IDriveManager>().Verify(x => x.CreateFolderAsync(remotePath));

        [Test]
        public void ShouldUploadAllLocalFiles()
        {
            foreach (var file in localFileList)
            {
                GetTestDouble<IDriveManager>()
                    .Verify(
                        x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>(), file, It.IsAny<string>()));
            }
        }
    }

    public class WhenLocalDirectoryDoesNotExist : WithAutoMocked<DirectoryLogic>
    {
        private string remotePath = "files/testdir_backup/";

        [SetUp]
        public void Setup()
        {
            GetTestDouble<IFilesystem>()
                .Setup(x => x.List(It.IsAny<string>()))
                .Returns((IEnumerable<string>) null);

            Sut.ExecuteAsync(new BackupDirectory()
            {
                RemotePath = remotePath,
                LocalPath = Guid.NewGuid().ToString(),
                Application = TestHelpers.GetApplication().Name,
            });
        }

        [Test]
        public void ShouldNotQueryRemoteDirectory() =>
            GetTestDouble<IDriveManager>()
                .Verify(x => x.GetFileNamesAsync(It.IsAny<string>()), Times.Never());

        [Test]
        public void ShouldNotUploadNewTarball() =>
            GetTestDouble<IDriveManager>()
                .Verify(
                    x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()), Times.Never);
    }
}