using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Service.Test.Unit
{
    public class WhenExecuting : WithAutoMocked<BackupManager>
    {
        private IEnumerable<BackupDirectory> backupDirectories;
        private IEnumerable<Application> applications;
        private Mock<IApplicationsManager> applicationsManager;

        [SetUp]
        public void Setup()
        {
            applications = TestHelpers.GetApplications(2).ToList();
            backupDirectories = TestHelpers.GetBackupDirectories(3, applications.First())
                .Concat(TestHelpers.GetBackupDirectories(3, applications.Last())).ToList();
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager
                .Setup(x => x.GetAllApplications())
                .Returns(applications);
            foreach (var backupDirectory in backupDirectories)
            {
                applicationsManager.Setup(x => x.GetApplicationDirectories(backupDirectory.Application))
                    .Returns(new List<BackupDirectory>() {backupDirectory});
            }

            foreach (var application in applications)
            {
                var dirs = backupDirectories.Where(d => d.Application == application.Name).ToList();
                applicationsManager.Setup(x => x.GetApplicationDirectories(application.Name))
                    .Returns(dirs);
            }

            Sut.Execute(CancellationToken.None);
        }

        [Test]
        public void ShouldQueryAllApplications()
        {
            foreach (var application in applications)
                applicationsManager.Verify(x => x.GetApplicationDirectories(application.Name));
        }

        [Test]
        public void ShouldGetAllApplications() =>
            applicationsManager.Verify(x => x.GetAllApplications());

        [Test]
        public void ShouldExecuteOnAllDirectories()
        {
            foreach (var backupDirectory in backupDirectories)
                GetTestDouble<IDirectoryLogic>().Verify(x => x.ExecuteAsync(backupDirectory));
        }
    }
}