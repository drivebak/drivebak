using DriveBak.Logging;

namespace DriveBak.Test.Helpers
{
    public class TestLogger<T> : ILogger<T>
    {
        public ILogger<T> AddContext<T1>() => this;

        public ILogger<T> AddContext(LogContext key, string value) => this;

        public void Verbose(string message, params object[] values) { }

        public void Debug(string message, params object[] values) { }

        public void Information(string message, params object[] values) { }

        public void Warning(string message, params object[] values) { }

        public void Error(string message, params object[] values) { }

        public void Fatal(string message, params object[] values) { }
    }
}