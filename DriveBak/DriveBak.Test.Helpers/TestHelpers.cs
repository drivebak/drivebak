using System;
using System.Collections.Generic;
using System.Linq;

namespace DriveBak.Test.Helpers
{
    public static class TestHelpers
    {
        public static Application GetApplication(string prefix = null) => GetApplications(1, prefix).Single();
        public static IEnumerable<Application> GetApplications(int count, string prefix = null)
        {
            for (var i = 0; i < count; i++)
            {
                yield return new Application()
                {
                    Name = prefix + Guid.NewGuid(),
                    Description = Guid.NewGuid().ToString(),
                };
            }
        }

        public static BackupDirectory GetBackupDirectory(Application application = null) =>
            GetBackupDirectories(1, application).Single();

        public static IEnumerable<BackupDirectory> GetBackupDirectories(int count, Application application = null)
        {
            application ??= GetApplication();
            for (var i = 0; i < count; i++)
            {
                yield return new BackupDirectory()
                {
                    Application = application.Name,
                    Description = Guid.NewGuid().ToString(),
                    LocalPath = Guid.NewGuid().ToString(),
                    RemotePath = Guid.NewGuid().ToString(),
                };
            }
        }
    }
}