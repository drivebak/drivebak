using System;

namespace DriveBak.Api
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message) {}

        public object ResponseBody => new {Message};
    }
}