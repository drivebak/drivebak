using System.Web;
using DriveBak.Api.Commands;
using Microsoft.AspNetCore.Mvc;

namespace DriveBak.Api.Controllers
{
    [ApiController]
    [Route("api/application/{application}")]
    public class DirectoryController : ControllerBase
    {
        private readonly AddTrackedDirectoryCommand addTrackedDirectoryCommand;
        private readonly GetTrackedDirectoryCommand getTrackedDirectoryCommand;
        private readonly RemoveTrackedDirectoryCommand removeTrackedDirectoryCommand;

        public DirectoryController(AddTrackedDirectoryCommand addTrackedDirectoryCommand,
            GetTrackedDirectoryCommand getTrackedDirectoryCommand,
            RemoveTrackedDirectoryCommand removeTrackedDirectoryCommand)
        {
            this.addTrackedDirectoryCommand = addTrackedDirectoryCommand;
            this.getTrackedDirectoryCommand = getTrackedDirectoryCommand;
            this.removeTrackedDirectoryCommand = removeTrackedDirectoryCommand;
        }

        [Route("directory")]
        [HttpPost]
        public BackupDirectory AddTrackedDirectory(string application,
            [FromBody]AddTrackedDirectoryRequest addTrackedDirectoryRequest)
        {
            return addTrackedDirectoryCommand.Execute(application, addTrackedDirectoryRequest);
        }

        [Route("directory/{localPath}")]
        [HttpGet]
        public BackupDirectory GetTrackedDirectory(string application, string localPath)
        {
            var decodedPath = HttpUtility.UrlDecode(localPath);
            return getTrackedDirectoryCommand.Execute(application, decodedPath);
        }

        [Route("directory/{localPath}")]
        [HttpDelete]
        public NoContentResult RemoveTrackedDirectory(string application, string localPath)
        {
            var decodedPath = HttpUtility.UrlDecode(localPath);
            removeTrackedDirectoryCommand.Execute(application, decodedPath);
            return NoContent();
        }
    }
}