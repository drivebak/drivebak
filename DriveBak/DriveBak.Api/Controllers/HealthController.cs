using DriveBak.Api.Commands;
using Microsoft.AspNetCore.Mvc;

namespace DriveBak.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class HealthController : ControllerBase
    {
        private readonly GetHealthCommand getHealthCommand;

        public HealthController(GetHealthCommand getHealthCommand)
        {
            this.getHealthCommand = getHealthCommand;
        }

        [Route("health")]
        [HttpGet]
        public HealthCheckResponse Health ()
        {
            return getHealthCommand.Execute();
        }
    }
}