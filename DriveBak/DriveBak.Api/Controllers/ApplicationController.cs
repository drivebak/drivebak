using System.Collections.Generic;
using DriveBak.Api.Commands;
using Microsoft.AspNetCore.Mvc;

namespace DriveBak.Api.Controllers
{
    [ApiController]
    [Route("api/application")]
    public class ApplicationController : ControllerBase
    {
        private readonly GetApplicationCommand getApplicationCommand;
        private readonly AddApplicationCommand addApplicationCommand;
        private readonly GetAllApplicationsCommand getAllApplicationsCommand;
        private readonly RemoveApplicationCommand removeApplicationCommand;

        public ApplicationController(GetApplicationCommand getApplicationCommand,
            AddApplicationCommand addApplicationCommand,
            GetAllApplicationsCommand getAllApplicationsCommand,
            RemoveApplicationCommand removeApplicationCommand)
        {
            this.getApplicationCommand = getApplicationCommand;
            this.addApplicationCommand = addApplicationCommand;
            this.getAllApplicationsCommand = getAllApplicationsCommand;
            this.removeApplicationCommand = removeApplicationCommand;
        }

        [HttpGet]
        public IEnumerable<Application> GetAllApplication()
        {
            return getAllApplicationsCommand.Execute();
        }

        [Route("{application}")]
        [HttpGet]
        public GetApplicationResponse GetApplication(string application)
        {
            return getApplicationCommand.Execute(application);
        }

        [Route("")]
        [HttpPost]
        public CreatedAtActionResult CreateApplication([FromBody]Application application)
        {
            addApplicationCommand.Execute(application);
            return CreatedAtAction(nameof(GetApplication), new {application = application.Name}, application);
        }

        [Route("{application}")]
        [HttpDelete]
        public NoContentResult RemoveApplication(string application)
        {
            removeApplicationCommand.Execute(application);
            return new NoContentResult();
        }
    }
}