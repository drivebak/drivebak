using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class AddApplicationCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public AddApplicationCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public void Execute(Application application)
        {
            if (string.IsNullOrEmpty(application.Name))
                throw new BadRequestException($"Invalid application name '{application.Name}'");

            applicationsManager.AddApplication(application);
        }
    }
}