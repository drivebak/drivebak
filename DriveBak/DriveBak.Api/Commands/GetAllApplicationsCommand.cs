using System.Collections.Generic;
using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class GetAllApplicationsCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public GetAllApplicationsCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public IEnumerable<Application> Execute() =>
            applicationsManager.GetAllApplications();
    }
}