using DriveBak.DB;
using DriveBak.Drive;

namespace DriveBak.Api.Commands
{
    public class AddTrackedDirectoryCommand
    {
        private readonly IApplicationsManager applicationsManager;
        private readonly IFilesystem filesystem;
        private readonly IDriveManager driveManager;

        public AddTrackedDirectoryCommand(IApplicationsManager applicationsManager, IFilesystem filesystem, IDriveManager driveManager)
        {
            this.applicationsManager = applicationsManager;
            this.filesystem = filesystem;
            this.driveManager = driveManager;
        }

        public BackupDirectory Execute(string applicationName, AddTrackedDirectoryRequest addTrackedDirectoryRequest)
        {
            if (!filesystem.IsValidPath(addTrackedDirectoryRequest.LocalPath) ||
                !driveManager.IsValidPath(addTrackedDirectoryRequest.RemotePath))
                throw new BadRequestException("Invalid local or remote path");

            if (string.IsNullOrEmpty(applicationName))
                throw new BadRequestException("Invalid application name");

            var applicationNotFoundError = new BadRequestException($"Application '${applicationName}' was not found");
            var appId = applicationsManager.GetApplicationId(applicationName);
            if (appId <= 0)
                throw applicationNotFoundError;


            var backupDirectory = new BackupDirectory()
            {
                Application = applicationName,
                Description = addTrackedDirectoryRequest.Description,
                LocalPath = addTrackedDirectoryRequest.LocalPath,
                RemotePath = addTrackedDirectoryRequest.RemotePath,
            };
            if (applicationsManager.AddDirectory(appId, backupDirectory) <= 0)
                throw applicationNotFoundError;

            return backupDirectory;
        }
    }

    public class AddTrackedDirectoryRequest
    {
        public string Description { get; set; }
        public string LocalPath { get; set; }
        public string RemotePath { get; set; }
    }
}