using System.Collections.Generic;
using System.Linq;
using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class GetApplicationCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public GetApplicationCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public GetApplicationResponse Execute(string applicationName)
        {
            var application = applicationsManager.GetApplication(applicationName);
            if (application == null)
                throw new NotFoundException($"Application '{applicationName}' could not be found.");

            var directories = applicationsManager.GetApplicationDirectories(applicationName).ToList();

            return new GetApplicationResponse()
            {
                Name = application.Name,
                Description = application.Description,
                Directories = directories
            };
        }
    }

    public class GetApplicationResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<BackupDirectory> Directories { get; set; }
    }

}