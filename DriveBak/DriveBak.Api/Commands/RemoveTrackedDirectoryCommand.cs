using System;
using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class RemoveTrackedDirectoryCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public RemoveTrackedDirectoryCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public void Execute(string application, string localPath)
        {
            if (string.IsNullOrEmpty(application))
                throw new ArgumentException("Invalid application name");

            var directoryId = applicationsManager.GetDirectoryId(localPath);
            if (directoryId <= 0)
                throw new NotFoundException($"Application '{application}' could not be found.");

            applicationsManager.RemoveDirectory(directoryId);
        }
    }
}