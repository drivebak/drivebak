using System.Reflection;
using DriveBak.Cache;
using DriveBak.DB;
using DriveBak.Logging;

namespace DriveBak.Api.Commands
{
    public class GetHealthCommand
    {
        private readonly IDBClient dbClient;
        private readonly ICacheClient cacheClient;
        private readonly ILogger<GetHealthCommand> logger;

        public GetHealthCommand(IDBClient dbClient, ICacheClient cacheClient, ILogger<GetHealthCommand> logger)
        {
            this.dbClient = dbClient;
            this.cacheClient = cacheClient;
            this.logger = logger;
        }

        public HealthCheckResponse Execute()
        {
            return new HealthCheckResponse()
            {
                Api = GetApiInfo(),
                Database = GetDbInfo(),
                Cache = GetCacheInfo(),
            };
        }

        private HealthInfo GetApiInfo()
        {
            logger.Debug("Verifying API health");
            var healthInfo = new HealthInfo
            {
                Status = Status.Healthy.ToString(),
                Name = "Drive Bak API",
                Version = Assembly.GetExecutingAssembly().GetName().Version.ToString(),
            };
            logger.Debug("Found '{@Name}' version '{@Version}'", healthInfo.Name, healthInfo.Version);
            return healthInfo;
        }

        private HealthInfo GetDbInfo()
        {
            logger.Debug("Verifying Database health");
            var dbInfo = new HealthInfo()
            {
                Name = dbClient.Name,
            };
            try
            {
                dbInfo.Version = dbClient.Version;
                dbInfo.Status = Status.Healthy.ToString();
                logger.Debug("Found database '{@Name}' version '{@Version}'", dbInfo.Name, dbInfo.Version);
            }
            catch
            {
                dbInfo.Status = Status.Unhealthy.ToString();
                logger.Error("Unable to query database '{@Name}' for version", dbInfo.Name);
            }

            return dbInfo;
        }

        private HealthInfo GetCacheInfo()
        {
            logger.Debug("Verifying Cache health");
            var cacheInfo = new HealthInfo()
            {
                Name = cacheClient.Name,
            };
            try
            {
                cacheInfo.Version = cacheClient.Version;
                cacheInfo.Status = Status.Healthy.ToString();
                logger.Debug("Found cache '{@Name}' version '{@Version}'", cacheInfo.Name, cacheInfo.Version);
            }
            catch
            {
                // Ignore
            }

            if (string.IsNullOrEmpty(cacheInfo.Version))
            {
                cacheInfo.Status = Status.Unhealthy.ToString();
                logger.Warning("Unable to query cache '{@Name}' for version", cacheInfo.Name);
            }

            return cacheInfo;
        }
    }

    public class HealthCheckResponse
    {
        public string Status
        {
            get
            {
                if (!Database.Status.Equals(DriveBak.Api.Commands.Status.Healthy.ToString()))
                    return DriveBak.Api.Commands.Status.Unhealthy.ToString();

                if (!Cache.Status.Equals(DriveBak.Api.Commands.Status.Healthy.ToString()))
                    return DriveBak.Api.Commands.Status.Degraded.ToString();

                return DriveBak.Api.Commands.Status.Healthy.ToString();
            }
        }

        public HealthInfo Api { get; set; }
        public HealthInfo Database { get; set; }
        public HealthInfo Cache { get; set; }
    }

    public class HealthInfo
    {
        public string Status { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
    }

    public enum Status
    {
        Healthy,
        Degraded,
        Unhealthy,
    }
}
