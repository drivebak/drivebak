using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class RemoveApplicationCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public RemoveApplicationCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public void Execute(string applicationName)
        {
            var appId = applicationsManager.GetApplicationId(applicationName);

            if (appId <= 0)
                throw new NotFoundException($"Unable to find application '{applicationName}'");

            applicationsManager.RemoveApplication(appId);
        }
    }
}