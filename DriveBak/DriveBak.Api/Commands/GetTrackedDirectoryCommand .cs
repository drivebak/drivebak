using System;
using System.Linq;
using DriveBak.DB;

namespace DriveBak.Api.Commands
{
    public class GetTrackedDirectoryCommand
    {
        private readonly IApplicationsManager applicationsManager;

        public GetTrackedDirectoryCommand(IApplicationsManager applicationsManager)
        {
            this.applicationsManager = applicationsManager;
        }

        public BackupDirectory Execute(string application, string localPath)
        {
            if (string.IsNullOrEmpty(application))
                throw new ArgumentException("Invalid application name");

            var directory = applicationsManager.GetDirectory(localPath).SingleOrDefault();
            if (directory == null)
                throw new NotFoundException($"Directory '{localPath}' could not be found.");

            return directory;
        }
    }
}