﻿using DriveBak.Cache;
using DriveBak.DB;
using DriveBak.Logging;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace DriveBak.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Scan(ts =>
            {
                ts.FromAssemblyOf<BackupDirectory>().AddClasses().AsSelf().AsMatchingInterface().WithTransientLifetime();
                ts.FromAssemblyOf<ILogger>().AddClasses().AsSelf().AsMatchingInterface().WithTransientLifetime();
                ts.FromCallingAssembly().AddClasses().AsSelf().AsMatchingInterface().WithTransientLifetime();
            });
            var settings = new ApiSettings();
            Log.Logger = Logging.Configuration
                .BuildLoggerConfiguration(settings.LogSettings)
                .ConfigureAppLogging(settings.LogSettings, exclusions: new []{"Serilog", "Microsoft"})
                .ConfigureWebLogging(settings.LogSettings, inclusions: new []{"Serilog", "Microsoft"})
                .CreateLogger();

            services.AddSingleton(Log.Logger);
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            var driveService = new DriveService();
            services.AddSingleton(driveService);
            services.AddSingleton(new FilesResource(driveService));
            services.AddSingleton(settings.PostgresSettings);
            services.AddSingleton(settings.IoAuthSettings);
            services.AddSingleton(settings.RedisSettings);
            services.AddSingleton<IApiSettings>(settings);
            services.AddTransient<IDBClient, PostgresClient>();
            services.AddSingleton<ICacheClient, RedisCacheClient>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSerilogRequestLogging();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}