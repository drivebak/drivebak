using System;
using System.Text;
using System.Threading.Tasks;
using DriveBak.Logging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DriveBak.Api
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate nextRequest;
        private readonly IApiSettings apiSettings;
        private readonly ILogger<ExceptionMiddleware> logger;
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public ExceptionMiddleware(RequestDelegate nextRequest, IApiSettings apiSettings, ILogger<ExceptionMiddleware> logger)
        {
            this.nextRequest = nextRequest;
            this.apiSettings = apiSettings;
            this.logger = logger;
            jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await nextRequest(httpContext);
            }
            catch (NotFoundException e)
            {
                httpContext.Response.StatusCode = 404;
                var serialized = JsonConvert.SerializeObject(e.ResponseBody, jsonSerializerSettings);
                await httpContext.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(serialized));
            }
            catch (BadRequestException e)
            {
                httpContext.Response.StatusCode = 400;
                var serialized = JsonConvert.SerializeObject(e.ResponseBody, jsonSerializerSettings);
                await httpContext.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(serialized));
            }
            catch (Exception e)
            {
                httpContext.Response.StatusCode = 500;
                var message = "An unexpected error occurred";
                var contextMessage =  message + $": {e.Message}\nTrace: {e.StackTrace}\nConfiguration:\n\t{apiSettings.ToString().Replace("\n", "\n\t")}";
                logger.Error(contextMessage);

                if (apiSettings.DebugEnabled)
                    message = contextMessage;

                var serialized = JsonConvert.SerializeObject(new {Message = message}, jsonSerializerSettings);
                await httpContext.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(serialized));
            }
        }
    }
}