using System;
using System.IO;
using DriveBak.Cache;
using DriveBak.DB;
using DriveBak.Drive;
using DriveBak.Logging;
using Microsoft.Extensions.Configuration;

namespace DriveBak.Api
{
    public interface IApiSettings
    {
        IPostgresSettings PostgresSettings { get; }
        IRedisSettings RedisSettings { get; }
        IGoogleOAuthSettings IoAuthSettings { get; }
        int ExecutionPeriodSeconds { get; }
        bool DebugEnabled { get; }
        ILogSettings LogSettings { get; }
    }

    public partial class ApiSettings : IApiSettings
    {
        private IConfigurationRoot config;

        public ApiSettings()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .AddKeyPerFile("/run/secrets", optional: true)
                .Build();
        }

        public int ExecutionPeriodSeconds => config.GetValue<int>("EXECUTION_PERIOD");

        public bool DebugEnabled => config.GetValue<bool>("DEBUG_ENABLED");

        public ILogSettings LogSettings => new LogSettings()
        {
            Level = config.GetValue<Level>("LOG_LEVEL"),
            AppLogPath = config.GetValue<string>("LOG_APP_PATH"),
            WebLogPath = config.GetValue<string>("LOG_WEB_PATH"),
            ConsoleEnabled = config.GetValue<bool>("LOG_CONSOLE"),
        };

        public IPostgresSettings PostgresSettings => new PostgresSettings()
        {
            Host = config.GetValue<string>("POSTGRES_HOST"),
            Database = config.GetValue<string>("POSTGRES_DATABASE"),
            Username = GetValueOrReadFile("POSTGRES_USERNAME"),
            Password = GetValueOrReadFile("POSTGRES_PASSWORD"),
        };

        public IRedisSettings RedisSettings => new RedisSettings
        {
            Host = config.GetValue<string>("REDIS_HOST"),
            Port = config.GetValue<int>("REDIS_PORT"),
            Password = GetValueOrReadFile("REDIS_PASSWORD"),
            CacheLengthSeconds = config.GetValue<int>("REDIS_CACHE_LENGTH_SECONDS"),
        };

        public IGoogleOAuthSettings IoAuthSettings => new GoogleOAuthSettings()
        {
            CredentialsFile = config.GetValue<string>("DRIVE_CREDS_FILE"),
            User = config.GetValue<string>("DRIVE_USER"),
        };

        private string GetValueOrReadFile(string key)
        {
            var value = config.GetValue<string>(key);
            if (String.IsNullOrEmpty(value))
            {
                try
                {
                    value = File.ReadAllText(config.GetValue<string>($"{key}_FILE"));
                }
                catch
                {
                    // ignore
                }
            }

            return value;
        }

        public override string ToString()
        {
            return $"ExecutionPeriodSeconds: {ExecutionPeriodSeconds}\n" +
                   $"DebugEnabled: {DebugEnabled}\n" +
                   $"PostgresSettings: \n\t{PostgresSettings.ToString().Replace("\n", "\n\t")}";
        }
    }
}
