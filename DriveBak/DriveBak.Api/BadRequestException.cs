using System;

namespace DriveBak.Api
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string message) : base(message) {}

        public object ResponseBody => new {Message};
    }
}