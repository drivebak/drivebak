using System;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Drive;
using DriveBak.Drive.Auth;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util;
using Google.Apis.Util.Store;
using Moq;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Test.Unit.Drive.Auth
{
    public class BaseDriveAuthFixture : WithAutoMocked<DriveAuth>
    {
        protected string User;
        protected DeviceCodeResponse DeviceCodeResponse;
        protected Mock<IOAuthRestHelper> MockRequestHelper;
        protected Mock<IAuthorizationCodeFlow> mockCodeFlow;

        [SetUp]
        public void SetupFixture()
        {
            var scope = Guid.NewGuid().ToString();
            User = Guid.NewGuid().ToString();
            var clientSecrets = new ClientSecrets
            {
                ClientId = Guid.NewGuid().ToString(),
                ClientSecret = Guid.NewGuid().ToString(),
            };

            var mockOAuthSettings = GetTestDouble<IGoogleOAuthSettings>();
            mockOAuthSettings.SetupGet(x => x.Secrets).Returns(clientSecrets);
            mockOAuthSettings.SetupGet(x => x.User).Returns(User);
            mockOAuthSettings.SetupGet(x => x.Scope).Returns(scope);

            DeviceCodeResponse = new DeviceCodeResponse
            {
                DeviceCode = Guid.NewGuid().ToString(),
                UserCode = Guid.NewGuid().ToString(),
                ExpiresIn = 60,
                Interval = 10,
                VerificationUrl = Guid.NewGuid().ToString(),
            };

            var mockRestResponse = GetTestDouble<IRestResponse<DeviceCodeResponse>>();
            mockRestResponse.SetupGet(x => x.Data)
                .Returns(DeviceCodeResponse);

            MockRequestHelper = GetTestDouble<IOAuthRestHelper>();
            MockRequestHelper.Setup(x => x.ExecuteRequestAsync<DeviceCodeResponse>(
                    Method.POST,
                    "device/code",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockRestResponse.Object);

            mockCodeFlow = GetTestDouble<IAuthorizationCodeFlow>();
            mockCodeFlow.SetupGet(x => x.Clock)
                .Returns(SystemClock.Default);
        }
    }

    public class WhenGettingDeviceCodes : BaseDriveAuthFixture
    {
        private DeviceCodeResponse actual;

        [SetUp]
        public async Task Setup()
        {
            actual = await Sut.GetDeviceCodes(It.IsAny<CancellationToken>());
        }

        [Test]
        public void ShouldExecuteRequest() =>
            MockRequestHelper
                .Verify(x => x.ExecuteRequestAsync<DeviceCodeResponse>(
                    Method.POST,
                    "device/code",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()));

        [Test]
        public void ShouldReturnDeviceCode() =>
            Assert.AreEqual(DeviceCodeResponse, actual);
    }

    public class WhenGettingUserCredentialsAndTokenIsCachedAndValid : BaseDriveAuthFixture
    {
        private UserCredential actualUserCredential;
        private TokenResponse tokenResponse;

        [SetUp]
        public async Task Setup()
        {
            tokenResponse = new TokenResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresInSeconds = 60,
                IdToken = Guid.NewGuid().ToString(),
                IssuedUtc = DateTime.UtcNow,
                RefreshToken = Guid.NewGuid().ToString(),
                Scope = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            mockCodeFlow.Setup(x => x.LoadTokenAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(tokenResponse);

            actualUserCredential = await Sut.GetUserCredentialsAsync(CancellationToken.None);
        }

        [Test]
        public void ShouldNotRequestDeviceCode() =>
            GetTestDouble<IOAuthRestHelper>()
                .Verify(x => x.ExecuteRequestAsync<DeviceCodeResponse>(
                        Method.POST,
                        "device/code",
                        It.IsAny<object>(),
                        It.IsAny<CancellationToken>()),
                    Times.Never);

        [Test]
        public void ShouldNotAttemptToExchangeForToken() =>
            mockCodeFlow.Verify(x => x.ExchangeCodeForTokenAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()),
                Times.Never);

        [Test]
        public void ShouldSetCorrectCodeFlow() =>
            Assert.AreEqual(mockCodeFlow.Object, actualUserCredential.Flow);

        [Test]
        public void ShouldSetCorrectUser() =>
            Assert.AreEqual(User, actualUserCredential.UserId);

        [Test]
        public void ShouldSetCorrectTokenResponse() =>
            Assert.AreEqual(tokenResponse, actualUserCredential.Token);
    }

    public class WhenGettingUserCredentialsAndTokenIsCachedButExpired : BaseDriveAuthFixture
    {
        private UserCredential actualUserCredential;
        private TokenResponse tokenResponse;

        [SetUp]
        public async Task Setup()
        {
            var cachedTokenResponse = new TokenResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresInSeconds = 60,
                IdToken = Guid.NewGuid().ToString(),
                IssuedUtc = DateTime.UtcNow.Subtract(TimeSpan.FromDays(1)),
                RefreshToken = null,
                Scope = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            tokenResponse = new TokenResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresInSeconds = 60,
                IdToken = Guid.NewGuid().ToString(),
                IssuedUtc = DateTime.UtcNow.Subtract(TimeSpan.FromDays(1)),
                RefreshToken = Guid.NewGuid().ToString(),
                Scope = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };


            mockCodeFlow.Setup(x => x.LoadTokenAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(cachedTokenResponse);

            mockCodeFlow.Setup(x => x.ExchangeCodeForTokenAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(tokenResponse);

            actualUserCredential = await Sut.GetUserCredentialsAsync(CancellationToken.None);
        }

        [Test]
        public void ShouldRequestDeviceCode() =>
            MockRequestHelper
                .Verify(x => x.ExecuteRequestAsync<DeviceCodeResponse>(
                    Method.POST,
                    "device/code",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()));

        [Test]
        public void ShouldWriteDeviceCodesToDataStore() =>
            GetTestDouble<IDataStore>()
                .Verify(x => x.StoreAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));

        [Test]
        public void ShouldExchangeForToken() =>
            mockCodeFlow.Verify(x => x.ExchangeCodeForTokenAsync(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<CancellationToken>())
            );

        [Test]
        public void ShouldSetCorrectCodeFlow() =>
            Assert.AreEqual(mockCodeFlow.Object, actualUserCredential.Flow);

        [Test]
        public void ShouldSetCorrectUser() =>
            Assert.AreEqual(User, actualUserCredential.UserId);

        [Test]
        public void ShouldSetCorrectTokenResponse() =>
            Assert.AreEqual(tokenResponse, actualUserCredential.Token);
    }

    public class WhenGettingUserCredentialsAndTokenIsNotCached : BaseDriveAuthFixture
    {
        private UserCredential actualUserCredential;
        private TokenResponse tokenResponse;

        [SetUp]
        public async Task Setup()
        {
            tokenResponse = new TokenResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresInSeconds = 60,
                IdToken = Guid.NewGuid().ToString(),
                IssuedUtc = DateTime.UtcNow,
                RefreshToken = Guid.NewGuid().ToString(),
                Scope = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            mockCodeFlow.Setup(x => x.LoadTokenAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync((TokenResponse)null);

            mockCodeFlow.Setup(x => x.ExchangeCodeForTokenAsync(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(tokenResponse);

            actualUserCredential = await Sut.GetUserCredentialsAsync(CancellationToken.None);
        }

        [Test]
        public void ShouldRequestDeviceCode() =>
            MockRequestHelper
                .Verify(x => x.ExecuteRequestAsync<DeviceCodeResponse>(
                    Method.POST,
                    "device/code",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()));

        [Test]
        public void ShouldWriteDeviceCodesToDataStore() =>
            GetTestDouble<IDataStore>()
                .Verify(x => x.StoreAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));

        [Test]
        public void ShouldExchangeForToken() =>
            mockCodeFlow.Verify(x => x.ExchangeCodeForTokenAsync(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<CancellationToken>()));

        [Test]
        public void ShouldSetCorrectCodeFlow() =>
            Assert.AreEqual(mockCodeFlow.Object, actualUserCredential.Flow);

        [Test]
        public void ShouldSetCorrectUser() =>
            Assert.AreEqual(User, actualUserCredential.UserId);

        [Test]
        public void ShouldSetCorrectTokenResponse() =>
            Assert.AreEqual(tokenResponse, actualUserCredential.Token);
    }
}
