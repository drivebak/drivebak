using System;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util.Store;
using Moq;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.Auth.DriveBakAuthorizationCodeFlow
{
    public class WhenLoadingToken : BaseDriveBakAuthorizationCodeFlowFixture
    {
        [SetUp]
        public async Task Setup()
        {
            await Sut.LoadTokenAsync(UserId, CancellationToken.None);
        }

        [Test]
        public void ShouldGetFromDataStore() => GetTestDouble<IDataStore>()
            .Verify(d => d.GetAsync<TokenResponse>(UserId));
    }

    public class WhenDeletingToken : BaseDriveBakAuthorizationCodeFlowFixture
    {
        [SetUp]
        public async Task Setup()
        {
            await Sut.DeleteTokenAsync(UserId, CancellationToken.None);
        }

        [Test]
        public void ShouldDeleteFromDataStore() => GetTestDouble<IDataStore>()
            .Verify(d => d.DeleteAsync<TokenResponse>(UserId));
    }

    public class WhenCreatingAuthorizationCodeRequest : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private string redirectUri;
        private AuthorizationCodeRequestUrl authorizationCodeRequestUri;

        [SetUp]
        public void Setup()
        {
            redirectUri = "https://example.com";
            authorizationCodeRequestUri = Sut.CreateAuthorizationCodeRequest(redirectUri);
        }

        [Test]
        public void ShouldSetCorrectUri() =>
            Assert.AreEqual(new Uri(redirectUri), authorizationCodeRequestUri.AuthorizationServerUrl);

        [Test]
        public void ShouldSetCorrectClientId() =>
            Assert.AreEqual(ClientSecrets.ClientId, authorizationCodeRequestUri.ClientId);

        [Test]
        public void ShouldSetCorrectScope() =>
            Assert.AreEqual(Scope, authorizationCodeRequestUri.Scope);
    }

    public class WhenRevokingToken : BaseDriveBakAuthorizationCodeFlowFixture
    {
        [SetUp]
        public async Task Setup()
        {
            await Sut.RevokeTokenAsync(UserId, It.IsAny<string>(), CancellationToken.None);
        }

        [Test]
        public void ShouldDeleteFromDataStore() => GetTestDouble<IDataStore>()
            .Verify(d => d.DeleteAsync<TokenResponse>(UserId));
    }
}