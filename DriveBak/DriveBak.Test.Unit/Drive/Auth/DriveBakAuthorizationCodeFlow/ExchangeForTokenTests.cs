using System;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Drive.Auth;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util.Store;
using Moq;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Test.Unit.Drive.Auth.DriveBakAuthorizationCodeFlow
{
    public class WhenExchangingCodeForTokenAndTokenIsReturned : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private TokenResponse actualToken;
        private TokenResponse expectedToken;

        [SetUp]
        public async Task Setup()
        {
            var accessResponse = new AccessResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresIn = 60,
                Error = null,
                RefreshToken = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.Data)
                .Returns(accessResponse);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            expectedToken = accessResponse.GetTokenResponse(DateTime.Now, Scope);
            await Task.Delay(TimeSpan.FromMilliseconds(100));
            actualToken = await Sut.ExchangeCodeForTokenAsync(UserId, Code, It.IsAny<string>(), CancellationToken.None);
        }

        [Test]
        public void ShouldStoreToken() =>
            GetTestDouble<IDataStore>().Verify(d => d.StoreAsync(UserId, actualToken));

        [Test]
        public void ShouldReturnCorrectToken() =>
            Assert.AreEqual(expectedToken.AccessToken, actualToken.AccessToken);

        [Test]
        public void ShouldReturnCorrectTokenType() =>
            Assert.AreEqual(expectedToken.TokenType, actualToken.TokenType);

        [Test]
        public void ShouldReturnCorrectRefreshToken() =>
            Assert.AreEqual(expectedToken.RefreshToken, actualToken.RefreshToken);

        [Test]
        public void ShouldReturnCorrectExpiration() =>
            Assert.AreEqual(expectedToken.ExpiresInSeconds, actualToken.ExpiresInSeconds);

        [Test]
        public void ShouldReturnCorrectScope() =>
            Assert.AreEqual(expectedToken.Scope, actualToken.Scope);

        [Test]
        public void ShouldReturnValidDate() =>
            Assert.LessOrEqual(expectedToken.IssuedUtc, actualToken.IssuedUtc);
    }

    public class WhenExchangingCodeForTokenAndAccessIsDenied : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private Exception actualException;

        [SetUp]
        public async Task Setup()
        {
            var accessResponse = new AccessResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresIn = 60,
                Error = AccessError.AccessDenied,
                RefreshToken = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.Data)
                .Returns(accessResponse);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            try
            {
                await Sut.ExchangeCodeForTokenAsync(UserId, Code, It.IsAny<string>(), CancellationToken.None);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Test]
        public void ShouldThrowException() =>
            Assert.NotNull(actualException);

        [Test]
        public void ShouldThrowAccessException() =>
            Assert.AreEqual(typeof(AccessException), actualException.GetType());
    }

    public class WhenExchangingCodeForTokenAndUnableToQueryAccess : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private Exception actualException;

        [SetUp]
        public async Task Setup()
        {
            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.Data)
                .Returns((AccessResponse)null);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            try
            {
                await Sut.ExchangeCodeForTokenAsync(UserId, Code, It.IsAny<string>(), CancellationToken.None);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Test]
        public void ShouldThrowException() =>
            Assert.NotNull(actualException);

        [Test]
        public void ShouldThrowAccessException() =>
            Assert.AreEqual(typeof(AccessException), actualException.GetType());
    }
}