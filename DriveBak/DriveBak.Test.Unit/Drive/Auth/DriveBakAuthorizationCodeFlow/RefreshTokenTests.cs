using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using DriveBak.Drive.Auth;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util.Store;
using Moq;
using NUnit.Framework;
using RestSharp;

namespace DriveBak.Test.Unit.Drive.Auth.DriveBakAuthorizationCodeFlow
{
    public class WhenRefreshingTokenAndTokenIsReturnedWithoutDriveBakAuthorizationCodeFlow : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private TokenResponse actualToken;
        private TokenResponse expectedToken;

        [SetUp]
        public async Task Setup()
        {
            var accessResponse = new AccessResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresIn = 60,
                Error = null,
                RefreshToken = null,
                TokenType = Guid.NewGuid().ToString(),
            };

            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.Data)
                .Returns(accessResponse);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            expectedToken = accessResponse.GetTokenResponse(DateTime.Now, Scope);
            await Task.Delay(TimeSpan.FromMilliseconds(100));
            actualToken = await Sut.RefreshTokenAsync(UserId, RefreshToken, CancellationToken.None);
        }

        [Test]
        public void ShouldStoreToken() =>
            GetTestDouble<IDataStore>().Verify(d => d.StoreAsync(UserId, actualToken));

        [Test]
        public void ShouldReturnCorrectToken() =>
            Assert.AreEqual(expectedToken.AccessToken, actualToken.AccessToken);

        [Test]
        public void ShouldReturnCorrectTokenType() =>
            Assert.AreEqual(expectedToken.TokenType, actualToken.TokenType);

        [Test]
        public void ShouldReturnCorrectRefreshToken() =>
            Assert.AreEqual(RefreshToken, actualToken.RefreshToken);

        [Test]
        public void ShouldReturnCorrectExpiration() =>
            Assert.AreEqual(expectedToken.ExpiresInSeconds, actualToken.ExpiresInSeconds);

        [Test]
        public void ShouldReturnCorrectScope() =>
            Assert.AreEqual(expectedToken.Scope, actualToken.Scope);

        [Test]
        public void ShouldReturnValidDate() =>
            Assert.LessOrEqual(expectedToken.IssuedUtc, actualToken.IssuedUtc);
    }

    public class WhenRefreshingTokenAndTokenIsReturnedWithDriveBakAuthorizationCodeFlow : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private TokenResponse actualToken;
        private TokenResponse expectedToken;

        [SetUp]
        public async Task Setup()
        {
            var accessResponse = new AccessResponse
            {
                AccessToken = Guid.NewGuid().ToString(),
                ExpiresIn = 60,
                Error = null,
                RefreshToken = Guid.NewGuid().ToString(),
                TokenType = Guid.NewGuid().ToString(),
            };

            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.Data)
                .Returns(accessResponse);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            expectedToken = accessResponse.GetTokenResponse(DateTime.Now, Scope);
            await Task.Delay(TimeSpan.FromMilliseconds(100));
            actualToken = await Sut.RefreshTokenAsync(UserId, RefreshToken, CancellationToken.None);
        }

        [Test]
        public void ShouldStoreToken() =>
            GetTestDouble<IDataStore>().Verify(d => d.StoreAsync(UserId, actualToken));

        [Test]
        public void ShouldReturnCorrectToken() =>
            Assert.AreEqual(expectedToken.AccessToken, actualToken.AccessToken);

        [Test]
        public void ShouldReturnCorrectTokenType() =>
            Assert.AreEqual(expectedToken.TokenType, actualToken.TokenType);

        [Test]
        public void ShouldReturnCorrectRefreshToken() =>
            Assert.AreEqual(expectedToken.RefreshToken, actualToken.RefreshToken);

        [Test]
        public void ShouldReturnCorrectExpiration() =>
            Assert.AreEqual(expectedToken.ExpiresInSeconds, actualToken.ExpiresInSeconds);

        [Test]
        public void ShouldReturnCorrectScope() =>
            Assert.AreEqual(expectedToken.Scope, actualToken.Scope);

        [Test]
        public void ShouldReturnValidDate() =>
            Assert.LessOrEqual(expectedToken.IssuedUtc, actualToken.IssuedUtc);
    }

    public class WhenRefreshingTokenAndResponseIsError500 : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private Exception actualException;

        [SetUp]
        public async Task Setup()
        {
            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.StatusCode)
                .Returns(HttpStatusCode.InternalServerError);
            mockResponse.SetupGet(x => x.Data)
                .Returns((AccessResponse)null);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            try
            {
                await Sut.RefreshTokenAsync(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Test]
        public void ShouldThrowException() =>
            Assert.NotNull(actualException);

        [Test]
        public void ShouldThrowAccessException() =>
            Assert.AreEqual(typeof(AccessException), actualException.GetType());

        [Test]
        public void ShouldNOTDeleteToken() =>
            GetTestDouble<IDataStore>().Verify(d => d.DeleteAsync<TokenResponse>(It.IsAny<string>()), Times.Never);
    }

    public class WhenRefreshingTokenAndResponseIsErrorNOT500 : BaseDriveBakAuthorizationCodeFlowFixture
    {
        private Exception actualException;

        [SetUp]
        public async Task Setup()
        {
            var mockResponse = GetTestDouble<IRestResponse<AccessResponse>>();
            mockResponse.SetupGet(x => x.StatusCode)
                .Returns(HttpStatusCode.Forbidden);
            mockResponse.SetupGet(x => x.Data)
                .Returns((AccessResponse)null);

            GetTestDouble<IOAuthRestHelper>()
                .Setup(x => x.ExecuteRequestAsync<AccessResponse>(Method.POST,
                    "token",
                    It.IsAny<object>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(mockResponse.Object);

            try
            {
                await Sut.RefreshTokenAsync(UserId, It.IsAny<string>(), CancellationToken.None);
            }
            catch (Exception e)
            {
                actualException = e;
            }
        }

        [Test]
        public void ShouldThrowException() =>
            Assert.NotNull(actualException);

        [Test]
        public void ShouldThrowAccessException() =>
            Assert.AreEqual(typeof(AccessException), actualException.GetType());

        [Test]
        public void ShouldDeleteToken() =>
            GetTestDouble<IDataStore>().Verify(d => d.DeleteAsync<TokenResponse>(UserId));
    }
}