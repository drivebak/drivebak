using System;
using DriveBak.Drive;
using Google.Apis.Auth.OAuth2;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.Auth.DriveBakAuthorizationCodeFlow
{
    public class BaseDriveBakAuthorizationCodeFlowFixture : WithAutoMocked<DriveBak.Drive.Auth.DriveBakAuthorizationCodeFlow>
    {
        protected string RefreshToken;
        protected string UserId;
        protected string Scope;
        protected ClientSecrets ClientSecrets;
        protected string Code;

        [SetUp]
        public void BaseSetup()
        {
            ClientSecrets = new ClientSecrets
            {
                ClientId = Guid.NewGuid().ToString(),
                ClientSecret = Guid.NewGuid().ToString(),
            };

            Scope = Guid.NewGuid().ToString();
            RefreshToken = Guid.NewGuid().ToString();
            UserId = Guid.NewGuid().ToString();
            Code = Guid.NewGuid().ToString();

            var oAuthSettings = GetTestDouble<IGoogleOAuthSettings>();
            oAuthSettings.SetupGet(x => x.Scopes)
                .Returns(new[] {Scope});
            oAuthSettings.SetupGet(x => x.Scope)
                .Returns(Scope);
            oAuthSettings.SetupGet(x => x.Secrets)
                .Returns(ClientSecrets);
        }
    }
}