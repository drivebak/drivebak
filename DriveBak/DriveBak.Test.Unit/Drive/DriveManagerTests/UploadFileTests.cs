using System;
using System.Threading.Tasks;
using DriveBak.Cache;
using DriveBak.Drive;
using DriveBak.Test.Helpers;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Upload;
using Moq;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.DriveManagerTests
{
    public class WhenUploadingAFile : WithAutoMocked<DriveManager>
    {
        private UploadStatus uploadStatus;
        private string redisKeyStatus;
        private BackupDirectory backupDir;
        private string fileName;
        private string contentType;
        private int bytesSent;
        private string redisKeyBytes;

        [SetUp]
        public async Task Setup()
        {
            backupDir = TestHelpers.GetBackupDirectory();
            fileName = Guid.NewGuid().ToString();
            contentType = "application/gzip";

            redisKeyStatus = $"drive:file:{backupDir.RemotePath}/{fileName}:progress";
            redisKeyBytes = $"drive:file:{backupDir.RemotePath}/{fileName}:bytes";

            Action<IUploadProgress> progressChangedCommand = null;
            Action<File> responseReceivedCommand = null;
            GetTestDouble<IDriveClient>().Setup(x => x.UploadFileAsync(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<Action<IUploadProgress>>(),
                It.IsAny<Action<File>>()
            )).Callback<string, string, string, string, Action<IUploadProgress>, Action<File>>(
                (localPath, remotePath, fileName, contentType, progressChanged, responseReceived) =>
                {
                    progressChangedCommand = progressChanged;
                    responseReceivedCommand = responseReceived;
                });
            await Sut.UploadFileAsync(backupDir.LocalPath, backupDir.RemotePath, fileName, contentType);

            uploadStatus = UploadStatus.Uploading;
            bytesSent = 1000;
            var uploadProgress = GetTestDouble<IUploadProgress>();
            uploadProgress.SetupGet(x => x.Status)
                .Returns(uploadStatus);
            uploadProgress.SetupGet(x => x.BytesSent)
                .Returns(bytesSent);
            progressChangedCommand.Invoke(uploadProgress.Object);
            responseReceivedCommand.Invoke(new File()
            {
                Name = fileName,
            });
        }

        [Test]
        public void ShouldCallDriveClientWithCorrectParameters()
        {
            GetTestDouble<IDriveClient>().Verify(x => x.UploadFileAsync(
                backupDir.LocalPath,
                backupDir.RemotePath,
                fileName,
                contentType,
                It.IsAny<Action<IUploadProgress>>(),
                It.IsAny<Action<File>>()
            ));
        }

        [Test]
        public void ShouldUpdateStatusInCacheOnProgress()
        {
            GetTestDouble<ICacheClient>()
                .Verify(x => x.SetAsync(redisKeyStatus, uploadStatus.ToString()));
        }

        [Test]
        public void ShouldUpdateBytesInCacheOnProgress()
        {
            GetTestDouble<ICacheClient>()
                .Verify(x => x.SetAsync(redisKeyBytes, bytesSent.ToString()));
        }

        [Test]
        public void ShouldUpdateStatusInCacheOnCompletion()
        {
            GetTestDouble<ICacheClient>()
                .Verify(x => x.SetAsync(redisKeyStatus, UploadStatus.Completed.ToString()));
        }
    }
}