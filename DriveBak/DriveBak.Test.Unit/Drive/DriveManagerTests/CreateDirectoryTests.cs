using System;
using DriveBak.Drive;
using Moq;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.DriveManagerTests
{
    public class WhenCreatingDirectoryAndPathIsValid : WithAutoMocked<DriveManager>
    {
        [Test]
        public void ShouldCallCallDriveClientForCreation()
        {
            var path = $"{Guid.NewGuid()}/{Guid.NewGuid()}";
            Sut.CreateFolderAsync(path);
            GetTestDouble<IDriveClient>().Verify(x => x.CreateFolderAsync(path));
        }
    }

    public class WhenCreatingDirectoryAndPathIsNotValid : WithAutoMocked<DriveManager>
    {
        [Test]
        public void ShouldNotCallDriveClientForCreation()
        {
            var path = $"{Guid.NewGuid()}${Guid.NewGuid()}";
            Sut.CreateFolderAsync(path);
            GetTestDouble<IDriveClient>().Verify(x => x.CreateFolderAsync(path), Times.Never);
        }
    }
}