using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DriveBak.Drive;
using Google.Apis.Drive.v3.Data;
using Moq;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.DriveManagerTests
{
    public class WhenGettingFileNamesAndDirectoryDoesNotExist : WithAutoMocked<DriveManager>
    {
        private IEnumerable<string> actualFiles;
        private Mock<IDriveClient> driveClient;
        private string path;

        [SetUp]
        public async Task Setup()
        {
            path = $"drivebak/test/unit/{Guid.NewGuid()}";
            driveClient = GetTestDouble<IDriveClient>();
            driveClient.Setup(x => x.GetFolderIdAsync(path)).Returns(Task.FromResult((string)null));

            actualFiles = await Sut.GetFileNamesAsync(path);
        }

        [Test]
        public void ShouldQueryFolderId() =>
            driveClient.Verify(x => x.GetFolderIdAsync(path));

        [Test]
        public void ShouldNotQueryChildren() =>
            driveClient.Verify(x => x.GetChildrenAsync(It.IsAny<string>()), Times.Never);

        [Test]
        public void ShouldReturnNull() =>
            Assert.IsNull(actualFiles);
    }

    public class WhenGettingFileNamesAndDirectoryDoesNotContainsFiles : WithAutoMocked<DriveManager>
    {
        private IEnumerable<string> actualFiles;
        private Mock<IDriveClient> driveClient;
        private string path;
        private string folderId;

        [SetUp]
        public async Task Setup()
        {
            path = $"drivebak/test/unit/{Guid.NewGuid()}";
            folderId = Guid.NewGuid().ToString();
            driveClient = GetTestDouble<IDriveClient>();
            driveClient.Setup(x => x.GetFolderIdAsync(path)).Returns(Task.FromResult(folderId));
            driveClient.Setup(x => x.GetChildrenAsync(folderId))
                .Returns(Task.FromResult((IEnumerable<File>) new List<File>()));

            actualFiles = await Sut.GetFileNamesAsync(path);
        }

        [Test]
        public void ShouldQueryFolderId() =>
            driveClient.Verify(x => x.GetFolderIdAsync(path));

        [Test]
        public void ShouldQueryChildren() =>
            driveClient.Verify(x => x.GetChildrenAsync(folderId));

        [Test]
        public void ShouldReturnEmptyList() =>
            Assert.IsEmpty(actualFiles);
    }

    public class WhenGettingFileNamesAndDirectoryContainsFiles : WithAutoMocked<DriveManager>
    {
        private IEnumerable<string> actualFiles;
        private IList<File> files;
        private Mock<IDriveClient> driveClient;
        private string path;
        private string folderId;

        [SetUp]
        public async Task Setup()
        {
            path = $"drivebak/test/unit/{Guid.NewGuid()}";
            files = new List<File>();
            for (var i = 0; i < 10; i++)
            {
                var file = new File();
                file.Name = Guid.NewGuid().ToString();
                files.Add(file);
            }

            folderId = Guid.NewGuid().ToString();
            driveClient = GetTestDouble<IDriveClient>();
            driveClient.Setup(x => x.GetFolderIdAsync(path)).Returns(Task.FromResult(folderId));
            driveClient.Setup(x => x.GetChildrenAsync(folderId)).Returns(Task.FromResult((IEnumerable<File>) files));

            actualFiles = await Sut.GetFileNamesAsync(path);
        }

        [Test]
        public void ShouldQueryFolderId() =>
            driveClient.Verify(x => x.GetFolderIdAsync(path));

        [Test]
        public void ShouldQueryChildren() =>
            driveClient.Verify(x => x.GetChildrenAsync(folderId));

        [Test]
        public void ShouldReturnListOfFilenames() =>
            CollectionAssert.AreEquivalent(files.Select(f => f.Name), actualFiles);
    }
}