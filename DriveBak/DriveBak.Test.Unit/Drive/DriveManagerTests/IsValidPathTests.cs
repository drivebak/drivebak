using System.Collections.Generic;
using DriveBak.Drive;
using NUnit.Framework;

namespace DriveBak.Test.Unit.Drive.DriveManagerTests
{
    public class WhenVerifyingPathIsValid : WithAutoMocked<DriveManager>
    {
        public static IEnumerable<TestCaseData> ValidSources()
        {
            var validNames = new List<(string, string)>()
            {
                ("JustLetters", @"abcdefg"),
                ("LettersAndNumbers", @"abc1232435defg"),
                ("LettersParenthesis", @"abc1232435(defg)"),
                ("LettersAndDashAndUnderscore", @"abc-12324_35(defg)"),
                ("AllWithCapitals", @"ABC-12324_35(DEFG)"),
            };
            foreach (var validName in validNames)
            {
                yield return new TestCaseData(validName.Item2)
                    .SetName($"{nameof(ShouldReturnTrue)}When{validName.Item1}");
                yield return new TestCaseData($"{validName.Item2}/{validName.Item2}")
                    .SetName($"{nameof(ShouldReturnTrue)}WhenTwoLevel{validName.Item1}");
                yield return new TestCaseData($"{validName.Item2}/{validName.Item2}/{validName.Item2}")
                    .SetName($"{nameof(ShouldReturnTrue)}WhenThreeLevel{validName.Item1}");
            }
        }

        public static IEnumerable<TestCaseData> InvalidSources()
        {
            var validName = @"abcdefg";

            var invalidNames = new List<(string, string)>()
            {
                ("Empty", @""),
                ("Space", @"ab cdefg"),
                ("WrongSlash", @"ab\cdefg"),
                ("Dollar", @"ab$cdefg"),
                ("Equals", @"ab=cdefg"),
                ("Tick", @"ab`cdefg"),
                ("Tilda", @"ab~cdefg"),
            };

            foreach (var invalidName in invalidNames)
            {
                yield return new TestCaseData(invalidName.Item2)
                    .SetName($"{nameof(ShouldReturnFalse)}When{invalidName.Item1}");
                yield return new TestCaseData($"{validName}/{invalidName.Item2}")
                    .SetName($"{nameof(ShouldReturnFalse)}WhenTwoLevel{invalidName.Item1}");
                yield return new TestCaseData($"{validName}/{invalidName.Item2}/{invalidName.Item2}")
                    .SetName($"{nameof(ShouldReturnFalse)}WhenThreeLevelFirstValid{invalidName.Item1}");
                yield return new TestCaseData($"{invalidName.Item2}/{validName}/{invalidName.Item2}")
                    .SetName($"{nameof(ShouldReturnFalse)}WhenThreeLevelMiddleValid{invalidName.Item1}");
            }
        }

        [Test]
        [TestCaseSource(nameof(ValidSources))]
        public void ShouldReturnTrue(string path) => Assert.That(Sut.IsValidPath(path));

        [Test]
        [TestCaseSource(nameof(InvalidSources))]
        public void ShouldReturnFalse(string path) => Assert.False(Sut.IsValidPath(path));
    }
}