using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DriveBak.Cache;
using Moq;
using NUnit.Framework;
using StackExchange.Redis;

namespace DriveBak.Test.Unit.Cache
{
    public class WhenSettingAValue : WithAutoMocked<RedisCacheClient>
    {
        private string expected;
        private Mock<IDatabase> databaseMock;
        private string key;
        private int timespan;

        [SetUp]
        public async Task Setup()
        {
            databaseMock = GetTestDouble<IDatabase>();
            timespan = 60;
            GetTestDouble<IRedisConnection>()
                .SetupGet(x => x.Database)
                .Returns(databaseMock.Object);
            GetTestDouble<IRedisSettings>().SetupGet(x => x.CacheLengthSeconds)
                .Returns(timespan);

            key = $"test:drive:folder:{expected}:id";
            expected = Guid.NewGuid().ToString();

            await Sut.SetAsync(key, expected);
        }

        [Test]
        public void ShouldCallSetOnDatabase() =>
            databaseMock.Verify(x => x.StringSetAsync(key, expected, TimeSpan.FromSeconds(timespan), It.IsAny<When>(), It.IsAny<CommandFlags>()));
    }

    public class WhenGettingAnExistingValue : WithAutoMocked<RedisCacheClient>
    {
        private string actualOut;
        private bool cacheHit;
        private string expected;
        private Mock<IDatabase> databaseMock;
        private string key;

        [SetUp]
        public void Setup()
        {
            databaseMock = GetTestDouble<IDatabase>();
            GetTestDouble<IRedisConnection>()
                .SetupGet(x => x.Database)
                .Returns(databaseMock.Object);

            key = $"test:drive:folder:{expected}:id";
            expected = Guid.NewGuid().ToString();
            databaseMock.Setup(x => x.StringGet(key, It.IsAny<CommandFlags>())).Returns(expected);

            cacheHit = Sut.TryGet(key, out actualOut);
        }

        [Test]
        public void ShouldQueryCache() =>
            databaseMock.Verify(x => x.StringGet(key, It.IsAny<CommandFlags>()));

        [Test]
        public void ShouldReturnTrue() =>
            Assert.That(cacheHit);

        [Test]
        public void ShouldReturnShouldSetOutParamToId() =>
            Assert.AreEqual(expected, actualOut);
    }

    public class WhenGettingAValueThatDoesNotExist : WithAutoMocked<RedisCacheClient>
    {
        private bool cacheHit;
        private string expected;
        private Mock<IDatabase> databaseMock;
        private string key;

        [SetUp]
        public void Setup()
        {
            databaseMock = GetTestDouble<IDatabase>();
            GetTestDouble<IRedisConnection>()
                .SetupGet(x => x.Database)
                .Returns(databaseMock.Object);

            key = $"test:drive:folder:{expected}:id";
            expected = Guid.NewGuid().ToString();
            databaseMock.Setup(x => x.StringGet(key, It.IsAny<CommandFlags>())).Returns("");

            cacheHit = Sut.TryGet(key, out string _);
        }

        [Test]
        public void ShouldQueryCache() =>
            databaseMock.Verify(x => x.StringGet(key, It.IsAny<CommandFlags>()));

        [Test]
        public void ShouldReturnFalse() =>
            Assert.False(cacheHit);
    }

    public class WhenGettingAVersionAndServerConnected : WithAutoMocked<RedisCacheClient>
    {
        private Mock<IServer> serverMock;
        private Version version;

        [SetUp]
        public void Setup()
        {
            version = new Version(1, 2, 3, 4);

            serverMock = GetTestDouble<IServer>();
            serverMock.SetupGet(s => s.Version).Returns(version);
            serverMock.SetupGet(s => s.IsConnected).Returns(true);
            GetTestDouble<IRedisConnection>()
                .SetupGet(x => x.Servers)
                .Returns(new List<IServer>(){serverMock.Object});
        }

        [Test]
        public void ShouldReturnCorrectVersion() =>
            Assert.AreEqual(version.ToString(), Sut.Version);
    }

    public class WhenGettingAVersionAndNoServerConnected : WithAutoMocked<RedisCacheClient>
    {
        private Mock<IServer> serverMock;

        [SetUp]
        public void Setup()
        {
            serverMock = GetTestDouble<IServer>();
            serverMock.SetupGet(s => s.IsConnected).Returns(false);
            GetTestDouble<IRedisConnection>()
                .SetupGet(x => x.Servers)
                .Returns(new List<IServer>(){serverMock.Object});
        }

        [Test]
        public void ShouldReturnCorrectVersion() =>
            Assert.IsNull(Sut.Version);
    }
}