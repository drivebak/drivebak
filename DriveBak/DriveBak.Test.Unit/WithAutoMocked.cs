using Autofac.Extras.Moq;
using DriveBak.Logging;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Test.Unit
{
    public abstract class WithAutoMocked<T> where T : class
    {
        private static AutoMock autoMock;
        protected static T Sut => autoMock.Create<T>();
        protected static Mock<TM> GetTestDouble<TM>() where TM : class => autoMock.Mock<TM>();

        [SetUp]
        public void SetUp()
        {
            autoMock = AutoMock.GetLoose();
            autoMock.Provide<ILogger<T>>(new TestLogger<T>());
        }

        protected static void Provide<T1>(T1 instance) where T1 : class => autoMock.Provide(instance);

        [TearDown]
        public void TearDown() => autoMock?.Dispose();
    }
}