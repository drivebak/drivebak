using Autofac.Extras.Moq;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit
{
    public abstract class WithAutoMocked<T> where T : class
    {
        private static AutoMock autoMock;
        protected static T Sut => autoMock.Create<T>();
        protected static Mock<TM> GetTestDouble<TM>() where TM : class => autoMock.Mock<TM>();

        [SetUp]
        public void SetUp() => autoMock = AutoMock.GetLoose();

        [TearDown]
        public void TearDown() => autoMock?.Dispose();
    }
}