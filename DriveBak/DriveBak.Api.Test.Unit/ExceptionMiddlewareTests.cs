using System;
using System.IO;
using System.Net;
using System.Text;
using DriveBak.Cache;
using DriveBak.DB;
using DriveBak.Drive;
using DriveBak.Logging;
using DriveBak.Test.Helpers;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit
{
    public class BaseExceptionThrownFixture : WithAutoMocked<ExceptionMiddleware>
    {
        protected Mock<HttpResponse> MockResponse;
        protected MemoryStream ResponseBody;
        protected IApiSettings ApiSettings;
        protected string Message;

        protected void Setup(string message, RequestDelegate requestDelegate, IApiSettings apiSettings=null)
        {
            Message = message;
            ApiSettings = apiSettings ?? new TestApiSettings();
            var middleware = new ExceptionMiddleware(requestDelegate, ApiSettings, new TestLogger<ExceptionMiddleware>());
            MockResponse = new Mock<HttpResponse>();
            ResponseBody = new MemoryStream();
            MockResponse.Setup(x => x.Body).Returns(ResponseBody);
            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(x => x.Response).Returns(MockResponse.Object);
            middleware.InvokeAsync(mockHttpContext.Object).Wait();
        }

        protected class TestApiSettings : IApiSettings
        {
            public IPostgresSettings PostgresSettings => null;
            public IRedisSettings RedisSettings => null;
            public IGoogleOAuthSettings IoAuthSettings => null;
            public int ExecutionPeriodSeconds => 60;
            public bool DebugEnabled { get; set; }
            public ILogSettings LogSettings => null;
        }
    }

    public class WhenNotFoundExceptionIsThrown : BaseExceptionThrownFixture
    {
        [OneTimeSetUp]
        public void Setup() =>
            base.Setup("Expected could not be found.", (x => throw new NotFoundException(Message)));

        [Test]
        public void ShouldSetStatusCodeTo404() =>
            MockResponse.VerifySet(x => x.StatusCode = (int)HttpStatusCode.NotFound);

        [Test]
        public void ShouldIncludeErrorMessageInTheBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain(Message));
        }
    }

    public class WhenBadRequestExceptionIsThrown : BaseExceptionThrownFixture
    {
        [OneTimeSetUp]
        public void Setup() =>
            base.Setup("Bad request: details here", (x => throw new BadRequestException(Message)));

        [Test]
        public void ShouldSetStatusCodeTo400() =>
            MockResponse.VerifySet(x => x.StatusCode = (int)HttpStatusCode.BadRequest);

        [Test]
        public void ShouldIncludeErrorMessageInTheBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain(Message));
        }
    }

    public class WhenGenericExceptionIsThrownAndDebugEnabled : BaseExceptionThrownFixture
    {
        [OneTimeSetUp]
        public void Setup() =>
            base.Setup("This error message should be hidden", (x => throw new Exception(Message)), new TestApiSettings() { DebugEnabled = true });

        [Test]
        public void ShouldSetStatusCodeTo500() =>
            MockResponse.VerifySet(x => x.StatusCode = (int)HttpStatusCode.InternalServerError);

        [Test]
        public void ShouldIncludeErrorMessageInTheBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain(Message));
        }

        [Test]
        public void ShouldIncludeTraceInTheBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain("Trace"));
        }

        [Test]
        public void ShouldIncludeSettingsInTheBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain(ApiSettings.ToString().Replace("\n", "\n\t")));
        }
    }

    public class WhenGenericExceptionIsThrownAndDebugIsNotEnabled : BaseExceptionThrownFixture
    {
        [OneTimeSetUp]
        public void Setup() =>
            base.Setup("This error message should be hidden", (x => throw new Exception(Message)));

        [Test]
        public void ShouldSetStatusCodeTo500() =>
            MockResponse.VerifySet(x => x.StatusCode = (int)HttpStatusCode.InternalServerError);

        [Test]
        public void ShouldNotIncludeErrorMessageInBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Not.Contain(Message));
        }

        [Test]
        public void ShouldIncludeGenericMessageInBody()
        {
            var bytes = new byte[ResponseBody.Length];
            ResponseBody.Position = 0;
            ResponseBody.Read(bytes, 0, (int) ResponseBody.Length);
            var content = Encoding.UTF8.GetString(bytes);
            Assert.That(content, Does.Contain("An unexpected error occurred"));
        }
    }
}