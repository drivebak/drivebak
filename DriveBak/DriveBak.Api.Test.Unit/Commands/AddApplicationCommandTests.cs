using System;
using DriveBak.Api.Commands;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenAddingApplicationWithAllParts : WithAutoMocked<AddApplicationCommand>
    {
        private Application application;

        [SetUp]
        public void Setup()
        {
            application = TestHelpers.GetApplication();
            Sut.Execute(application);
        }

        [Test]
        public void ShouldAddApplicationToTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddApplication(application));
    }

    public class WhenAddingApplicationWithoutDescription : WithAutoMocked<AddApplicationCommand>
    {
        private Application application;

        [SetUp]
        public void Setup()
        {
            application = new Application()
            {
                Name = Guid.NewGuid().ToString(),
            };
            Sut.Execute(application);
        }

        [Test]
        public void ShouldAddApplicationToTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddApplication(application));
    }

    public class WhenAddingApplicationAndNameIsNotValid : WithAutoMocked<AddApplicationCommand>
    {
        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void ShouldThrownBadRequestException(string applicationName)
        {
            var application = new Application()
            {
                Name = applicationName,
            };
            Assert.Throws(typeof(BadRequestException), () => Sut.Execute(application));
        }
    }
}