using System;
using System.Collections.Generic;
using DriveBak.Api.Commands;
using DriveBak.DB;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenRemovingTrackedDirectoryAndDirectoryIsFound : WithAutoMocked<RemoveTrackedDirectoryCommand>
    {
        private int directoryId;
        private string localPath;

        [SetUp]
        public void Setup()
        {
            directoryId = 1;
            localPath = Guid.NewGuid().ToString();
            GetTestDouble<IApplicationsManager>()
                .Setup(x => x.GetDirectoryId(localPath))
                .Returns(directoryId);
            Sut.Execute(Guid.NewGuid().ToString(), localPath);
        }

        [Test]
        public void ShouldQueryDirectoryTable() =>
            GetTestDouble<IApplicationsManager>()
                .Verify(x => x.GetDirectoryId(localPath));

        [Test]
        public void ShouldRemoveDirectoryFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveDirectory(directoryId));
    }

    public class WhenRemovingTrackedDirectoryAndDirectoryIsNotFound : WithAutoMocked<RemoveTrackedDirectoryCommand>
    {
        private string application;
        private string localPath;
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                application = "app";
                localPath = Guid.NewGuid().ToString();
                GetTestDouble<IApplicationsManager>()
                    .Setup(x => x.GetDirectory(localPath))
                    .Returns(new List<BackupDirectory>());
                Sut.Execute(application, localPath);
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldQueryDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectoryId(localPath));

        [Test]
        public void ShouldThrowNotFoundException() =>
            Assert.AreEqual(typeof(NotFoundException), exception.GetType());

        [Test]
        public void ShouldNotRemoveDirectoryFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveDirectory(It.IsAny<int>()), Times.Never);
    }

    public class WhenRemovingTrackedDirectoryAndApplicationIsNull : WithAutoMocked<RemoveTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                Sut.Execute(null, Guid.NewGuid().ToString());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowArgumentException() =>
            Assert.AreEqual(typeof(ArgumentException), exception.GetType());

        [Test]
        public void ShouldNotLookupDirectory() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectory(It.IsAny<string>()), Times.Never);

        [Test]
        public void ShouldNotRemoveDirectoryFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveDirectory(It.IsAny<int>()), Times.Never);
    }

    public class WhenRemovingTrackedDirectoryAndApplicationIsEmpty : WithAutoMocked<RemoveTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                Sut.Execute("", Guid.NewGuid().ToString());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowArgumentException() =>
            Assert.AreEqual(typeof(ArgumentException), exception.GetType());

        [Test]
        public void ShouldNotLookupDirectory() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectory(It.IsAny<string>()), Times.Never);

        [Test]
        public void ShouldNotRemoveDirectoryFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveDirectory(It.IsAny<int>()), Times.Never);
    }
}
