using System;
using DriveBak.Api.Commands;
using DriveBak.DB;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenRemovingExistingApplication : WithAutoMocked<RemoveApplicationCommand>
    {
        private string applicationName;
        private int appId;

        [SetUp]
        public void Setup()
        {
            appId = 1;
            applicationName = Guid.NewGuid().ToString();
            GetTestDouble<IApplicationsManager>().Setup(x => x.GetApplicationId(applicationName)).Returns(appId);
            Sut.Execute(applicationName);
        }

        [Test]
        public void ShouldQueryApplicationsTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetApplicationId(applicationName));

        [Test]
        public void ShouldRemoveApplicationFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveApplication(appId));
    }

    public class WhenRemovingNonexistentApplication : WithAutoMocked<RemoveApplicationCommand>
    {
        private string applicationName;
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                applicationName = Guid.NewGuid().ToString();
                Sut.Execute(applicationName);
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldQueryApplicationsTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetApplicationId(applicationName));


        [Test]
        public void ShouldThrowNotFoundException() =>
            Assert.AreEqual(typeof(NotFoundException), exception.GetType());

        [Test]
        public void ShouldNotRemoveApplicationFromTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.RemoveApplication(It.IsAny<int>()), Times.Never);
    }
}
