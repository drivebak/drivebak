using System;
using System.Collections.Generic;
using DriveBak.Api.Commands;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenGettingTrackedDirectoryAndDirectoryIsFound : WithAutoMocked<GetTrackedDirectoryCommand>
    {
        private BackupDirectory actualDirectory;
        private BackupDirectory expectedDirectory;

        [SetUp]
        public void Setup()
        {
            expectedDirectory = TestHelpers.GetBackupDirectory();
            GetTestDouble<IApplicationsManager>()
                .Setup(x => x.GetDirectory(expectedDirectory.LocalPath))
                .Returns(new List<BackupDirectory>(){expectedDirectory});
            actualDirectory = Sut.Execute(Guid.NewGuid().ToString(), expectedDirectory.LocalPath);
        }

        [Test]
        public void ShouldQueryDirectoryTable() =>
                GetTestDouble<IApplicationsManager>()
                    .Verify(x => x.GetDirectory(expectedDirectory.LocalPath));

        [Test]
        public void ShouldReturnDirectory() =>
            Assert.AreEqual(expectedDirectory, actualDirectory);
    }

    public class WhenGettingTrackedDirectoryAndDirectoryIsNotFound : WithAutoMocked<GetTrackedDirectoryCommand>
    {
        private string application;
        private string localPath;
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                application = "app";
                localPath = Guid.NewGuid().ToString();
                GetTestDouble<IApplicationsManager>()
                    .Setup(x => x.GetDirectory(localPath))
                    .Returns(new List<BackupDirectory>());
                Sut.Execute(application, localPath);
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldQueryDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectory(localPath));

        [Test]
        public void ShouldThrowNotFoundException() =>
            Assert.AreEqual(typeof(NotFoundException), exception.GetType());
    }

    public class WhenGettingTrackedDirectoryAndApplicationIsNull : WithAutoMocked<GetTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                Sut.Execute(null, Guid.NewGuid().ToString());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowArgumentException() =>
            Assert.AreEqual(typeof(ArgumentException), exception.GetType());

        [Test]
        public void ShouldNotLookupDirectory() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectory(It.IsAny<string>()), Times.Never);
    }

    public class WhenGettingTrackedDirectoryAndApplicationIsEmpty : WithAutoMocked<GetTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                Sut.Execute("", Guid.NewGuid().ToString());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowArgumentException() =>
            Assert.AreEqual(typeof(ArgumentException), exception.GetType());

        [Test]
        public void ShouldNotLookupDirectory() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.GetDirectory(It.IsAny<string>()), Times.Never);
    }
}
