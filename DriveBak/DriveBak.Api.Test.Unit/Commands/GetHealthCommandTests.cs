using System;
using DriveBak.Api.Commands;
using DriveBak.Cache;
using DriveBak.DB;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenAllIsHealthy : WithAutoMocked<GetHealthCommand>
    {
        private HealthCheckResponse healthResponse;
        private string dbName;
        private string dbVersion;
        private string cacheName;
        private string cacheVersion;

        [SetUp]
        public void Setup()
        {
            dbName = "database";
            dbVersion = "1.0.0";
            var dbClient = GetTestDouble<IDBClient>();
            dbClient.SetupGet(x => x.Name).Returns(dbName);
            dbClient.SetupGet(x => x.Version).Returns(dbVersion);

            cacheName = "cache";
            cacheVersion = "0.1.0";
            var cacheClient = GetTestDouble<ICacheClient>();
            cacheClient.SetupGet(x => x.Name).Returns(cacheName);
            cacheClient.SetupGet(x => x.Version).Returns(cacheVersion);

            healthResponse = Sut.Execute();
        }

        [Test]
        public void ShouldReturnStatusHealthy() =>
            Assert.AreEqual(Status.Healthy.ToString(), healthResponse.Status);

        [Test]
        public void ShouldReturnApplicationName() =>
            Assert.AreEqual("Drive Bak API", healthResponse.Api.Name);

        [Test]
        public void ShouldReturnVersion() =>
            Assert.IsNotEmpty(healthResponse.Api.Version);

        [Test]
        public void ShouldReturnApiStatusHealthy() =>
            Assert.AreEqual(Status.Healthy.ToString(), healthResponse.Api.Status);

        [Test]
        public void ShouldReturnDbName() =>
            Assert.AreEqual(dbName, healthResponse.Database.Name);

        [Test]
        public void ShouldReturnDbVersion() =>
            Assert.AreEqual(dbVersion, healthResponse.Database.Version);

        [Test]
        public void ShouldReturnDbStatusHealthy() =>
            Assert.AreEqual(Status.Healthy.ToString(), healthResponse.Database.Status);

        [Test]
        public void ShouldReturnCacheName() =>
            Assert.AreEqual(cacheName, healthResponse.Cache.Name);

        [Test]
        public void ShouldReturnCacheVersion() =>
            Assert.AreEqual(cacheVersion, healthResponse.Cache.Version);

        [Test]
        public void ShouldReturnCacheStatusHealthy() =>
            Assert.AreEqual(Status.Healthy.ToString(), healthResponse.Cache.Status);
    }

    public class WhenDatabaseIsNotHealthy : WithAutoMocked<GetHealthCommand>
    {
        private HealthCheckResponse healthResponse;
        private string dbName;
        private Mock<IDBClient> dbClient;

        [SetUp]
        public void Setup()
        {
            dbName = "database";
            dbClient = GetTestDouble<IDBClient>();
            dbClient.SetupGet(x => x.Name).Returns(dbName);
            dbClient.SetupGet(x => x.Version).Throws<Exception>();

            var cacheName = "cache";
            var cacheVersion = "0.1.0";
            var cacheClient = GetTestDouble<ICacheClient>();
            cacheClient.SetupGet(x => x.Name).Returns(cacheName);
            cacheClient.SetupGet(x => x.Version).Returns(cacheVersion);

            healthResponse = Sut.Execute();
        }

        [Test]
        public void ShouldReturnStatusUnhealthy() =>
            Assert.AreEqual(Status.Unhealthy.ToString(), healthResponse.Status);

        [Test]
        public void ShouldReturnDbName() =>
            Assert.AreEqual(dbName, healthResponse.Database.Name);

        [Test]
        public void ShouldNotReturnDbVersion() =>
            Assert.IsNull(healthResponse.Database.Version);

        [Test]
        public void ShouldReturnDbStatusUnhealthy() =>
            Assert.AreEqual(Status.Unhealthy.ToString(), healthResponse.Database.Status);
    }

    public class WhenCacheIsNotHealthy : WithAutoMocked<GetHealthCommand>
    {
        private HealthCheckResponse healthResponse;
        private string cacheName;
        private Mock<ICacheClient> cacheClient;

        [SetUp]
        public void Setup()
        {
            var dbName = "database";
            var dbVersion = "1.0.0";
            var dbClient = GetTestDouble<IDBClient>();
            dbClient.SetupGet(x => x.Name).Returns(dbName);
            dbClient.SetupGet(x => x.Version).Returns(dbVersion);

            cacheName = "cache";
            cacheClient = GetTestDouble<ICacheClient>();
            cacheClient.SetupGet(x => x.Name).Returns(cacheName);
            cacheClient.SetupGet(x => x.Version).Throws<Exception>();
            healthResponse = Sut.Execute();
        }

        [Test]
        public void ShouldReturnStatusDegraded() =>
            Assert.AreEqual(Status.Degraded.ToString(), healthResponse.Status);

        [Test]
        public void ShouldReturnCacheName() =>
            Assert.AreEqual(cacheName, healthResponse.Cache.Name);

        [Test]
        public void ShouldNotReturnCacheVersion() =>
            Assert.IsNull(healthResponse.Cache.Version);

        [Test]
        public void ShouldReturnCacheStatusUnhealthy() =>
            Assert.AreEqual(Status.Unhealthy.ToString(), healthResponse.Cache.Status);
    }
}