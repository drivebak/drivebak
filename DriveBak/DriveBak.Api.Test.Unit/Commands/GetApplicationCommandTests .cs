using System;
using System.Collections.Generic;
using System.Linq;
using DriveBak.Api.Commands;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenGettingApplicationAndRequestIsValid : WithAutoMocked<GetApplicationCommand>
    {
        private Application application;
        private GetApplicationResponse expected;
        private GetApplicationResponse actual;
        private Mock<IApplicationsManager> applicationsManager;

        [SetUp]
        public void Setup()
        {
            application = TestHelpers.GetApplication();
            var directories = TestHelpers.GetBackupDirectories(10, application).ToList();
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetApplicationDirectories(application.Name)).Returns(directories);
            applicationsManager.Setup(x => x.GetApplication(application.Name)).Returns(application);

            expected = new GetApplicationResponse()
            {
                Name = application.Name,
                Description = application.Description,
                Directories = directories,
            };
            actual = Sut.Execute(application.Name);
        }

        [Test]
        public void ShouldQueryForApplication() =>
            applicationsManager.Verify(x => x.GetApplication(application.Name));

        [Test]
        public void ShouldQueryForApplicationDirectories() =>
            applicationsManager.Verify(x => x.GetApplicationDirectories(application.Name));

        [Test]
        public void ShouldReturnGetApplicationResponse()
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expected.Name, actual.Name);
                CollectionAssert.AreEquivalent(expected.Directories, actual.Directories);
            });
        }
    }

    public class WhenApplicationDoesNotExist : WithAutoMocked<GetApplicationCommand>
    {
        private Exception thrown;
        private Mock<IApplicationsManager> applicationsManager;
        private Application application;

        [SetUp]
        public void Setup()
        {
            try
            {
                application = TestHelpers.GetApplication();
                applicationsManager = GetTestDouble<IApplicationsManager>();
                applicationsManager.Setup(x => x.GetApplication(application.Name))
                    .Returns<Application>(null);
                Sut.Execute(application.Name);
            }
            catch (Exception e)
            {
                thrown = e;
            }
        }


        [Test]
        public void ShouldQueryForApplication() =>
            applicationsManager.Verify(x => x.GetApplication(application.Name));

        [Test]
        public void ShouldNotQueryForApplicationDirectories() =>
            GetTestDouble<IApplicationsManager>()
                .Verify(x => x.GetApplicationDirectories(application.Name), Times.Never);

        [Test]
        public void ShouldThrowNotFoundException() =>
            Assert.AreEqual(typeof(NotFoundException), thrown.GetType());
    }

    public class WhenApplicationContainsNoDirectories : WithAutoMocked<GetApplicationCommand>
    {
        private GetApplicationResponse actual;
        private Application application;

        [SetUp]
        public void Setup()
        {
            application = TestHelpers.GetApplication();
            var applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetApplication(application.Name))
                .Returns(application);
            applicationsManager.Setup(x => x.GetApplicationDirectories(application.Name))
                .Returns(new List<BackupDirectory>());
            actual = Sut.Execute(application.Name);
        }

        [Test]
        public void ShouldQueryForApplicationDirectories() =>
            GetTestDouble<IApplicationsManager>()
                .Verify(x => x.GetApplicationDirectories(application.Name));

        [Test]
        public void ShouldReturnGetApplicationResponse()
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(application.Name, actual.Name);
                Assert.AreEqual(application.Description, actual.Description);
                Assert.IsEmpty(actual.Directories);
            });
        }
    }
}