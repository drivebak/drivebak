using System;
using DriveBak.Api.Commands;
using DriveBak.DB;
using DriveBak.Drive;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenAddingTrackedDirectoryAndRequestIsValid : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private AddTrackedDirectoryRequest addTrackedDirectoryRequest;
        private int appId;
        private Application application;
        private BackupDirectory backupDirectory;
        private Mock<IApplicationsManager> applicationsManager;

        [SetUp]
        public void Setup()
        {
            application = TestHelpers.GetApplication();
            appId = 7;
            addTrackedDirectoryRequest = new AddTrackedDirectoryRequest()
                {LocalPath = Guid.NewGuid().ToString(), RemotePath = Guid.NewGuid().ToString(),};
            backupDirectory = new BackupDirectory()
            {
                Application = application.Name,
                LocalPath = addTrackedDirectoryRequest.LocalPath,
                RemotePath = addTrackedDirectoryRequest.RemotePath,
            };
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetApplicationId(It.IsAny<string>()))
                .Returns(appId);
            applicationsManager.Setup(x => x.AddDirectory(appId, backupDirectory))
                .Returns(7);
            GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            Sut.Execute(application.Name, addTrackedDirectoryRequest);
        }

        [Test]
        public void ShouldAddToDirectoryTable() =>
            applicationsManager.Verify(x => x.AddDirectory(appId, backupDirectory));
    }

    public class WhenLocalPathIsInvalid : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(false);
                GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                Sut.Execute(TestHelpers.GetApplication().Name, new AddTrackedDirectoryRequest());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowArgumentException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());

        [Test]
        public void ShouldNotAddToDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()), Times.Never);
    }

    public class WhenAddingTrackedDirectoryAndRemotePathIsInvalid : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(false);
                Sut.Execute(TestHelpers.GetApplication().Name, new AddTrackedDirectoryRequest());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowBadRequestException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());

        [Test]
        public void ShouldNotAddToDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()), Times.Never);
    }

    public class WhenAddingTrackedDirectoryAndApplicationIsNull : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                Sut.Execute(null, new AddTrackedDirectoryRequest());
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowBadRequestException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());

        [Test]
        public void ShouldNotAddToDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()), Times.Never);
    }

    public class WhenAddingTrackedDirectoryAndApplicationNameIsEmpty : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private Exception exception;

        [SetUp]
        public void Setup()
        {
            try
            {
                GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                    .Returns(true);
                Sut.Execute("", new AddTrackedDirectoryRequest()
                {
                    LocalPath = Guid.NewGuid().ToString(),
                    RemotePath = Guid.NewGuid().ToString(),
                });
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldThrowBadRequestException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());

        [Test]
        public void ShouldNotAddToDirectoryTable() =>
            GetTestDouble<IApplicationsManager>().Verify(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()), Times.Never);
    }

    public class WhenAddingTrackedDirectoryAndApplicationDoesNotExist : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private AddTrackedDirectoryRequest addTrackedDirectoryRequest;
        private Application application;
        private Exception exception;
        private Mock<IApplicationsManager> applicationsManager;

        [SetUp]
        public void Setup()
        {
            application = TestHelpers.GetApplication();
            addTrackedDirectoryRequest = new AddTrackedDirectoryRequest()
                {LocalPath = Guid.NewGuid().ToString(), RemotePath = Guid.NewGuid().ToString(),};
            var backupDirectory = new BackupDirectory()
            {
                Application = application.Name,
                LocalPath = addTrackedDirectoryRequest.LocalPath,
                RemotePath = addTrackedDirectoryRequest.RemotePath,
            };
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetApplicationId(It.IsAny<string>()))
                .Returns(0);
            GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            try
            {
                Sut.Execute(application.Name, addTrackedDirectoryRequest);
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldQueryForApplicationId() =>
            applicationsManager.Verify(x => x.GetApplicationId(application.Name));

        [Test]
        public void ShouldNotAddToDirectoryTable() =>
            applicationsManager.Verify(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()), Times.Never);

        [Test]
        public void ItShouldThrowBadRequestException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());
    }

    public class WhenAddingTrackedDirectoryAndAnErrorOccursDuringWrite : WithAutoMocked<AddTrackedDirectoryCommand>
    {
        private AddTrackedDirectoryRequest addTrackedDirectoryRequest;
        private int appId;
        private Application application;
        private Exception exception;
        private Mock<IApplicationsManager> applicationsManager;
        private BackupDirectory backupDirectory;

        [SetUp]
        public void Setup()
        {
            appId = 7;
            application = TestHelpers.GetApplication();
            addTrackedDirectoryRequest = new AddTrackedDirectoryRequest()
                {LocalPath = Guid.NewGuid().ToString(), RemotePath = Guid.NewGuid().ToString(),};
            backupDirectory = new BackupDirectory()
            {
                Application = application.Name,
                LocalPath = addTrackedDirectoryRequest.LocalPath,
                RemotePath = addTrackedDirectoryRequest.RemotePath,
            };
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetApplicationId(It.IsAny<string>()))
                .Returns(appId);
            applicationsManager.Setup(x => x.AddDirectory(It.IsAny<int>(), It.IsAny<BackupDirectory>()))
                .Returns(0);
            GetTestDouble<IFilesystem>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            GetTestDouble<IDriveManager>().Setup(x => x.IsValidPath(It.IsAny<string>()))
                .Returns(true);
            try
            {
                Sut.Execute(application.Name, addTrackedDirectoryRequest);
            }
            catch (Exception e)
            {
                exception = e;
            }
        }

        [Test]
        public void ShouldQueryForApplicationId() =>
            applicationsManager.Verify(x => x.GetApplicationId(application.Name));

        [Test]
        public void ShouldAttemptToAddToDirectoryTable() =>
            applicationsManager.Verify(x => x.AddDirectory(appId, backupDirectory));

        [Test]
        public void ItShouldThrowBadRequestException() =>
            Assert.AreEqual(typeof(BadRequestException), exception.GetType());
    }
}