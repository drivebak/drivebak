using System.Collections.Generic;
using System.Linq;
using DriveBak.Api.Commands;
using DriveBak.DB;
using DriveBak.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace DriveBak.Api.Test.Unit.Commands
{
    public class WhenGettingAllApplications : WithAutoMocked<GetAllApplicationsCommand>
    {
        private IEnumerable<Application> expected;
        private Mock<IApplicationsManager> applicationsManager;
        private IEnumerable<Application> actual;

        [SetUp]
        public void Setup()
        {
            expected = TestHelpers.GetApplications(3).ToList();
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetAllApplications()).Returns(expected);

            actual = Sut.Execute();
        }

        [Test]
        public void ShouldQueryForApplicationDirectories() =>
            applicationsManager.Verify(x => x.GetAllApplications());

        [Test]
        public void ShouldReturnAllApplications() =>
            CollectionAssert.AreEquivalent(expected, actual);
    }

    public class WhenGettingAllApplicationsAndNoApplicationsExist : WithAutoMocked<GetAllApplicationsCommand>
    {
        private Mock<IApplicationsManager> applicationsManager;
        private IEnumerable<Application> actual;

        [SetUp]
        public void Setup()
        {
            applicationsManager = GetTestDouble<IApplicationsManager>();
            applicationsManager.Setup(x => x.GetAllApplications()).Returns(new List<Application>());

            actual = Sut.Execute();
        }

        [Test]
        public void ShouldQueryForApplicationDirectories() =>
            applicationsManager.Verify(x => x.GetAllApplications());

        [Test]
        public void ShouldReturnEmpty() =>
            Assert.IsEmpty(actual);
    }
}

