#!/usr/bin/env sh

RED='\033[0;31m'
NC='\033[0;0m'

error() {
    echo -e "${RED}$1${NC}"
    echo ""
    exit 1
}

envsubst '${API_HOST} ${API_PORT}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

echo "Waiting for api to be ready..."
timeout 45 \
    sh -c 'until $(curl --output /dev/null --silent --fail http://${API_HOST}:${API_PORT}/api/health); do sleep 1; done' || \
    error "Timed out waiting for api to become available"

exec nginx -g "daemon off;" $@
