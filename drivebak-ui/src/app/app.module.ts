import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StatusComponent } from './status/status.component';
import { StatusItemComponent } from './status/status-item/status-item.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { MenuItemComponent } from './nav/menu-item/menu-item.component';
import {MatButtonModule, MatCardModule, MatGridListModule, MatIconModule, MatMenuModule, MatToolbarModule} from '@angular/material';
import {ApplicationsModule} from './applications/applications.module';
import {DirectoriesModule} from './directories/directories.module';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'status', component: StatusComponent},
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    StatusComponent,
    StatusItemComponent,
    NavComponent,
    MenuItemComponent
  ],
    imports: [
      BrowserModule,
      BrowserAnimationsModule,
      HttpClientModule,
      RouterModule.forRoot(routes),
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatIconModule,
      MatMenuModule,
      MatToolbarModule,
      ApplicationsModule,
      DirectoriesModule,
    ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
