export interface IStatus {
  status: string;
  api: IStatusItem;
  database: IStatusItem;
  cache: IStatusItem;
}

export interface IStatusItem {
  status: string;
  name: string;
  version: string;
}
