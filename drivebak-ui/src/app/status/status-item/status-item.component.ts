import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {IStatusItem} from '../status';


@Component({
  selector: 'app-status-item',
  templateUrl: './status-item.component.html',
  styleUrls: ['./status-item.component.css']
})
export class StatusItemComponent implements OnInit {

  @Input() statusItem: IStatusItem;

  constructor() { }

  ngOnInit() {
  }

}
