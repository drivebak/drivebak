import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IStatus} from './status';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  constructor(private http: HttpClient) { }

  getApiHealth(): Observable<IStatus> {
    return this.http.get<IStatus>(`api/health`);
  }
}
