import { Component, OnInit } from '@angular/core';
import {StatusService} from './status.service';
import {IStatus} from './status';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  status: IStatus = {
    status: 'Unhealthy',
    api: {
      name: 'API',
      status: 'unhealthy',
      version: '',
    },
    database: {
      name: 'database',
      status: 'unhealthy',
      version: '',
    },
    cache: {
      name: 'cache',
      status: 'unhealthy',
      version: '',
    },
  };

  constructor(private statusService: StatusService) { }

  ngOnInit() {
    this.statusService.getApiHealth()
      .subscribe(
        status => this.status = status,
          err => console.log(err)
      );
  }
}
