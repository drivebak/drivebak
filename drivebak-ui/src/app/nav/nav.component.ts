import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {INavItem} from './nav-item';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  title = 'Drive Bak';
  navItems: INavItem[] = [
    {
      displayName: 'Dashboard',
      iconName: 'dashboard',
      route: 'dashboard',
    },
    {
      displayName: 'Status',
      iconName: 'check',
      route: 'status',
    },
    {
      displayName: 'Applications',
      iconName: '',
      route: 'applications',
    }
  ];

  loginButton: INavItem = {
    displayName: 'Login',
    iconName: 'keyboard_arrow_right',
    route: 'auth/login',
  };

  logoutButton: INavItem = {
    displayName: 'Logout',
    iconName: 'exit_to_app',
  };

  get loggedIn() {
    return true;
  }

  constructor() { }

  ngOnInit() {
  }

  logout() {

  }
}
