export interface INavItem {
  displayName: string;
  disabled?: boolean;
  hidden?: boolean;
  displayRequiresAuth?: boolean;
  iconName?: string;
  route?: string;
  children?: INavItem[];
}
