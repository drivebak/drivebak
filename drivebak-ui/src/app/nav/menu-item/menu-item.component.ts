import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {INavItem} from '../nav-item';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {

  @Input() items: INavItem[];
  @ViewChild('childMenu', {static: false}) public childMenu;

  get loggedIn() {
    return true;
  }

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
