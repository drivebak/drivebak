import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavComponent } from './nav.component';
import {MatIconModule, MatMenuModule, MatToolbar, MatToolbarModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {MenuItemComponent} from './menu-item/menu-item.component';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
      ],
      declarations: [
        NavComponent,
        MenuItemComponent,
      ],
      providers: [MatToolbar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
