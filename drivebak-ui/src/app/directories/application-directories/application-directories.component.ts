import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DirectoryService} from '../directory.service';
import {IApplication} from '../../application';
import {IDirectory} from '../directory';
import {ActivatedRoute} from '@angular/router';
import {ApplicationsService} from '../../applications/applications.service';
import {DirectoryCreateComponent} from '../directory-create/directory-create.component';

@Component({
  selector: 'app-directory-create',
  templateUrl: './application-directories.component.html',
  styleUrls: ['./application-directories.component.css']
})
export class ApplicationDirectoriesComponent implements OnInit {
  public application: IApplication;
  public displayedColumns: string[] = ['delete', 'localPath', 'remotePath', 'description'];
  public dataSource: MatTableDataSource<IDirectory>;

  @ViewChild(MatPaginator, {static: true}) applicationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) applicationsSort: MatSort;

  constructor(private directoryService: DirectoryService,
              private route: ActivatedRoute,
              private applicationsService: ApplicationsService,
              public createDialog: MatDialog) {
  }

  ngOnInit() {
    this.application = this.route.snapshot.data.resolvedApplication;

    this.dataSource = new MatTableDataSource<IDirectory>(this.application.directories);
    this.dataSource.sort = this.applicationsSort;
    this.dataSource.paginator = this.applicationsPaginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  reload() {
    this.applicationsService.getApplication(this.application.name).subscribe(
      app => this.application = app,
      err => console.log(err),
    );
    location.reload();
  }

  deleteDirectory(directory: IDirectory) {
    const result = confirm(`Are you sure you want to delete tracking for directory '${directory.localPath}'? `);
    if (result) {
      this.directoryService.deleteDirectory(directory).subscribe(
        _ => this.reload(),
        err => {
          console.log(err);
          alert('An error occurred while deleting the application.');
        }
      );
    }
  }

  openCreateDialog() {
    const dialogRef = this.createDialog.open(DirectoryCreateComponent, {
      data: {
        application: this.application
      },
    });

    dialogRef.afterClosed().subscribe(_ => this.reload());
  }
}

