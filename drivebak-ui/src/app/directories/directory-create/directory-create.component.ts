import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MatPaginator, MatSort, MAT_DIALOG_DATA} from '@angular/material';
import {debounceTime} from 'rxjs/operators';
import {DirectoryService} from '../directory.service';
import {IApplication} from '../../application';

@Component({
  selector: 'app-directory-create',
  templateUrl: './directory-create.component.html',
  styleUrls: ['./directory-create.component.css']
})
export class DirectoryCreateComponent implements OnInit {
  public form: FormGroup;
  public description: AbstractControl;
  private newLocalPath: AbstractControl;
  private newRemotePath: AbstractControl;

  public application: IApplication;

  errorMessages = {
    description: '',
    localPath: '',
    remotePath: '',
  };

  private validationMessages = {
    localPath: {
      required: 'Local Path is required',
    },
    remotePath: {
      required: 'Remote Path is required',
    },
    description: {},
  };

  @ViewChild(MatPaginator, {static: true}) applicationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) applicationsSort: MatSort;

  constructor(private dialogRef: MatDialogRef<DirectoryCreateComponent>,
              private fb: FormBuilder,
              private directoryService: DirectoryService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.application = this.data.application;
  }

  ngOnInit() {
    this.form = this.fb.group({
      newLocalPath: ['', Validators.required],
      newRemotePath: ['', Validators.required],
      description: [''],
    });

    this.description = this.form.get('description');
    this.description.valueChanges.pipe(debounceTime(1000)).subscribe(_ => this.setMessage(this.description, 'description'));

    this.newLocalPath = this.form.get('newLocalPath');
    this.newLocalPath.valueChanges.pipe(debounceTime(1000)).subscribe(_ => this.setMessage(this.newLocalPath, 'newLocalPath'));

    this.newRemotePath = this.form.get('newRemotePath');
    this.newRemotePath.valueChanges.pipe(debounceTime(1000)).subscribe(_ => this.setMessage(this.newRemotePath, 'newRemotePath'));
  }

  setMessage(c: AbstractControl, controlKey: string): void {
    this.errorMessages[controlKey] = '';
    if ((c.touched || c.dirty) && c.errors) {
      this.errorMessages[controlKey] = Object.keys(c.errors).map(key =>
        this.validationMessages[controlKey][key]).join(' ');
    }
  }

  createDirectory() {
    if (this.form.valid) {
      this.directoryService.createDirectory({
          application: this.application.name,
          description: this.description.value || null,
          localPath: this.newLocalPath.value,
          remotePath: this.newRemotePath.value,
        }
      ).subscribe(
        _ => this.dialogRef.close(),
        err => {
          console.log(err);
          alert('Error adding directory');
        }
      );
    }
  }
}
