import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { ApplicationDirectoriesComponent } from './application-directories/application-directories.component';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule, MatSortModule,
  MatTableModule
} from '@angular/material';
import {DirectoryCreateComponent} from './directory-create/directory-create.component';
import {RouterModule} from '@angular/router';
import {ApplicationResolverService} from '../applications/application-resolver.service';



@NgModule({
  declarations: [ApplicationDirectoriesComponent, ApplicationDirectoriesComponent, DirectoryCreateComponent],
  entryComponents: [DirectoryCreateComponent],
  imports: [
    RouterModule.forChild([
      {
        path: 'applications/:application', component: ApplicationDirectoriesComponent,
        resolve: {
          resolvedApplication: ApplicationResolverService,
        },
      }
    ]),
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatSortModule,
  ]
})
export class DirectoriesModule { }
