export interface IDirectory {
  application: string;
  description?: string;
  localPath: string;
  remotePath: string;
}
