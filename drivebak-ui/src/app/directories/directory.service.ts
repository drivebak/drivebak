import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IDirectory} from './directory';

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private http: HttpClient) { }

  createDirectory(directory: IDirectory): Observable<IDirectory> {
     return this.http.post<IDirectory>(`api/application/${encodeURIComponent(directory.application)}/directory`, directory);
  }

  deleteDirectory(directory: IDirectory): Observable<{}> {
    return this.http.delete(
      `api/application/${encodeURIComponent(directory.application)}/directory/${encodeURIComponent(directory.localPath)}`, {});
  }
}
