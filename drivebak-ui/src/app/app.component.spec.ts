import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import {DashboardComponent} from './dashboard/dashboard.component';
import {StatusComponent} from './status/status.component';
import {StatusItemComponent} from './status/status-item/status-item.component';
import {NavComponent} from './nav/nav.component';
import {MenuItemComponent} from './nav/menu-item/menu-item.component';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule
} from '@angular/material';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatCardModule,
        MatGridListModule,
      ],
      declarations: [
        AppComponent,
        DashboardComponent,
        StatusComponent,
        StatusItemComponent,
        NavComponent,
        MenuItemComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Drive Bak'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Drive Bak');
  });

  it('should link to gitlab repo in footer', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const app = fixture.debugElement.componentInstance;
    expect(compiled.querySelector('p#repo>a').getAttribute('href')).toEqual(app.repoInfo.link);
  });

  it('should link to license in footer', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const app = fixture.debugElement.componentInstance;
    expect(compiled.querySelector('p#license>a').getAttribute('href')).toEqual(app.repoInfo.license.link);
  });
});
