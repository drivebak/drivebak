import {IDirectory} from './directories/directory';

export interface IApplication {
  name: string;
  description?: string;
  directories: IDirectory[];
}
