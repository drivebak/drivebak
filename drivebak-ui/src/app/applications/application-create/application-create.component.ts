import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {ApplicationsService} from '../applications.service';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-application-create',
  templateUrl: './application-create.component.html',
  styleUrls: ['./application-create.component.css']
})
export class ApplicationCreateComponent implements OnInit {
  public form: FormGroup;
  public name: AbstractControl;
  public description: AbstractControl;

  errorMessages = {
    name: '',
    description: '',
  };

  private validationMessages = {
    name: {
      required: 'Name is required',
    },
    description: {
      required: 'Description is required',
    },
  };

  constructor(private dialogRef: MatDialogRef<ApplicationCreateComponent>,
              private fb: FormBuilder,
              private applicationsService: ApplicationsService) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: [''],
    });

    this.name = this.form.get('name');
    this.name.valueChanges.pipe(debounceTime(1000)).subscribe(_ => this.setMessage(this.name, 'name'));

    this.description = this.form.get('description');
    this.description.valueChanges.pipe(debounceTime(1000)).subscribe(_ => this.setMessage(this.description, 'description'));
  }

  setMessage(c: AbstractControl, controlKey: string): void {
    this.errorMessages[controlKey] = '';
    if ((c.touched || c.dirty) && c.errors) {
      this.errorMessages[controlKey] = Object.keys(c.errors).map(key =>
        this.validationMessages[controlKey][key]).join(' ');
    }
  }

  createApplication() {
    if (this.form.valid) {
      this.applicationsService.createApplication(
        this.name.value,
        this.description.value || null,
      ).subscribe(
        _ => this.dialogRef.close(),
        err => {
          console.log(err);
          alert('Error creating application');
        }
      );
    }
  }

}
