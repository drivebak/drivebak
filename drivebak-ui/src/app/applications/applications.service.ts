import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IApplication} from '../application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  constructor(private http: HttpClient) { }

  getAllApplications(): Observable<IApplication[]> {
    return this.http.get<IApplication[]>('api/application');
  }

  getApplication(applicationName: string): Observable<IApplication> {
    return this.http.get<IApplication>(`api/application/${encodeURIComponent(applicationName)}`);
  }

  createApplication(applicationName: string, applicationDescription: string = null): Observable<string> {
    return this.http.post<string>(`api/application/`, {Name: applicationName, Description: applicationDescription});
  }

  deleteApplication(applicationName: string): Observable<{}> {
    return this.http.delete(`api/application/${encodeURIComponent(applicationName)}`);
  }
}
