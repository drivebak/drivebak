import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ApplicationsService} from './applications.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IApplication} from '../application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationListResolverService implements Resolve<IApplication[]> {

  constructor(private applicationsService: ApplicationsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<IApplication[]> | Promise<IApplication[]> | IApplication[] {
      return this.applicationsService.getAllApplications();
  }
}
