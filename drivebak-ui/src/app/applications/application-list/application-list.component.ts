import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {IApplication} from '../../application';
import {ApplicationsService} from '../applications.service';
import {ApplicationCreateComponent} from '../application-create/application-create.component';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit {
  private applications: IApplication[];
  public displayedColumns: string[] = ['delete', 'name', 'description'];
  public applicationsDataSource: MatTableDataSource<IApplication>;

  @ViewChild(MatPaginator, {static: true}) applicationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) applicationsSort: MatSort;

  constructor(private route: ActivatedRoute,
              private applicationsService: ApplicationsService,
              private router: Router,
              public createDialog: MatDialog) { }

  ngOnInit() {
    this.applications = this.route.snapshot.data.resolvedApplicationList;

    this.applicationsDataSource = new MatTableDataSource<IApplication>(this.applications);
    this.applicationsDataSource.sort = this.applicationsSort;
    this.applicationsDataSource.paginator = this.applicationsPaginator;
  }

  reloadApplications() {
    this.applicationsService.getAllApplications().subscribe(
      applications => this.applications = applications,
      err => console.log(err),
    );
    location.reload();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.applicationsDataSource.filter = filterValue;

    if (this.applicationsDataSource.paginator) {
      this.applicationsDataSource.paginator.firstPage();
    }
  }

  openCreateDialog() {
    const dialogRef = this.createDialog.open(ApplicationCreateComponent, {});

    dialogRef.afterClosed().subscribe(_ => this.reloadApplications());
  }

  deleteApplication(name: string) {
    const result = confirm(`Are you sure you want to delete tracking for '${name}'. ` +
                           'This will remove tracking for all directories in the application.');
    if (result) {
      this.applicationsService.deleteApplication(name).subscribe(
        _ => this.reloadApplications(),
        err => {
          console.log(err);
          alert('An error occurred while deleting the application.');
        }
      );
    }
  }
}
