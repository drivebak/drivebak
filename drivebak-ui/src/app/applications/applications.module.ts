import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationListComponent } from './application-list/application-list.component';
import {RouterModule} from '@angular/router';
import {ApplicationListResolverService} from './application-list-resolver.service';
import {ApplicationsService} from './applications.service';
import {
  MatButtonModule,
  MatCardModule, MatDialogModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import { ApplicationCreateComponent } from './application-create/application-create.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [ApplicationListComponent, ApplicationCreateComponent],
  entryComponents: [ApplicationCreateComponent],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'applications', component: ApplicationListComponent,
        resolve: {
          resolvedApplicationList: ApplicationListResolverService,
        },
      }
    ]),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ReactiveFormsModule,
  ],
  providers: [
    ApplicationsService,
    ApplicationListResolverService,
  ]
})
export class ApplicationsModule { }
