import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ApplicationsService} from './applications.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IApplication} from '../application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationResolverService implements Resolve<IApplication> {

  constructor(private applicationsService: ApplicationsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IApplication> | Promise<IApplication> | IApplication {
    const application = route.paramMap.get('application');
    return this.applicationsService.getApplication(application);
  }
}
