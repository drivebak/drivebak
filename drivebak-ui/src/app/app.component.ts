import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Drive Bak';
  repoInfo = {
    link: 'https://gitlab.com/parker.johansen/drivebak',
    license: {
      name: 'MIT',
      link: 'https://gitlab.com/parker.johansen/drivebak/blob/master/LICENSE'
    },
  };
}
